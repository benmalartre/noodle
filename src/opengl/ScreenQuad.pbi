; ============================================================================
; ScreenQuad Declare Module
; ============================================================================
XIncludeFile "../libs/OpenGL.pbi"
XIncludeFile "../libs/OpenGLExt.pbi"


DeclareModule ScreenQuad

  ;---------------------------------------------------
  ; DataSection
  ;---------------------------------------------------
  DataSection
    screenquad_positions:
    Data.f -1,-1
    Data.f  1,-1
    Data.f  1, 1
    Data.f  1, 1
    Data.f -1, 1
    Data.f -1,-1
  
    screenquad_uvs:
    Data.f 0,0
    Data.f 1,0
    Data.f 1,1
    Data.f 1,1
    Data.f 0,1
    Data.f 0,0
  EndDataSection
       
  Structure ScreenQuad_t
    vbo.i
    vao.i
    shader.i
    *pgm.Program::Program_t
  EndStructure
  
  Declare New()
  Declare Delete(*Me.ScreenQuad_t)
  Declare Setup(*Me.ScreenQuad_t,*pgm.Program::Program_t)
  Declare Draw(*Me.ScreenQuad_t)

EndDeclareModule


; ============================================================================
;  ScreenQuad Module IMPLEMENTATION
; ============================================================================
Module ScreenQuad
  UseModule OpenGL
  UseModule OpenGLExt

  ;----------------------------------------------------------------------------
  ; Constructor
  ;----------------------------------------------------------------------------
  Procedure New()
    Protected *Me.ScreenQuad_t = AllocateMemory(SizeOf(ScreenQuad_t))

    ProcedureReturn *Me
    
  EndProcedure
  
  ;----------------------------------------------------------------------------
  ; Destructor
  ;----------------------------------------------------------------------------
  Procedure Delete(*Me.ScreenQuad_t)
    glDeleteVertexArrays(1,@*Me\vao)
    glDeleteBuffers(1,@*Me\vbo)
    FreeMemory(*Me)
  EndProcedure

  
  ;----------------------------------------------------------------------------
  ; Setup
  ;----------------------------------------------------------------------------
  Procedure Setup(*Me.ScreenQuad_t,*pgm.Program::Program_t)
     ;Generate Vertex Array Object
    glGenVertexArrays(1,@*Me\vao)
    glBindVertexArray(*Me\vao)
    
    ;Generate Vertex Buffer Object
    glGenBuffers(1,@*Me\vbo)
    glBindBuffer(#GL_ARRAY_BUFFER,*Me\vbo)
    
    *Me\pgm = *pgm
    
    ; Get Quad Datas
    Protected GLfloat_s.GLfloat
    Protected size_t.i = 12 * SizeOf(GLfloat_s)
    
    ; Allocate Memory
    Protected *flatdata = AllocateMemory(2*size_t)
    CopyMemory(?screenquad_positions,*flatdata,size_t)
    CopyMemory(?screenquad_uvs,*flatdata+size_t,size_t)
      
    ; Push Buffer to GPU
    glBufferData(#GL_ARRAY_BUFFER,2*size_t,*flatdata,#GL_STATIC_DRAW)
    FreeMemory(*flatdata)
    
    ; Attibute Position
    glEnableVertexAttribArray(0)
    glVertexAttribPointer(0,2,#GL_FLOAT,#GL_FALSE,0,0)
    
    ;Attibute UVs
    glEnableVertexAttribArray(1)
    glVertexAttribPointer(1,2,#GL_FLOAT,#GL_FALSE,0,size_t)
    
    glBindAttribLocation(*pgm\pgm, 0, "position")
    glBindAttribLocation(*pgm\pgm, 1, "coords")
    
    glLinkProgram(*pgm\pgm);
    
    glBindVertexArray(0)
    
  EndProcedure
  
  ;----------------------------------------------------------------------------
  ; Draw
  ;----------------------------------------------------------------------------
  Procedure Draw(*Me.ScreenQuad_t)

    glBindVertexArray(*Me\vao)
    Debug "VAO ---------------------------> "+Str(*Me\vao)
    GLCheckError("BIND ARRAY")
    glDrawArrays(#GL_TRIANGLES,0,6)
    GLCheckError("SCREEN QUAD DRAW")
    glBindVertexArray(0)
  EndProcedure
 

  
EndModule




; IDE Options = PureBasic 5.42 LTS (Windows - x64)
; CursorPosition = 34
; FirstLine = 1
; Folding = --
; EnableXP
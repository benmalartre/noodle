﻿XIncludeFile "../core/Attribute.pbi"
XIncludeFile "../graph/Types.pbi"
XIncludeFile "../graph/Port.pbi"
XIncludeFile "../graph/Node.pbi"
XIncludeFile "../graph/Compound.pbi"
; ==================================================================================================
; TREENODE MODULE DECLARATION
; ==================================================================================================
DeclareModule TreeNode
  Structure TreeNode_t Extends Node::Node_t
  EndStructure
  
  ;------------------------------
  ;Interface
  ;------------------------------
  Interface ITreeNode Extends Node::INode 
  EndInterface
  
  Declare New(*tree.Tree::Tree_t,type.s="Tree",x.i=0,y.i=0,w.i=100,h.i=50,c.i=0)
  Declare Delete(*node.TreeNode_t)
  Declare Init(*node.TreeNode_t)
  Declare RecurseNodes(*node.TreeNode_t,*current.Node::Node_t)
  
  ; ============================================================================
  ;  ADMINISTRATION
  ; ============================================================================
  ;{
  Define *desc.Nodes::NodeDescription_t = Nodes::NewNodeDescription("TreeNode","",@New())
  Nodes::AppendDescription(*desc)
  ;}
  
EndDeclareModule

; ==================================================================================================
; TREENODE MODULE IMPLEMENTATION
; ==================================================================================================
Module TreeNode 
  Procedure Init(*node.TreeNode_t)
    Protected datatype.i = Attribute::#ATTR_TYPE_FLOAT|Attribute::#ATTR_TYPE_INTEGER|Attribute::#ATTR_TYPE_VECTOR2|Attribute::#ATTR_TYPE_VECTOR3
    Node::AddInputPort(*node,"Port1",Attribute::#ATTR_TYPE_EXECUTE)
    Node::AddInputPort(*node,"New(Port1)...",Attribute::#ATTR_TYPE_NEW)
    *node\label = "Tree"
    *node\leaf = #False
    *node\root = #True
  EndProcedure

  Procedure RecurseNodes(*node.TreeNode_t,*current.Node::Node_t)
  ;   CHECK_PTR1_NULL(*current)
  ; 
  ;   ForEach *current\inputs()
  ;     If *current\inputs()\connected
  ;       *node\nodes\Append(*current\inputs()\source\node)
  ;       OTreeNode_RecurseNodes(*node,*current\inputs()\source\node)
  ;     EndIf
  ;   Next
  EndProcedure
  
  Procedure EvaluatePort(*node.TreeNode_t,*port.NodePort::NodePort_t)
    ;recurse to leaf node
    If Not *port\connected Or *port\connexion = #Null : ProcedureReturn : EndIf
    ClearList(*node\nodes())
    AddElement(*node\nodes())
    *node\nodes() = *port\source\node
  
    RecurseNodes(*node,*port\source\node)
    Protected *current.Node::Node_t
    
    Debug "Port : "+*port\name
    Debug "Nb Nodes in Branch : "+Str(ListSize(*node\nodes()))
    Protected i = ListSize(*node\nodes())-1
    LastElement(*node\nodes())
    While i>=0
      *current = *node\nodes()
     ; *current\Evaluate()
      PreviousElement(*node\nodes())
      i-1
    Wend
  
    
  EndProcedure
  
  Procedure Evaluate(*node.TreeNode_t)
     Debug "-------------- Begin Evaluate Tree ----------------------------------"
    ForEach *node\inputs()
      EvaluatePort(*node,*node\inputs())
    Next
    Debug "----------------- End Evaluate Tree -------------------------"
  EndProcedure
  
  Procedure Terminate(*node.TreeNode_t)
  
  EndProcedure
  
  ; ============================================================================
  ;  DESTRUCTOR
  ; ============================================================================
  Procedure Delete(*node.TreeNode_t)
    FreeMemory(*node)
  EndProcedure
 
  
  
  ; ============================================================================
  ;  CONSTRUCTORS
  ; ============================================================================
  ;{
  ; ---[ Heap & stack]-----------------------------------------------------------------
  Procedure.i New(*tree.Tree::Tree_t,type.s="Tree",x.i=0,y.i=0,w.i=100,h.i=50,c.i=0)
    
    ; ---[ Allocate Node Memory ]---------------------------------------------
    Protected *Me.TreeNode_t = AllocateMemory(SizeOf(TreeNode_t))
    ; ---[ Init Node]----------------------------------------------
    Node::Node_INI(TreeNode,*tree,type,x,y,w,h,c)
    
    *Me\name = "TreeNode"
    ; ---[ Return Node ]--------------------------------------------------------
    ProcedureReturn( *Me)
    
  EndProcedure
  ;}
  

  
  ; ============================================================================
  ;  EOF
  ; ============================================================================
EndModule



; IDE Options = PureBasic 5.31 (Windows - x64)
; CursorPosition = 4
; Folding = --
; EnableUnicode
; EnableThread
; EnableXP
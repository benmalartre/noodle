﻿XIncludeFile "../core/Attribute.pbi"
XIncludeFile "../graph/Types.pbi"
XIncludeFile "../graph/Port.pbi"
XIncludeFile "../graph/Node.pbi"
XIncludeFile "../graph/Compound.pbi"
XIncludeFile "../objects/Object3D.pbi"
XIncludeFile "../objects/Polymesh.pbi"
XIncludeFile "../objects/Topology.pbi"

; ==================================================================================================
; FLOAT NODE MODULE DECLARATION
; ==================================================================================================
DeclareModule SetDataNode
  Structure SetDataNode_t Extends Node::Node_t
    *data
    *attribute.Attribute::Attribute_t
  EndStructure
  
  ;------------------------------
  ;Interface
  ;------------------------------
  Interface ISetDataNode Extends Node::INode 
  EndInterface
  
  Declare New(*tree.Tree::Tree_t,type.s="SetData",x.i=0,y.i=0,w.i=100,h.i=50,c.i=0)
  Declare Delete(*node.SetDataNode_t)
  Declare Init(*node.SetDataNode_t)
  Declare Evaluate(*node.SetDataNode_t)
  Declare Terminate(*node.SetDataNode_t)
  
  Declare ResolveReference(*node.SetDataNode_t)
  
  ; ============================================================================
  ;  ADMINISTRATION
  ; ============================================================================
  ;{
  Define *desc.Nodes::NodeDescription_t = Nodes::NewNodeDescription("SetDataNode","Data",@New())
  Nodes::AppendDescription(*desc)
  ;}

EndDeclareModule

; ==================================================================================================
; SETDATA NODE MODULE IMPLEMENTATION
; ==================================================================================================
Module SetDataNode
  UseModule Math
  Procedure ResolveReference(*node.SetDataNode_t)
    Protected *ref.NodePort::NodePort_t = Node::GetPortByName(*node,"Reference")
    Protected refname.s = NodePort::AcquireReferenceData(*ref)
    Debug "[SetDataNode] Reference Name : "+refname
    
    If refname
      Protected fields.i = CountString(refname, ".")+1
      Protected base.s = StringField(refname, 1,".")
      
      If base ="Self" Or base ="This"
        Protected *obj.Object3D::Object3D_t = *node\parent3dobject
        *node\attribute = *obj\m_attributes(StringField(refname, 2,"."))
        If *node\attribute
          Protected *input.NodePort::NodePort_t = Node::GetPortByName(*node,"Data")
  
          NodePort::InitFromReference(*input,*node\attribute)
          *node\state = Graph::#Node_StateOK
          *node\errorstr = ""
        EndIf
        
        Debug "Search Attribute Named : "+StringField(refname, 2,".")+" ---> "+Str(*node\attribute)
      EndIf
    Else
      *node\state = Graph::#Node_StateError
      *node\errorstr = "[ERROR] Input Empty"
      *node\attribute = #Null
    EndIf
    
    
  ;   Protected *obj.C3DObject_t = *node\parent3dobject
  ;   Protected *data.NodePort::NodePort_t = Node::GetPortByName(*node,"Data")
  ;   Protected *ref.NodePort::NodePort_t = Node::GetPortByName(*node,"Reference")
  ;   Protected refname.s = NodePort::AcquireReferenceData(*ref)
  ;   
  ;   If refname = ""
  ;     *node\state = #RAA_GraphNode_StateUndefined
  ;     *node\errorstr = ""
  ;     *node\attribute = #Null
  ;     ProcedureReturn
  ;   Else
  ;     Debug "[SetDataNode] Reference Name : "+refname
  ;   EndIf
  ;   
  ;   Protected fields.i = CountString(refname, ".")+1
  ;   Protected base.s = StringField(refname, 1,".")
  ; 
  ;   If base ="Self" Or base ="This"
  ;     *node\attribute = *obj\m_attributes(StringField(refname, 2,"."))
  ;     Debug "Search Attribute Named : "+StringField(refname, 2,".")+" ---> "+Str(*node\attribute)
  ;     
  ;     If Not *node\attribute
  ;       ; Here we create a nex Attribute
  ;       *node\state = #RAA_GraphNode_StateInvalid
  ;     Else
  ;       *data\currenttype = *node\attribute\datatype    
  ;       Debug "Attribute Name : "+*node\attribute\name
  ;       Debug "Input Current Type : "+Str(*data\currenttype)
  ;       Debug "Attribute Current Type : "+Str(*node\attribute \datatype)
  ;       
  ;       If *data\connected
  ;         If Not *data\source\currenttype = *node\attribute\datatype
  ;           *node\state = #RAA_GraphNode_StateError
  ;           *node\errorstr = "[SetDataNode] input data type doesn't match output data type"
  ;           Debug "[SetDataNode] failed get attribute"
  ;         Else
  ;           *node\state = #RAA_GraphNode_StateOK
  ;           *node\errorstr = ""
  ;           Debug "[SetDataNode] succesfully get attribute"
  ;         EndIf
  ;       Else
  ;         *node\state = #RAA_GraphNode_StateOK
  ;       EndIf
  ;       
  ;       
  ;     EndIf
  ;     
  ;   Else
  ;     *node\attribute = #Null
  ;     *node\state = #RAA_GraphNode_StateUndefined
  ;     *node\errorstr = "[SetDataNode] input data is undefined"
  ;   EndIf
    
  EndProcedure

  
  Procedure Init(*node.SetDataNode_t) 
    Node::AddInputPort(*node,"Data",Attribute::#ATTR_TYPE_POLYMORPH)
    Node::AddInputPort(*node,"Reference",Attribute::#ATTR_TYPE_REFERENCE)
    Node::AddOutputPort(*node,"Execute",Attribute::#ATTR_TYPE_EXECUTE)
    *node\label = "Set Data"
    ResolveReference(*node)
  EndProcedure
  
  Procedure Evaluate(*node.SetDataNode_t)
    Debug "-------> Begin Evaluate [SetDataNode]"
    Protected *input.NodePort::NodePort_t = Node::GetPortByName(*node,"Data")
    Protected x, i, size_t
    Protected v.v3f32
    NodePort::Echo(*input)
    
    Protected *in_data.Carray::CArrayT = NodePort::AcquireInputData(*input)
    ResolveReference(*node.SetDataNode_t)
    
    Protected *obj.Object3D::Object3D_t
    Protected *mesh.Polymesh::Polymesh_t
    
    If *node\state = Graph::#Node_StateOK And *in_data And *node\attribute
      Debug "All is fine baby..."
      size_t = Carray::GetCount(*in_data)
      Select *input\currenttype
        Case Attribute::#ATTR_TYPE_BOOL
          Protected *bIn.Carray::CArrayBool = *in_data
          If *input\currentcontext =Attribute::#ATTR_CTXT_SINGLETON
            PokeB(*node\attribute\data,CArray::GetValueB(*bIn,0))
          Else
            For x=0 To CArray::GetCount(*bIn)-1
              If CArray::GetValueB(*bIn,x)
                Debug "SetDataNode Array Item ["+Str(x)+"]: True"
              Else
                Debug "SetDataNode Array Item ["+Str(x)+"]: False"
              EndIf
              
            Next x
          EndIf
          
        Case Attribute::#ATTR_TYPE_FLOAT
          Debug "SetDataNode Current Type FLOAT"
          Protected *fIn.Carray::CArrayFloat = *in_data
          For x=0 To CArray::GetCount(*fIn)-1
            Debug "SetDataNode Array Item ["+Str(x)+"]: "+StrF(CArray::GetValueF(*fiIn,x))
          Next x
          
        Case Attribute::#ATTR_TYPE_VECTOR3
  
          Protected *vIn.Carray::CArrayV3F32 = *in_data
          Protected *vOut.Carray::CArrayV3F32 = *node\attribute\data
  
          If *node\attribute\name = "PointPosition"
            *obj = *node\parent3dobject
            ;vOut\SetCount(vIn\GetCount())
            CArray::Copy(*vOut,*vIn)
       
            If *obj\type = Object3D::#Object3D_Polymesh
              *mesh = *obj
              *mesh\deformdirty = #True
            EndIf
            
          EndIf
          
          
        Case Attribute::#ATTR_TYPE_TOPOLOGY
          If *node\attribute\name = "Topology"
            Debug "[SetDataNode] TOPOLOGY"
            Protected *tIn.Carray::CArrayPtr = *in_data
            Protected *tOut.Carray::CArrayPtr = *node\attribute\data
           
            ;           Protected *iTopo.CAttributePolymeshTopology_t = tIn\GetValue(0)
            Protected *iTopo.Geometry::Topology_t = CArray::GetValuePtr(*tIn,0)
            *mesh.Polymesh::Polymesh_t = *node\parent3dobject
  
            If *mesh And Object3D::IsA(*mesh,Object3D::#Object3D_Polymesh)
              Protected *geom.Geometry::PolymeshGeometry_t = *mesh\geom
              Debug "[SetDataNode] Call PolymeshGeometry_Set()"
              PolymeshGeometry::Set2(*geom,*iTopo)
              *mesh\topodirty = #True
              Debug "[SetDataNode] Update Polymesh Topology"
            Else
              Debug "[SetDataNode] Topology only supported on POLYMESH!!"
            EndIf
            
          EndIf
          
      EndSelect
      
          
    EndIf
    Debug "-------> End Evaluate [SetDataNode]"
  EndProcedure

  Procedure Terminate(*node.SetDataNode_t)
  
  EndProcedure
  
  Procedure Delete(*node.SetDataNode_t)
    FreeMemory(*node)
  EndProcedure


  ; ============================================================================
  ;  CONSTRUCTORS
  ; ============================================================================
  ;{
  ; ---[ Heap & stack]-----------------------------------------------------------------
  Procedure.i New(*tree.Tree::Tree_t,type.s="SetData",x.i=0,y.i=0,w.i=100,h.i=50,c.i=0)
    
    ; ---[ Allocate Node Memory ]---------------------------------------------
    Protected *Me.SetDataNode_t = AllocateMemory(SizeOf(SetDataNode_t))
    
    ; ---[ Init Node]----------------------------------------------
    Node::Node_INI(SetDataNode,*tree,type,x,y,w,h,c)
    
    ; ---[ Return Node ]--------------------------------------------------------
    ProcedureReturn( *Me)
    
  EndProcedure
  ;}
EndModule


; ============================================================================
;  EOF
; ============================================================================
; IDE Options = PureBasic 5.31 (Windows - x64)
; CursorPosition = 4
; Folding = --
; EnableUnicode
; EnableThread
; EnableXP
﻿XIncludeFile "../core/Array.pbi"
XIncludeFile "../core/Math.pbi"
XIncludeFile "../core/Time.pbi"
XIncludeFile "../core/Application.pbi"

XIncludeFile "../libs/OpenGL.pbi"
XIncludeFile "../libs/GLFW.pbi"
XIncludeFile "../libs/OpenGLExt.pbi"
XIncludeFile "../libs/FTGL.pbi"
XIncludeFile "../libs/Alembic.pbi"

XIncludeFile "../opengl/Shader.pbi"
XIncludeFile "../opengl/Framebuffer.pbi"

XIncludeFile "../objects/Shapes.pbi"
XIncludeFile "../objects/KDTree.pbi"
XIncludeFile "../objects/PointCloud.pbi"
XIncludeFile "../objects/InstanceCloud.pbi"
XIncludeFile "../objects/Polymesh.pbi"



UseModule Math
UseModule Time
UseModule OpenGL
UseModule GLFW
UseModule OpenGLExt

EnableExplicit

Global *pgm.Program::Program_t

Global *buffer.Framebuffer::Framebuffer_t
Global *cloud.PointCloud::PointCloud_t
Global *app.Application::Application_t
Global *viewport.ViewportUI::ViewportUI_t
Global *model.Model::Model_t
Global texture.i

Procedure Draw(*app.Application::Application_t)
  Time::current_frame + 1
  If Time::current_frame>100 : Time::current_frame = 1:EndIf
  
  ;Model::Update(*model)
  
  glUseProgram(*pgm\pgm)
  Define.m4f32 model,view,proj
  Matrix4::SetIdentity(@model)
  
  Framebuffer::BindOutput(*buffer)

  glCheckError("Bind FrameBuffer")
  glViewport(0, 0, *app\width,*app\height)
  glCheckError("Set Viewport")

  glDepthMask(#GL_TRUE);
  glClearColor(0.33,0.33,0.33,1.0)
  glClear(#GL_COLOR_BUFFER_BIT|#GL_DEPTH_BUFFER_BIT)
  glCheckError("Clear")
  glEnable(#GL_DEPTH_TEST)
  
  glEnable(#GL_TEXTURE_2D)
  glBindTexture(#GL_TEXTURE_2D,texture)
  glUniform1i(glGetUniformLocation(*pgm\pgm,"texture"),0)
  
  glUniformMatrix4fv(glGetUniformLocation(*pgm\pgm,"offset"),1,#GL_FALSE,@model)
  glUniformMatrix4fv(glGetUniformLocation(*pgm\pgm,"model"),1,#GL_FALSE,@model)
  
  glUniformMatrix4fv(glGetUniformLocation(*pgm\pgm,"view"),1,#GL_FALSE,*app\camera\view)
  glUniformMatrix4fv(glGetUniformLocation(*pgm\pgm,"projection"),1,#GL_FALSE,*app\camera\projection)
  glUniform3f(glGetUniformLocation(*pgm\pgm,"color"),Random(100)*0.01,Random(100)*0.01,Random(100)*0.01)
  glUniform3f(glGetUniformLocation(*pgm\pgm,"lightPosition"),5,25,5)
  
  ;   PointCloud::Draw(*cloud)
  Model::Update(*model)
  Model::Draw(*model)
  glCheckError("Draw Mesh")
  glDepthMask(#GL_FALSE);
  
  ;Framebuffer::BlitTo(*buffer,#Null,#GL_COLOR_BUFFER_BIT | #GL_DEPTH_BUFFER_BIT,#GL_NEAREST)
  glBindFramebuffer(#GL_DRAW_FRAMEBUFFER,0)
  glClear(#GL_COLOR_BUFFER_BIT|#GL_DEPTH_BUFFER_BIT)
  glBindFramebuffer(#GL_READ_FRAMEBUFFER, *buffer\frame_id);
  glReadBuffer(#GL_COLOR_ATTACHMENT0)
  glBlitFramebuffer(0, 0, *buffer\width,*buffer\height,0, 0, *app\width,*app\height,#GL_COLOR_BUFFER_BIT ,#GL_NEAREST);
  glDisable(#GL_DEPTH_TEST)
  
  If Not #USE_GLFW
    SetGadgetAttribute(*viewport\gadgetID,#PB_OpenGL_FlipBuffers,#True)
  EndIf
  
EndProcedure
    
Define model.m4f32

; Main
;--------------------------------------------
If Time::Init()
  Log::Init()
  Alembic::Init()
  Define f.f

  *app = Application::New("Test",800,600)
  
  
  If Not #USE_GLFW
    *viewport = ViewportUI::New(*app\manager\main,"ViewportUI",0,0,800,600)
    *viewport\camera = *app\camera
    View::SetContent(*app\manager\main,*viewport)
  EndIf
  
  Debug "Camera :: "+Str(*app\camera)
  
  Matrix4::SetIdentity(@model)
    
  
  Debug "Size "+Str(*app\width)+","+Str(*app\height)
  *buffer = Framebuffer::New("Color",*app\width,*app\height)
  
  *pgm = Program::NewFromName("polymesh")
  CompilerIf #PB_Compiler_OS = #PB_OS_MacOS
    *model = Alembic::LoadABCArchive("/Users/benmalartre/Documents/RnD/Modules/abc/Chaley.abc");
  CompilerElseIf #PB_Compiler_OS = #PB_OS_Windows
    *model = Alembic::LoadABCArchive("D:\Projects\RnD\PureBasic\Noodle\abc\Turbulized.abc")
  CompilerElse
    *model = Model::New("Empty")
  CompilerEndIf
  
  Define img = LoadImage(#PB_Any,"H:\Projects\PureBasic\Modules\textures\earth.jpg")
  texture = Utils::GL_LoadImage(img)
  ;Define *t = Alembic::ABC_TestString("Test")
  
  
;   Define i
;   Define pos.v3f32
;   For i=0 To 12
;     Define *mesh.Polymesh::Polymesh_t = Polymesh::New("Star",Shape::#SHAPE_BUNNY)
;     Vector3::Set(@pos,Random(10),Random(10),Random(10))
;     Object3D::AddChild(*model,*mesh)
;     Matrix4::SetTranslation(*mesh\model,@pos)
;   Next
  
  ;Define *compo.Framebuffer::Framebuffer_t = Framebuffer::New("Compo",GadgetWidth(gadget),GadgetHeight(gadget))
  
  Framebuffer::AttachTexture(*buffer,"position",#GL_RGBA,#GL_LINEAR,#GL_REPEAT)
  Framebuffer::AttachRender(*buffer,"depth",#GL_DEPTH_COMPONENT)

;   *cloud = PointCloud::New("PointCloud",100)
;   PointCloud::Setup(*cloud,*pgm)
  Model::Setup(*model,*pgm)
  
  Debug "Setup Model Done!!!"
 Application::Loop(*app,@Draw())
EndIf
; IDE Options = PureBasic 5.31 (Windows - x64)
; CursorPosition = 74
; FirstLine = 45
; Folding = -
; EnableThread
; EnableXP
; Executable = bin\Alembic.app
; Compiler = PureBasic 5.31 (Windows - x64)
; Debugger = Standalone
; Warnings = Display
; Constant = #USE_GLFW=0

XIncludeFile "../core/Application.pbi"

EnableExplicit
UseModule Math
UseModule OpenGL
UseModule OpenGLExt

Global WIDTH = 800
Global HEIGHT = 600

CompilerIf #Use_LEGACY_OPENGL
  Global s_vert.s = "#version 120"+Chr(10)
  s_vert + "attribute vec2 position;"+Chr(10)
  s_vert + "attribute vec2 coords;"+Chr(10)
  s_vert + "varying vec2 texCoords;"+Chr(10)
  s_vert + "void main()"+Chr(10)
  s_vert + "{"+Chr(10)
  s_vert + "    gl_Position = vec4(position,0.0f,1.0f);"+Chr(10)
  s_vert + "    texCoords = coords;"+Chr(10)
  s_vert + "}"
  
  
  Global s_frag.s = "#version 120"+Chr(10)
  s_frag + "void main(){"+Chr(10)
  s_frag + " gl_FragColor = vec4(1.0,0.0,0.0,1.0);"+Chr(10)
  s_frag + "}"
CompilerElse
  
  Global s_vert.s = "#version 330"+Chr(10)
  s_vert + "layout (location = 0) in vec2 position;"+Chr(10)
  s_vert + "layout (location = 1) in vec2 coords;"+Chr(10)
  s_vert + "out vec2 texCoords;"+Chr(10)
  s_vert + "void main()"+Chr(10)
  s_vert + "{"+Chr(10)
  s_vert + "    gl_Position = vec4(position,0.0f,1.0f);"+Chr(10)
  s_vert + "    texCoords = coords;"+Chr(10)
  s_vert + "}"
  
  
  Global s_frag.s = "#version 330"+Chr(10)
  s_frag + "in vec2 texCoords;"+Chr(10)
  s_frag + "out vec4 outColor;"+Chr(10)
  s_frag + "uniform float iGlobalTime;"+Chr(10)
  s_frag + "uniform vec2 iResolution;"+Chr(10)
  s_frag + "void main(){"+Chr(10)
  s_frag + " outColor = vec4(texCoords,0.0,1.0);"+Chr(10)
  s_frag + "}"
CompilerEndIf


Globals::Init()
Time::Init()
Log::Init()
Define *app.Application::Application_t = Application::New("Graph UI",1200,600)
Define *m.ViewManager::ViewManager_t = *app\manager



Define *s1.View::View_t = View::Split(*m\main,#False,0,66)

ViewManager::Event(*m,#PB_Event_SizeWindow)
Define *viewport.UI::IUI = ViewportUI::New(*s1\left,"ViewportUI",*s1\left\x,*s1\left\y,*s1\left\width,*s1\left\height)

Define *graph.UI::IUI = GraphUI::New(*s1\right,"GraphUI",*s1\right\x,*s1\right\y,*s1\right\width,*s1\right\height)
;View::SetContent(*s1\right,*graph)

Define *obj.Object3D::Object3D_t = Polymesh::New("Sphere",Shape::#SHAPE_SPHERE)
Define *tree.Tree::Tree_t = Tree::New(*obj,"Tree",Graph::#Graph_Context_Operator)

GraphUI::SetContent(*graph,*tree)
GraphUI::CanvasEvent(*graph,#PB_Event_SizeWindow)

Define e.i
Repeat
  e  =WindowEvent()
  ViewManager::Event(*m,e)
Until e = #PB_Event_CloseWindow
; IDE Options = PureBasic 5.42 LTS (MacOS X - x64)
; CursorPosition = 63
; FirstLine = 43
; Folding = -
; EnableXP
; Executable = glslsandbox.exe
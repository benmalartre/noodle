﻿XIncludeFile "../core/Application.pbi"

UseModule Math
UseModule CArray

; Define *array.CArrayV3F32 = CArray::newCArrayV3F32()
; Define a.v3f32
; Define i
; For i=0 To 12
;   Vector3::Set(@a,0,i,0)
;   CArray::Append(*array,a)
; Next
; 
; Define *b.v3f32 
; Debug "Array Size : "+Str(CArray::GetCount(*array)-1)
; For i=0 To CArray::GetCount(*array)-1
;   *b = CArray::GetValue(*array,i)
;   Vector3::ScaleInPlace(*b,10)
;   Vector3::Echo(*b)
; Next
; 
; CArray::SetCount(*array,33)
; Debug "Array Size : "+Str(CArray::GetCount(*array))
; For i=0 To CArray::GetCount(*array)-1
;   *b = CArray::GetValue(*array,i)
;   Vector3::ScaleInPlace(*b,0.1)
;   Vector3::Echo(*b)
; Next

DataSection
  label:
  Data.i 1,2,3,4
  Data.i 5,6,7,8
  Data.i 9,10,11,12
  
  label2:
  Data.f 1,2,3,4
  Data.f 5,6,7,8
  Data.f 9,10,11,12
EndDataSection

Debug "====================== ARRAY INTEGER  FROM DATASECTION =============================="
Define *array2.CArrayInt = CArray::newCArrayInt()
Define x.i
Define i
CArray::SetCount(*array2,12)
*array2\data = ?label


For i=0 To CArray::GetCount(*array2)-1
  x = PeekI(CArray::GetValue(*array2,i))
 Debug x
Next

Debug "====================== ARRAY FLOAT  FROM DATASECTION =============================="
Define *array3.CArrayInt = CArray::newCArrayFloat()
Define f.f
CArray::SetCount(*array3,12)
*array3\data = ?label2


For i=0 To CArray::GetCount(*array3)-1
  f = PeekF(CArray::GetValue(*array3,i))
  Debug f
  
Next

Debug "====================== ARRAY PTR =============================="
Define *array4.CArrayPtr = CArray::newCArrayPtr()
CArray::SetCount(*array4,0)
Define x
For x=0 To 4
  Define *mesh.Polymesh::Polymesh_t =Polymesh::New("Test"+Str(x+1),Shape::#SHAPE_CUBE)
  CArray::AppendUnique(*array4,*mesh)
  ;CArray::SetValueI(*array4,x,*mesh)
  Debug *mesh
Next
Debug "---------------------------------"
For x=0 To CArray::GetCount(*array4)-1
  Define *mesh.Polymesh::Polymesh_t = CArray::GetValuePtr(*array4,x)
  Debug *mesh\name
  
Next

CArray::SetCount(*array4,0)

Define x
For x=0 To 13
  Define *mesh.Polymesh::Polymesh_t =Polymesh::New("Test"+Str(x+1),Shape::#SHAPE_CUBE)
  CArray::AppendUnique(*array4,*mesh)
  ;CArray::SetValueI(*array4,x,*mesh)
  Debug *mesh
Next
Debug "---------------------------------"
For x=0 To CArray::GetCount(*array4)-1
  Define *mesh.Polymesh::Polymesh_t = CArray::GetValuePtr(*array4,x)
  Debug *mesh\name
  
Next

Debug "====================== ARRAY FLOAT =============================="
Define *array5.CArrayFloat = CArray::newCArrayFloat()
CArray::SetCount(*array5,0)
Define x
For x=0 To 4
  CArray::AppendF(*array5,x*2.336)
  ;CArray::SetValueI(*array4,x,*mesh)
  Debug *mesh
Next
Debug "---------------------------------"
For x=0 To CArray::GetCount(*array5)-1
  Define f.f = CArray::GetValueF(*array5,x)
  Debug f
  
Next

; IDE Options = PureBasic 5.42 LTS (Windows - x64)
; CursorPosition = 98
; FirstLine = 39
; EnableUnicode
; EnableXP
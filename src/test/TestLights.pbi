XIncludeFile "../core/Application.pbi"

; XIncludeFile "FTGL.pbi"


UseModule Math
UseModule Time
UseModule OpenGL
UseModule GLFW
UseModule OpenGLExt

EnableExplicit

Global WIDTH = 1280
Global HEIGHT = 720

Global vwidth
Global vheight

Global *camera.Camera::Camera_t
Global NewList *lights.Light::Light_t()
Global *ground.Polymesh::Polymesh_t
Global NewList *bunnies.Polymesh::Polymesh_t()

Global *s_wireframe.Program::Program_t
Global *s_polymesh.Program::Program_t
Global *s_gbuffer.Program::Program_t
Global *s_deferred.Program::Program_t
Global shader.l

Global *gbuffer.Framebuffer::Framebuffer_t
Global *deferred.Framebuffer::Framebuffer_t 
Global *quad.ScreenQuad::ScreenQuad_t 

Global *ftgl_drawer.FTGL::FTGL_Drawer

Global offset.m4f32
Matrix4::SetIdentity(@offset)

Global a.v3f32
Global b.v3f32
Global c.v3f32
Global m.m4f32
Global q.q4f32
Global s.v3f32

Global nb_lights = 32
 
; Draw
;--------------------------------------------
Procedure Draw()
  Matrix4::SetIdentity(@offset)
  
  shader = *s_gbuffer\pgm
  glUseProgram(shader)
  ; 1. Geometry Pass: render scene's geometry/color data into gbuffer
  Framebuffer::BindOutput(*gbuffer)
  glViewport(0,0,vwidth,vheight)
  glClearColor(1.0,1.0,1.0,1.0)
  glClear(#GL_COLOR_BUFFER_BIT|#GL_DEPTH_BUFFER_BIT)
  
  glUniformMatrix4fv(glGetUniformLocation(shader,"model"),1,#GL_FALSE,@offset)
  glUniformMatrix4fv(glGetUniformLocation(shader,"view"),1,#GL_FALSE,*camera\view)
  glUniformMatrix4fv(glGetUniformLocation(shader,"projection"),1,#GL_FALSE,*camera\projection)
  glUniform1f(glGetUniformLocation(shader,"nearplane"),*camera\nearplane)
  glUniform1f(glGetUniformLocation(shader,"farplane"),*camera\farplane)
  glUniform3f(glGetUniformLocation(shader,"color"),Random(100)*0.01,Random(100)*0.01,Random(100)*0.01)
;       glUniform3f(glGetUniformLocation(shader,"color"),0,1,0)
      glUniform1f(glGetUniformLocation(shader,"T"),0)
  
  glEnable(#GL_DEPTH_TEST)
  Define p.v3f32

  ForEach *bunnies()
  ;         Vector3::Set(@p,*bunnies()\model\v[12],Sin(Time::Get()+i)+2,*bunnies()\model\v[14])
  ;         Matrix4::SetTranslation(*bunnies()\model,@p)
    Polymesh::Draw(*bunnies())
  Next
  
  Polymesh::Draw(*ground)
  
  glDisable(#GL_DEPTH_TEST)
  
  Define bw = WIDTH/5
  Define bh = HEIGHT/5
  
  Framebuffer::BlitTo(*gbuffer,#Null,#GL_COLOR_BUFFER_BIT | #GL_DEPTH_BUFFER_BIT,#GL_NEAREST)
  glBindFramebuffer(#GL_DRAW_FRAMEBUFFER,0)
  glClear(#GL_COLOR_BUFFER_BIT|#GL_DEPTH_BUFFER_BIT)
  glBindFramebuffer(#GL_READ_FRAMEBUFFER, *gbuffer\frame_id);
  glReadBuffer(#GL_COLOR_ATTACHMENT0)
  glBlitFramebuffer(0, 0, *gbuffer\width,*gbuffer\height,0, 0, vwidth,vheight,#GL_COLOR_BUFFER_BIT ,#GL_NEAREST);
  glDisable(#GL_DEPTH_TEST)
  glReadBuffer(#GL_COLOR_ATTACHMENT1)
  glBlitFramebuffer(0, 0, *gbuffer\width,*gbuffer\height,WIDTH-bw, HEIGHT-2*bh, WIDTH, HEIGHT-bh,#GL_COLOR_BUFFER_BIT,#GL_NEAREST);
  glReadBuffer(#GL_COLOR_ATTACHMENT2)
  glBlitFramebuffer(0, 0, *gbuffer\width,*gbuffer\height,WIDTH-bw, HEIGHT-3*bh, WIDTH, HEIGHT-2*bh,#GL_COLOR_BUFFER_BIT,#GL_NEAREST);
  
  
  shader = *s_deferred\pgm
  glUseProgram(shader)
  
  Framebuffer::BindInput(*gbuffer)
  Framebuffer::BindOutput(*deferred)
  glClear(#GL_COLOR_BUFFER_BIT | #GL_DEPTH_BUFFER_BIT);
  
  glViewport(0,0,*deferred\width,*deferred\height)
  glUniform1i(glGetUniformLocation(shader,"position_map"),0)
  glUniform1i(glGetUniformLocation(shader,"normal_map"),1)
  glUniform1i(glGetUniformLocation(shader,"color_map"),2)
  glUniform1i(glGetUniformLocation(shader,"ssao_map"),3)
  glUniform1i(glGetUniformLocation(shader,"nb_lights"),nb_lights)
  Define i = 0
  ForEach *lights()
    Light::PassToShader(*lights(),shader,i)
    i+1
  Next
  glUniformMatrix4fv(glGetUniformLocation(shader,"view"),1,#GL_FALSE,*camera\view)
;       glUniform3fv(glGetUniformLocation(shader, "viewPos"),1, *camera\pos)
  
  ScreenQuad::Draw(*quad)
      
      
  glViewport(0,0,vwidth,vheight)
  glBindFramebuffer(#GL_DRAW_FRAMEBUFFER,0)
  glClearColor(1.0,1.0,1.0,1.0)
  glClear(#GL_COLOR_BUFFER_BIT|#GL_DEPTH_BUFFER_BIT)
  glBindFramebuffer(#GL_READ_FRAMEBUFFER, *deferred\frame_id);
  glReadBuffer(#GL_COLOR_ATTACHMENT0)
  glBlitFramebuffer(0, 0, *deferred\width,*deferred\height,0, 0, vwidth, vheight,#GL_COLOR_BUFFER_BIT,#GL_LINEAR);
  
  
;   glEnable(#GL_BLEND)
;   glBlendFunc(#GL_SRC_ALPHA,#GL_ONE_MINUS_SRC_ALPHA)
;   glDisable(#GL_DEPTH_TEST)
;   FTGL::SetColor(*ftgl_drawer,1,1,1,1)
;   Define ss.f = 0.85/vwidth
;   Define ratio.f = vwidth / vheight
;   FTGL::Draw(*ftgl_drawer,"SSAO wip",-0.9,0.9,ss,ss*ratio)
;   FTGL::Draw(*ftgl_drawer,"User  : "+UserName(),-0.9,0.85,ss,ss*ratio)
;   glDisable(#GL_BLEND)
  glEnable(#GL_DEPTH_TEST)

 EndProcedure
 

 Globals::Init()
 Log::Init()
; Main
;--------------------------------------------
 If Time::Init()
   
  CompilerIf #USE_GLFW
    glfwInit()
    Define *window.GLFWWindow = glfwCreateFullScreenWindow()
    ;glfwCreateWindow(800,600,"TestGLFW",#Null,#Null)
    glfwMakeContextCurrent(*window)
    glfwGetWindowSize(*window,@vwidth,@vheight)
    GLLoadExtensions()
  CompilerElse
    vwidth = WIDTH
    vheight = HEIGHT
    Define window.i = OpenWindow(#PB_Any,0,0,vwidth,vheight,"OpenGLGadget",#PB_Window_ScreenCentered|#PB_Window_SystemMenu|#PB_Window_MaximizeGadget|#PB_Window_SizeGadget)
    Define gadget.i = OpenGLGadget(#PB_Any,0,0,WindowWidth(window,#PB_Window_InnerCoordinate),WindowHeight(window,#PB_Window_InnerCoordinate))
    
    SetGadgetAttribute(gadget,#PB_OpenGL_SetContext,#True)
    GLLoadExtensions()
   CompilerEndIf  
  Define i
  
  ; Camera
  ;-----------------------------------------------------
  *camera = Camera::New(Camera::#Camera_Perspective)
  
  ; Lights
  ;----------------------------------------------------
  Define i
  For i=0 To nb_lights
    AddElement(*lights())
    *lights() = Light::New()
    Vector3::Set(*lights()\position,Random(20)-10,4,Random(20)-10)
    Vector3::Set(*lights()\color,Random(100)*0.01,Random(100)*0.01,Random(100)*0.01)
  Next
  
  ; FTGL Drawer
  ;-----------------------------------------------------
  FTGL::Init()
  *ftgl_drawer = FTGL::New()
  
  
  ; Shaders
  ;-----------------------------------------------------
  *s_wireframe = Program::NewFromName("wireframe")
  *s_polymesh = Program::NewFromName("polymesh")
  *s_gbuffer = Program::NewFromName("gbuffer")
  *s_deferred = Program::NewFromName("deferred")
  
  shader = *s_gbuffer\pgm
  glUseProgram(shader)

  ; Meshes
  ;-----------------------------------------------------
  Define pos.v3f32, rot.q4f32
  Define color.v3f32
  Quaternion::SetFromEulerAngles(@rot,40,0,0)
  Define x,y,z
  For x = 0 To 9
    For y=0 To 9
      For z=0 To 9
        AddElement(*bunnies())
        *bunnies() = Polymesh::New("Bunny",Shape::#SHAPE_BUNNY)
        Vector3::Set(@color,Random(100)*0.005+0.5,Random(100)*0.005+0.5,Random(100)*0.005+0.5)
        ;Shape::RandomizeColors(*bunnies()\shape,@color,0.0)
        Vector3::Set(@pos,x-5,y+0.75,z-5)
        Matrix4::SetFromQuaternion(*bunnies()\matrix,@rot)
        Matrix4::SetTranslation(*bunnies()\matrix,@pos)
        Polymesh::Setup(*bunnies(),*s_gbuffer)
      Next
    Next
  Next
  
  *ground = Polymesh::New("Ground",Shape::#SHAPE_GRID)
;   Shape::RandomizeColors(*ground\shape,@color,0.1)
  Polymesh::Setup(*ground,*s_polymesh)
  
  ; Geometry Buffer
  ;-----------------------------------------------------
  *gbuffer = Framebuffer::New("GBuffer",vwidth,vheight)
  Framebuffer::AttachTexture(*gbuffer,"position",#GL_RGBA16F,#GL_LINEAR,#GL_REPEAT)
  Framebuffer::AttachTexture(*gbuffer,"normal",#GL_RGBA16F,#GL_LINEAR,#GL_CLAMP)
  Framebuffer::AttachTexture(*gbuffer,"color",#GL_RGBA,#GL_LINEAR,#GL_CLAMP)
  Framebuffer::AttachRender(*gbuffer,"depth",#GL_DEPTH_COMPONENT)
  
  ; Deferred Buffer
  ;-----------------------------------------------------
  *deferred = Framebuffer::New("Deferred",vwidth,vheight)
  Framebuffer::AttachTexture(*deferred,"deferred",#GL_RGBA,#GL_LINEAR)
  
  *quad = ScreenQuad::New()
  ScreenQuad::Setup(*quad,*s_gbuffer)
  
  Quaternion::SetIdentity(@q)
  Matrix4::SetFromQuaternion(@m,@q)
  
  CompilerIf #USE_GLFW
    While Not glfwWindowShouldClose(*window)
      glfwPollEvents()
      Draw()
      glfwSwapBuffers(*window)
     
    Wend
  CompilerElse
    Define e
    Repeat
      
      e = WaitWindowEvent(1000/60)
;       If e=#PB_Event_SizeWindow
; 
;         Camera::Resize(*camera,window,gadget)
;         Framebuffer::Delete(*gbuffer)
;         Framebuffer::Delete(*deferred)
; ;         Framebuffer::Resize(*gbuffer,GadgetWidth(gadget),GadgetHeight(gadget))
; ;         Framebuffer::Resize(*deferred,GadgetWidth(gadget),GadgetHeight(gadget))
;         
;         ; Geometry Buffer
;         ;-----------------------------------------------------
;         *gbuffer = Framebuffer::New("GBuffer",WIDTH,HEIGHT)
;         Framebuffer::AttachTexture(*gbuffer,"position",#GL_RGBA16F,#GL_NEAREST)
;         Framebuffer::AttachTexture(*gbuffer,"normal",#GL_RGBA16F,#GL_NEAREST)
;         Framebuffer::AttachTexture(*gbuffer,"depth",#GL_RGBA,#GL_NEAREST)
;         Framebuffer::AttachRender(*gbuffer,"depth",#GL_DEPTH_COMPONENT)
;   
;         ; Deferred Buffer
;         ;-----------------------------------------------------
;         *deferred = Framebuffer::New("Deferred",WIDTH,HEIGHT)
;         Framebuffer::AttachTexture(*deferred,"deferred",#GL_RGBA,#GL_LINEAR)
; 
; 
;       EndIf
    
      Draw()
     Camera::Event(*camera,gadget)
  
      SetGadgetAttribute(gadget,#PB_OpenGL_FlipBuffers,#True)

    Until e = #PB_Event_CloseWindow
  CompilerEndIf
EndIf

; glDeleteBuffers(1,@vbo)
; glDeleteVertexArrays(1,@vao)
; IDE Options = PureBasic 5.42 LTS (Windows - x64)
; CursorPosition = 46
; FirstLine = 33
; Folding = -
; EnableXP
; Constant = #USE_GLFW=0
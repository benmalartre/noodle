﻿

XIncludeFile "../core/Application.pbi"



UseModule Math
UseModule Time
UseModule OpenGL
UseModule GLFW
UseModule OpenGLExt

EnableExplicit

Global down.b
Global lmb_p.b
Global mmb_p.b
Global rmb_p.b
Global oldX.f
Global oldY.f
Global width.i
Global height.i

Global *torus.Polymesh::Polymesh_t
Global *teapot.Polymesh::Polymesh_t
Global *ground.Polymesh::Polymesh_t
Global *null.Polymesh::Polymesh_t
Global *cube.Polymesh::Polymesh_t
Global *bunny.Polymesh::Polymesh_t
Global *cloud.InstanceCloud::InstanceCloud_t
Global NewList *bunnies.Polymesh::Polymesh_t()

Global *buffer.Framebuffer::Framebuffer_t
Global shader.l
Global *s_wireframe.Program::Program_t
Global *s_polymesh.Program::Program_t
Global *s_pointcloud.Program::Program_t
Global *app.Application::Application_t
Global *viewport.ViewportUI::ViewportUI_t
Global offset.m4f32
Global model.m4f32
Global view.m4f32
Global proj.m4f32
Global T.f
Global *ftgl_drawer.FTGL::FTGL_Drawer

; Resize
;--------------------------------------------
Procedure Resize(window,gadget)
;   width = WindowWidth(window,#PB_Window_InnerCoordinate)
;   height = WindowHeight(window,#PB_Window_InnerCoordinate)
;   ResizeGadget(gadget,0,0,width,height)
;   glViewport(0,0,width,height)
EndProcedure

; Draw
;--------------------------------------------
Procedure Draw(*app.Application::Application_t)
  Framebuffer::BindOutput(*buffer)
  glClearColor(0.25,0.25,0.25,1.0)
  glViewport(0, 0, *buffer\width,*buffer\height)
  glClear(#GL_COLOR_BUFFER_BIT|#GL_DEPTH_BUFFER_BIT)
  glEnable(#GL_DEPTH_TEST)
  
  Protected shader = *s_pointcloud\pgm
  glUseProgram(shader)
  Matrix4::SetIdentity(@offset)
  Framebuffer::BindOutput(*buffer)
  
  glUniformMatrix4fv(glGetUniformLocation(shader,"model"),1,#GL_FALSE,@model)
  glUniformMatrix4fv(glGetUniformLocation(shader,"view"),1,#GL_FALSE,*app\camera\view)
  glUniformMatrix4fv(glGetUniformLocation(shader,"projection"),1,#GL_FALSE,*app\camera\projection)
  
  glUniform3f(glGetUniformLocation(shader,"color"),Random(100)*0.01,Random(100)*0.01,Random(100)*0.01)
  T+0.01

;   Polymesh::Draw(*torus)
;   Polymesh::Draw(*teapot)
;   Polymesh::Draw(*ground)
;   Polymesh::Draw(*null)
;   Polymesh::Draw(*cube)
;   ForEach *bunnies()
;     Polymesh::Draw(*bunnies())
;   Next
  
;   Protected msg.s = "------------ PARTICLE INFOS -----------------"+Chr(10)
;   Protected *geom.Geometry::PointCloudGeometry_t  = *cloud\geom
;   Protected *v.v3f32
;   *v = CArray::GetValue(*geom\a_positions,0)
;   msg + "Position : ("+StrF(*v\x)+","+StrF(*v\y)+","+StrF(*v\z)+")"+Chr(10)
;   *v = CArray::GetValue(*geom\a_normals,0)
;   msg + "Normal : ("+StrF(*v\x)+","+StrF(*v\y)+","+StrF(*v\z)+")"+Chr(10)
;   *v = CArray::GetValue(*geom\a_tangents,0)
;   msg + "Tangent : ("+StrF(*v\x)+","+StrF(*v\y)+","+StrF(*v\z)+")"+Chr(10)
;   
;   MessageRequester("FRAMEWORK",msg)
 
  InstanceCloud::Draw(*cloud)
  
  glDisable(#GL_DEPTH_TEST)
  glViewport(0,0,width,height)
  glBindFramebuffer(#GL_DRAW_FRAMEBUFFER,0)
  glClear(#GL_COLOR_BUFFER_BIT|#GL_DEPTH_BUFFER_BIT)
  glBindFramebuffer(#GL_READ_FRAMEBUFFER, *buffer\frame_id);
  glReadBuffer(#GL_COLOR_ATTACHMENT0)
  glBlitFramebuffer(0, 0, *buffer\width,*buffer\height,0, 0, *app\width,*app\height,#GL_COLOR_BUFFER_BIT ,#GL_NEAREST);
  glDisable(#GL_DEPTH_TEST)
  
;   glEnable(#GL_BLEND)
;   glBlendFunc(#GL_SRC_ALPHA,#GL_ONE_MINUS_SRC_ALPHA)
;   glDisable(#GL_DEPTH_TEST)
;   FTGL::SetColor(*ftgl_drawer,1,1,1,1)
;   Define ss.f = 0.85/width
;   Define ratio.f = width / height
;   FTGL::Draw(*ftgl_drawer,"Yeahhhhhh",-0.9,0.9,ss,ss*ratio)
; 
;   glDisable(#GL_BLEND)
  
  If Not #USE_GLFW
    SetGadgetAttribute(*viewport\gadgetID,#PB_OpenGL_FlipBuffers,#True)
  EndIf

 EndProcedure
 

 
 Define useJoystick.b = #False
 width = 600
 height = 600
; Main
;--------------------------------------------
 If Time::Init()
   Log::Init()
   *app = Application::New("TestMesh",width,height)
   If Not #USE_GLFW
    *viewport = ViewportUI::New(*app\manager\main,"ViewportUI",0,0,width,height)
    *viewport\camera = *app\camera
    View::SetContent(*app\manager\main,*viewport)
    ViewportUI::Event(*viewport ,#PB_Event_SizeWindow)
  EndIf

  Camera::LookAt(*app\camera)
  Matrix4::SetIdentity(@model)
  
  Debug "Size "+Str(*app\width)+","+Str(*app\height)
  *buffer = Framebuffer::New("Color",*app\width,*app\height)
  Framebuffer::AttachTexture(*buffer,"position",#GL_RGBA,#GL_LINEAR,#GL_REPEAT)
  Framebuffer::AttachTexture(*buffer,"normal",#GL_RGBA,#GL_LINEAR,#GL_REPEAT)
  Framebuffer::AttachRender(*buffer,"depth",#GL_DEPTH_COMPONENT)

  ; FTGL Drawer
  ;-----------------------------------------------------
  FTGL::Init()
  *ftgl_drawer = FTGL::New()
  
  *s_wireframe = Program::NewFromName("simple")
  *s_polymesh = Program::NewFromName("polymesh")
  *s_pointcloud = Program::NewFromName("instances")
  shader = *s_pointcloud\pgm
  
  *cloud.InstanceCloud::InstanceCloud_t = InstanceCloud::New("cloud",Shape::#SHAPE_BUNNY,100)
  
;   *torus.Polymesh::Polymesh_t  =  Polymesh::New("Torus",Shape::#SHAPE_TORUS)
;   *teapot.Polymesh::Polymesh_t =  Polymesh::New("Teapot",Shape::#SHAPE_TEAPOT)
;   *ground.Polymesh::Polymesh_t =  Polymesh::New("Grid",Shape::#SHAPE_GRID)
;   *null.Polymesh::Polymesh_t   =  Polymesh::New("Null",Shape::#SHAPE_NULL)
;   *cube.Polymesh::Polymesh_t   =  Polymesh::New("Cube",Shape::#SHAPE_CUBE)
  
;   Define x,z
;   Define pos.v3f32
;   For x=-10 To 10
;     For z=-10 To 10
;       AddElement(*bunnies())
;       *bunnies() = Polymesh::New("Bunny",Shape::#SHAPE_BUNNY)
;       Vector3::Set(@pos,x,0,z)
;       Matrix4::SetTranslation(*bunnies()\matrix,@pos)
;     Next
;   Next
  
;   *bunny.Polymesh::Polymesh_t = Polymesh::New("Bunny",Shape::#SHAPE_BUNNY)
;   Polymesh::Setup(*torus,*s_polymesh)
;   Polymesh::Setup(*teapot,*s_polymesh)
;   Polymesh::Setup(*ground,*s_polymesh)
;   Polymesh::Setup(*null,*s_polymesh)
;   Polymesh::Setup(*cube,*s_polymesh)
;   Polymesh::Setup(*bunny,*s_polymesh)
;   ForEach *bunnies()
;     Polymesh::Setup(*bunnies(),*s_polymesh)
;   Next
  
  Define ps.v3f32, pe.v3f32
  Vector3::Set(@ps,-10,0,0)
  Vector3::Set(@pe,10,0,0)
  PointCloudGeometry::PointsOnSphere(*cloud\geom)
  ;PointCloudGeometry::PointsOnLine(*cloud\geom,@ps,@pe)
  PointCloudGeometry::RandomizeColor(*cloud\geom)
  InstanceCloud::Setup(*cloud,*s_pointcloud)
  
  Define *geom.Geometry::PointCloudGeometry_t = *cloud\geom
;   Define i
;   Define msg.s
;   Define *p.v3f32
;   For i=0 To *geom\nbpoints - 1
;     *p = CArray::GetValue(*geom\a_positions,i)
;     msg + StrF(*p\x)+","+StrF(*p\y)+","+StrF(*p\z)+Chr(10)
;   Next
;   
;   MessageRequester("FRAMEWORK",msg)
  
  
  Define e
  CompilerIf #USE_GLFW
    glfwMakeContextCurrent(*app\window)
      While Not glfwWindowShouldClose(*app\window)
        ;glfwWaitEvents()
        glfwPollEvents()
        
        Draw(*app)
      
        glfwSwapBuffers(*app\window)
       
      Wend
    CompilerElse
      Repeat
        e = WaitWindowEvent(1000/60)
        ViewManager::Event(*app\manager,e)
        Draw(*app)
  
      Until e = #PB_Event_CloseWindow
    CompilerEndIf
EndIf
; IDE Options = PureBasic 5.42 LTS (Windows - x64)
; CursorPosition = 135
; FirstLine = 131
; Folding = -
; EnableUnicode
; EnableXP
; Executable = Test
; Debugger = Standalone
; Constant = #USE_GLFW=0
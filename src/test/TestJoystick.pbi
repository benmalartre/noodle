﻿

XIncludeFile "OpenGL.pbi"
XIncludeFile "GLFW.pbi"
XIncludeFile "OpenGLExt.pbi"
XIncludeFile "Shapes.pbi"
XIncludeFile "CubeMap.pbi"
XIncludeFile "Array.pbi"
XIncludeFile "Camera.pbi"
XIncludeFile "Shader.pbi"
XIncludeFile "Framebuffer.pbi"
XIncludeFile "Math.pbi"
XIncludeFile "Time.pbi"
XIncludeFile "KDTree.pbi"
XIncludeFile "Mesh.pbi"
XIncludeFile "FTGL.pbi"


UseModule Math
UseModule Time
UseModule OpenGL
UseModule GLFW
UseModule OpenGLExt

EnableExplicit

Global *camera.Camera::Camera_t = #Null
Global down.b
Global lmb_p.b
Global mmb_p.b
Global rmb_p.b
Global oldX.f
Global oldY.f

DeclareModule Joystick
  Structure Joystick_t
    *window.GLFWwindow
    horizontal_axis_1.f
    horizontal_axis_2.f
    vertical_axis_1.f
    vertical_axis_2.f
    
  EndStructure
  
  Declare New(*window.GLFW::GLFWwindow)
  Declare Delete(*joystick.Joystick_t)
EndDeclareModule

Module Joystick
  Procedure New(*window.GLFW::GLFWwindow)
    Protected *Me.Joystick_t = AllocateMemory(SizeOf(Joystick_t))
    *Me\window = *window
    ProcedureReturn *Me
  EndProcedure
  
  Procedure Delete(*Me.Joystick_t)
    FreeMemory(*Me)  
  EndProcedure
  
EndModule




; Resize
;--------------------------------------------
Procedure Resize(window,gadget)
  Protected width = WindowWidth(window,#PB_Window_InnerCoordinate)
  Protected height = WindowHeight(window,#PB_Window_InnerCoordinate)
  ResizeGadget(gadget,0,0,width,height)
  glViewport(0,0,width,height)
EndProcedure


 
; Draw
;--------------------------------------------
Procedure Draw(vao,nbp)
  glClearColor(0.25,0.25,0.25,1.0)
  glClear(#GL_COLOR_BUFFER_BIT|#GL_DEPTH_BUFFER_BIT)
  glEnable(#GL_DEPTH_TEST)
;   glBindVertexArray(vao)
;   
;   
;   glEnable(#GL_BLEND)
;   glEnable(#GL_POINT_SMOOTH)
;   glPointSize(3)
;   
;   glEnable(#GL_DEPTH_TEST)
;   glDrawElements(#GL_TRIANGLES,Shape::#TEAPOT_NUM_TRIANGLES*3,#GL_UNSIGNED_INT,#Null)
; 
;   glBindVertexArray(0)

 EndProcedure
 

 
 Define useJoystick.b = #False
 Define j
; Main
;--------------------------------------------
If Time::Init()
  CompilerIf #USE_GLFW
    glfwInit()
    Define *window.GLFWWindow = glfwCreateFullScreenWindow()
    
   ; Define *window.GLFWWindow = glfwCreateWindow(800,600,"TestGLFW",#Null,#Null)
    
    For j=0 To #GLFW_JOYSTICK_LAST
      If glfwJoystickPresent(j)
        useJoystick = j+1
        Debug "Use Joystick "+Str(j+1)
      EndIf
    Next
    
    glfwMakeContextCurrent(*window)
    GLLoadExtensions()
  CompilerElse
    Define window.i = OpenWindow(#PB_Any,0,0,800,600,"OpenGLGadget",#PB_Window_ScreenCentered|#PB_Window_SystemMenu|#PB_Window_MaximizeGadget|#PB_Window_SizeGadget)
    Define gadget.i = OpenGLGadget(#PB_Any,0,0,WindowWidth(window,#PB_Window_InnerCoordinate),WindowHeight(window,#PB_Window_InnerCoordinate))
    SetGadgetAttribute(gadget,#PB_OpenGL_SetContext,#True)
    GLLoadExtensions()
   CompilerEndIf  
   

 
  Define i
  Define *joystick.Joystick::Joystick_t = Joystick::New(*window)
  *camera = Camera::New(Camera::#Camera_Perspective)
  
  Define *position = Shape::?shape_teapot_positions
  Define nbp = Shape::#TEAPOT_NUM_VERTICES
  Define.m4f32 model,view,proj
  Matrix4::SetIdentity(@model)
  Define.v3f32 pos,lookat,up
  Vector3::Set(@pos,5,10,5)
  Vector3::Set(@up,0,1,0)
  Matrix4::GetViewMatrix(@view,@pos,@lookat,@up)
  Matrix4::GetProjectionMatrix(@proj,60,1.4,0.01,10000)
  
  Define s_wireframe.l = Shader::BuildProgram("wireframe")
  Define s_polymesh.l = Shader::BuildProgram("polymesh")

  Define *torus.Polymesh::Polymesh_t = Polymesh::New(Shape::#SHAPE_TORUS,s_polymesh)
  Define *teapot.Polymesh::Polymesh_t = Polymesh::New(Shape::#SHAPE_TEAPOT,s_polymesh)
  Define *ground.Polymesh::Polymesh_t = Polymesh::New(Shape::#SHAPE_GRID,s_polymesh)
  Define *null.Polymesh::Polymesh_t = Polymesh::New(Shape::#SHAPE_NULL,s_polymesh)
  Define *cube.Polymesh::Polymesh_t = Polymesh::New(Shape::#SHAPE_CUBE,s_polymesh)
  Define *bunny.Polymesh::Polymesh_t = Polymesh::New(Shape::#SHAPE_BUNNY,s_polymesh)
  Polymesh::Setup(*torus)
  Polymesh::Setup(*teapot)
  Polymesh::Setup(*ground)
  Polymesh::Setup(*null)
  Polymesh::Setup(*cube)
  Polymesh::Setup(*bunny)
  ;{ 
;    ; Query
;   Define query_vao.i
;   glGenVertexArrays(1,@query_vao)
;   glBindVertexArray(query_vao)
;   
;   Define query_vbo.i
;   glGenBuffers(1,@query_vbo)
;   glBindBuffer(#GL_ARRAY_BUFFER,query_vbo)
;   glBufferData(#GL_ARRAY_BUFFER,Shape::#SPHERE_NUM_VERTICES*3*#PB_Float,#Null,#GL_STATIC_DRAW)
;   glBufferSubData(#GL_ARRAY_BUFFER,0,Shape::#SPHERE_NUM_VERTICES*3*#PB_Float,Shape::?shape_sphere_positions)
;   
;   Define cube_eab.i
;   glGenBuffers(1,@cube_eab)
;   glBindBuffer(#GL_ELEMENT_ARRAY_BUFFER,cube_eab)
;   glBufferData(#GL_ELEMENT_ARRAY_BUFFER,Shape::#SPHERE_NUM_EDGES*2*#PB_Long,Shape::?shape_sphere_edges,#GL_STATIC_DRAW)
;   
;   glVertexAttribPointer(0,3,#GL_FLOAT,#GL_FALSE,0,#Null)
;   glEnableVertexAttribArray(0)
;   
;   glBindVertexArray(0)
  ;}

  Define offset.m4f32
  Matrix4::SetIdentity(@offset)
  
  Define axisCount = 0
  Define axisIt = 0
  Define f.f
  Define axisBuf ;= AllocateMemory(axisCount * SizeOf(f))
  
  Define a.v3f32
  Define b.v3f32
  Define c.v3f32
  Define m.m4f32
  Define q.q4f32
  Define s.v3f32
  Quaternion::SetIdentity(@q)
  Matrix4::SetFromQuaternion(@m,@q)
  Define shader.i
  Define t.f
  CompilerIf #USE_GLFW
    Define vwidth,vheight,mx,my
    
      
    While Not glfwWindowShouldClose(*window)
      glfwPollEvents()
      
      
      If useJoystick
        axisBuf = glfwGetJoystickAxes(#GLFW_JOYSTICK_1, @axisCount)
        *joystick\horizontal_axis_1 = PeekF(axisBuf + 0*SizeOf(f))
        *joystick\vertical_axis_1 = PeekF(axisBuf + 1*SizeOf(f))
        *joystick\horizontal_axis_2 = PeekF(axisBuf + 2*SizeOf(f))
        *joystick\vertical_axis_2 = PeekF(axisBuf + 3*SizeOf(f))
       
      EndIf
      
      Debug "Axis 1 Horizontal : "+StrF(*joystick\horizontal_axis_1)
      Debug "Axis 1 Vertical : "+StrF(*joystick\vertical_axis_1)
      Debug "Axis 2 Horizontal : "+StrF(*joystick\horizontal_axis_2)
      Debug "Axis 2 Vertical : "+StrF(*joystick\vertical_axis_2)
      
      glfwGetWindowSize(*window,@vwidth,@vheight)
      glfwGetCursorPos(*window,@mx,@my)
;       Camera::Event(*camera,mx,my,vwidth,vheight)
      
       shader = s_polymesh
      glUseProgram(shader)
      
      Matrix4::SetIdentity(@offset)
      
      glUniformMatrix4fv(glGetUniformLocation(shader,"offset"),1,#GL_FALSE,@offset)
      glUniformMatrix4fv(glGetUniformLocation(shader,"model"),1,#GL_FALSE,@offset)
      glUniformMatrix4fv(glGetUniformLocation(shader,"view"),1,#GL_FALSE,*camera\view)
      glUniformMatrix4fv(glGetUniformLocation(shader,"projection"),1,#GL_FALSE,*camera\projection)
      glUniform3f(glGetUniformLocation(shader,"color"),0,1,0)
      glUniform1f(glGetUniformLocation(shader,"T"),T)
      T+0.01
      Draw(0,0)
      Polymesh::Draw(*torus)
      Polymesh::Draw(*teapot)
      Polymesh::Draw(*ground)
      Polymesh::Draw(*null)
      Polymesh::Draw(*cube)
      Polymesh::Draw(*bunny)
      glfwSwapBuffers(*window)
      
      
    Wend
  CompilerElse
    Define e
    Repeat
      e = WaitWindowEvent(1000/60)
      shader = s_polymesh
      glUseProgram(shader)
      If e=#PB_Event_SizeWindow
        Camera::Resize(*camera,window,gadget)
      EndIf
      Matrix4::SetIdentity(@offset)
      
      glUniformMatrix4fv(glGetUniformLocation(shader,"offset"),1,#GL_FALSE,@offset)
      glUniformMatrix4fv(glGetUniformLocation(shader,"model"),1,#GL_FALSE,@offset)
      glUniformMatrix4fv(glGetUniformLocation(shader,"view"),1,#GL_FALSE,*camera\view)
      glUniformMatrix4fv(glGetUniformLocation(shader,"projection"),1,#GL_FALSE,*camera\projection)
      glUniform3f(glGetUniformLocation(shader,"color"),0,1,0)
      glUniform1f(glGetUniformLocation(shader,"T"),T)
      T+0.01
      Draw(0,0)
      Polymesh::Draw(*torus)
      Polymesh::Draw(*teapot)
      Polymesh::Draw(*ground)
      Polymesh::Draw(*null)
      Polymesh::Draw(*cube)
      Polymesh::Draw(*bunny)
;       ;Draw(vao,nbp)
;       ;       DrawKDTree(*tree,cube_vao,shader)
;       Vector3::Set(@s,5,5,5)
;       Matrix4::SetScale(@offset,@s)
;       glUniform3f(glGetUniformLocation(shader,"color"),1,0,0)
;       glUniformMatrix4fv(glGetUniformLocation(shader,"model"),1,#GL_FALSE,@offset)
;       DrawQuery(query_vao)
      SetGadgetAttribute(gadget,#PB_OpenGL_FlipBuffers,#True)
  Camera::Event(*camera,GetGadgetAttribute(gadget,#PB_OpenGL_MouseX),GetGadgetAttribute(gadget,#PB_OpenGL_MouseY),GadgetWidth(gadget),GadgetHeight(gadget))
    Until e = #PB_Event_CloseWindow
  CompilerEndIf
EndIf

; glDeleteBuffers(1,@vbo)
; glDeleteVertexArrays(1,@vao)
; IDE Options = PureBasic 5.42 LTS (MacOS X - x64)
; CursorPosition = 106
; FirstLine = 95
; Folding = --
; EnableUnicode
; EnableXP
; Executable = polymesh.exe
; Debugger = Standalone
;=======================================================================
; DECLARATION
;=======================================================================
DeclareModule KDTree
  #KDTREE_DIM = 3
  #KD_F32_MAX = 3.402823466e+38
  #KD_F32_MIN = 1.175494351e-38
  Structure KDPoint_t
    v.f[#KDTREE_DIM]
  EndStructure
  
  Structure KDSort_t
    v.f
    ID.i
  EndStructure
  
  Structure KDNode_t
    ID.i
    level.i
    split_value.f
    *left.KDNode_t
    *right.KDNode_t
    *parent.KDNode_t
    leftID.i
    rightID.i
    parentID.i
    r.f
    g.f
    b.f
    hit.b
    List indices.i()
  EndStructure
  
  Structure KDTree_t Extends Object::Object_t
    m_nbp.i                       ; nb points
    *root.KDNode_t                ; root node
    m_currentaxis.i               ; current axis
    m_levels.i                    ; maximum level depth
    m_cmps.i                      ; count how many comparisons were made in the tree for a query
    m_id.i                        ; current node ID
    Array *points.KDPoint_t(0)    ; array of point pointers
    List closests.KDSort_t()
  EndStructure
  
  Declare New()
  Declare Delete(*tree.KDTree_t)
  Declare NewNode(*parent.KDNode_t,ID.i,level.i)
  Declare DeleteNode(*node.KDNode_t)

  Declare Split(*tree.KDTree_t,*node.KDNode_t,*left.KDNode_t,*right.KDNode_t)
  Declare Build(*tree.KDTree_t,*pnts,nbp.i, max_levels=99,min_pnts=10)
  Declare.f Distance(*a.KDPoint_t,*b.KDPoint_t)
  Declare GetBoundingBox(*tree.KDTree_t,*node.KDNode_t,*min.KDPoint_t,*max.KDPoint_t)
  Declare SearchAtNode(*tree.KDTree_t,*node.KDNode_t, *query.KDPoint_t, *retID, *retDist, *retNode)
  Declare SearchAtNodeRange(*tree.KDTree_t,*node.KDNode_t, *query.KDPoint_t, range.f,*retID, *retDist)
  Declare Search(*tree.KDTree_t,*query.KDPoint_t,*retID,*retDist)
  Declare SearchN(*tree.KDTree_t, *query.KDPoint_t,max_distance,max_points)
  Declare ResetHit(*tree.KDTree_t)
EndDeclareModule

;=======================================================================
; IMPLEMENTATION
;=======================================================================
Module KDTree
  ; Distance Between Two Points
  ;-------------------------------------------------------------------
  Procedure.f Distance(*a.KDPoint_t,*b.KDPoint_t)
    Protected dist.f,d.f
    Protected i
    For i=0 To #KDTREE_DIM-1
      d = *a\v[i] - *b\v[i]
      dist + d*d
    Next
    ProcedureReturn dist
  EndProcedure
  
  
  ; New Node
  ;--------------------------------------------------------------------
  Procedure NewNode(*parent.KDNode_t,ID.i,level.i)
    Protected *node.KDNode_t = AllocateMemory(SizeOf(KDNode_t))
    InitializeStructure(*node,KDNode_t)
    *node\parent = *parent
    *node\ID = ID
    *node\level = level
    *node\r = Random(100)*0.01
    *node\g = Random(100)*0.01
    *node\b = Random(100)*0.01
    ProcedureReturn *node
  EndProcedure
  
  ; Delete Node
  ;--------------------------------------------------------------------
  Procedure DeleteNode(*node.KDNode_t)
    If *node\left:DeleteNode(*node\left):EndIf
    If *node\right:DeleteNode(*node\right):EndIf
    ClearStructure(*node,KDNode_t)
    FreeMemory(*node)
  EndProcedure
  
    
  ; Constructor
  ;--------------------------------------------------------------------
  Procedure New()
    Protected *tree.KDTree_t = AllocateMemory(SizeOf(KDTree_t))
    InitializeStructure(*tree,KDTree_t)
    *tree\root = #Null
    *tree\m_id = 0
    ProcedureReturn *tree
  EndProcedure
  
  ; Destructor
  ;--------------------------------------------------------------------
  Procedure Delete(*tree.KDTree_t)
    DeleteNode(*tree\root)
    ClearStructure(*tree,KDTree_t)
    FreeMemory(*tree)
  EndProcedure
  
  ; Sort Points
  ;--------------------------------------------------------------------
  Procedure SortPoints(*tree.KDTree_t,a.i,b.i)
    Protected *a.KDPoint_t = *tree\points(a)
    Protected *b.KDPoint_t = *tree\points(b)
    
    ProcedureReturn Bool(*a\v[*tree\m_currentaxis]<*b\v[*tree\m_currentaxis])
  EndProcedure
  
  ; Get Bounding Box
  ;--------------------------------------------------------------------
  Procedure GetBoundingBox(*tree.KDTree_t,*node.KDNode_t,*min.KDPoint_t,*max.KDPoint_t)
    Protected *v.KDPoint_t
    *min\v[0] = #KD_F32_MAX
    *min\v[1] = #KD_F32_MAX
    *min\v[2] = #KD_F32_MAX
    
    *max\v[0] = #KD_F32_MIN
    *max\v[1] = #KD_F32_MIN
    *max\v[2] = #KD_F32_MIN
    
;     Vector3_Set(*min,#F32_MAX,#F32_MAX,#F32_MAX)
;     Vector3_Set(*max,-#F32_MAX,-#F32_MAX,-#F32_MAX)
  
    ForEach *node\indices()
      *v = *tree\points(*node\indices())
      ;Vector3_MulByMatrix4InPlace(*v,*srt)
      If *v\v[0] < *min\v[0] : *min\v[0] = *v\v[0] : EndIf
      If *v\v[1] < *min\v[1] : *min\v[1] = *v\v[1] : EndIf
      If *v\v[2] < *min\v[2] : *min\v[2] = *v\v[2] : EndIf
     
      If *v\v[0] > *max\v[0] : *max\v[0] = *v\v[0] : EndIf
      If *v\v[1] > *max\v[1] : *max\v[1] = *v\v[1] : EndIf
      If *v\v[2] > *max\v[2] : *max\v[2] = *v\v[2] : EndIf
    Next
  EndProcedure
  
  ; ResetHit
  ;--------------------------------------------------------------------
  Procedure ResetHitAtNode(*tree.KDTree_t,*node.KDNode_t)
    If *node\left
      ResetHitAtNode(*tree,*node\left)
      ResetHitAtNode(*tree,*node\right)
    Else
      *node\hit = #False  
    EndIf
    
      
  EndProcedure
  ; ResetHit
  ;--------------------------------------------------------------------
  Procedure ResetHit(*tree.KDTree_t)
    ResetHitAtNode(*tree,*tree\root)
  EndProcedure
  
  
  ; Split
  ;--------------------------------------------------------------------
  Procedure Split(*tree.KDTree_t,*node.KDNode_t,*left.KDNode_t,*right.KDNode_t)
    *tree\m_currentaxis = *node\level % #KDTREE_DIM
    Protected i,j,k
    
    ; Sort Indices
    Dim v.KDSort_t(ListSize(*node\indices())-1)
    
    ForEach *node\indices()
      v(i)\v = *tree\points(*node\indices())\v[*tree\m_currentaxis]
      v(i)\ID = *node\indices() 
      i+1
    Next
    
    SortStructuredArray(v(),#PB_Sort_Ascending,OffsetOf(KDSort_t\v),#PB_Float)
    
    i=0
    ForEach *node\indices()
      *node\indices() = v(i)\ID
      i+1
    Next
    
    SelectElement( *node\indices(),ListSize(*node\indices())/2)
    *node\split_value = *tree\points(*node\indices())\v[*tree\m_currentaxis]
    
    ForEach *node\indices()
        
      j = *node\indices()
      
      If *tree\points(j)\v[*tree\m_currentaxis]<*node\split_value
        AddElement(*left\indices())
        *left\indices() = j
      Else
        AddElement(*right\indices())
        *right\indices() = j
      EndIf
    Next
    
  EndProcedure
  
  ; Search At Node
  ;--------------------------------------------------------------------
  Procedure SearchAtNode(*tree.KDTree_t,*node.KDNode_t, *query.KDPoint_t, *retID, *retDist, *retNode)
    Protected best_idx.i=0
    Protected best_dist.f = #KD_F32_MAX
    Protected idx.i
    Protected dist.f
    While #True
      Protected split_axis.i = *node\level % #KDTREE_DIM
      *tree\m_cmps +1
      If *node\left = #Null

        PokeI(*retNode,*node)

        ForEach *node\indices()
          *tree\m_cmps +1
          idx = *node\indices()
          dist = Distance(*query,*tree\points(idx))
          If(dist<best_dist)
            best_dist = dist
            best_idx = idx
          EndIf
          
        Next
        
        Break 
      ElseIf(*query\v[split_axis]<*node\split_value)
        *node = *node\left
      Else
        *node = *node\right
      EndIf
    Wend
    
    PokeI(*retID,best_idx)
    PokeF(*retDist,best_dist)
    
  EndProcedure
  
  ; Search At Node Range
  ;--------------------------------------------------------------------
  Procedure SearchAtNodeRange(*tree.KDTree_t,*node.KDNode_t, *query.KDPoint_t, range.f,*retID, *retDist)
    Protected best_idx.i
    Protected best_dist.f = #KD_F32_MAX
    
    Protected split_axis.i
    Protected idx.i
    Protected dist.f
    
    Protected NewList *to_visit.KDNode_t()
    AddElement(*to_visit())
    *to_visit() = *node
    
    While ListSize(*to_visit())
      Protected NewList *next_search.KDNode_t()
      While ListSize(*to_visit())
        LastElement(*to_visit())
        *node = *to_visit()
        DeleteElement(*to_visit())
        split_axis = *node\level % #KDTREE_DIM
        
        If *node\left = #Null
          ForEach *node\indices()
            *tree\m_cmps +1
            
            idx = *node\indices()
            dist = Distance(*query,*tree\points(idx))
            If dist<best_dist
              best_dist = dist
              best_idx = idx
              *node\hit = #True
            EndIf
          Next
   
        Else
          dist = *query\v[split_axis]- *node\split_value
          ; there are 3 possible scenarios
          ; the hypercircle only intersect the left region
          ; the hypercricle only intersect the right region
          ; the hypercircle intersects both
          
          *tree\m_cmps +1
          If(Abs(dist)>range)
            If dist<0
              AddElement(*next_search())
              *next_search() = *node\left
            Else
              AddElement(*next_search())
              *next_search() = *node\right
            EndIf
            
          Else
            AddElement(*next_search())
            *next_search() = *node\left
            AddElement(*next_search())
            *next_search() = *node\right
          EndIf
        EndIf
      Wend
      CopyList(*next_search(),*to_visit())
      
    Wend  
    
    PokeI(*retID,best_idx)
    PokeF(*retDist,best_dist)
  EndProcedure
  
  ; SearchN At Node Range
  ;--------------------------------------------------------------------
  Procedure SearchNAtNodeRange(*tree.KDTree_t,*node.KDNode_t, *query.KDPoint_t, range.f)
    Protected best_idx.i
    Protected best_dist.f = #KD_F32_MAX
    
    Protected split_axis.i
    Protected idx.i
    Protected dist.f
    
    Protected NewList *to_visit.KDNode_t()
    AddElement(*to_visit())
    *to_visit() = *node
    
    While ListSize(*to_visit())
      Protected NewList *next_search.KDNode_t()
      While ListSize(*to_visit())
        LastElement(*to_visit())
        *node = *to_visit()
        DeleteElement(*to_visit())
        split_axis = *node\level % #KDTREE_DIM
        
        If *node\left = #Null
          ForEach *node\indices()
            *tree\m_cmps +1
            
            idx = *node\indices()
            dist = Distance(*query,*tree\points(idx))
            If dist<range
              *node\hit = #True
              AddElement(*tree\closests())
              *tree\closests()\ID = idx
              *tree\closests()\v = Sqr(dist)
            EndIf
          Next
   
        Else
          dist = *query\v[split_axis]- *node\split_value
          ; there are 3 possible scenarios
          ; the hypercircle only intersect the left region
          ; the hypercricle only intersect the right region
          ; the hypercircle intersects both
          
          *tree\m_cmps +1
          If(Abs(dist)>range)
            If dist<0
              AddElement(*next_search())
              *next_search() = *node\left
            Else
              AddElement(*next_search())
              *next_search() = *node\right
            EndIf
            
          Else
            AddElement(*next_search())
            *next_search() = *node\left
            AddElement(*next_search())
            *next_search() = *node\right
          EndIf
        EndIf
      Wend
      CopyList(*next_search(),*to_visit())
      
    Wend  

  EndProcedure
  
  ; Search
  ;--------------------------------------------------------------------
  Procedure Search(*tree.KDTree_t, *query.KDPoint_t,*retID, *retDist)
    ; Find the closest Node, this will be the upper bound for the next searches
    Protected *best_node.KDNode_t = #Null
    Protected best_idx = 0
    Protected best_dist.f = #KD_F32_MAX
    Protected radius.f = 0
    *tree\m_cmps = 0
    
    SearchAtNode(*tree,*tree\root,*query,@best_idx,@best_dist,@*best_node)
    radius = Sqr(best_dist)
    
    ; now find possible other candidates
    Protected *node.KDNode_t = *best_node
    Protected *parent.KDNode_t
    Protected split_axis.i
   
    
    While *node\parent
      ;Go up
      *parent = *node\parent
      split_axis = *parent\level % #KDTREE_DIM
      
      ; Search theother node
      Protected tmp_idx
      Protected tmp_dist.f = #KD_F32_MAX
      Protected *tmp_node.KDNode_t
      Protected *search_node.KDNode_t
      
      If Abs(*parent\split_value - *query\v[split_axis]) <= radius
        ; search opposite node
        If Not *parent\left = *node
          SearchAtNodeRange(*tree,*parent\left,*query,radius,@tmp_idx,@tmp_dist)
        Else
          SearchAtNodeRange(*tree,*parent\right,*query,radius,@tmp_idx,@tmp_dist)
        EndIf
      EndIf
      
      If tmp_dist<best_dist
        best_dist = tmp_dist
        best_idx = tmp_idx
      EndIf
      
      *node = *parent
    Wend  
      
    PokeI(*retID,best_idx)
    PokeF(*retDist,best_dist)
    
  EndProcedure
  
  ; Search
  ;--------------------------------------------------------------------
  Procedure SearchN(*tree.KDTree_t, *query.KDPoint_t,max_distance,max_points)
    ; Find the closest Node, this will be the upper bound for the next searches
    Protected *best_node.KDNode_t = #Null
    Protected best_idx = 0
    Protected best_dist.f = #KD_F32_MAX
    Protected radius.f = 0
    *tree\m_cmps = 0
    Protected founds.i = 0
    ResetHit(*tree)
    ClearList(*tree\closests())
    
    SearchAtNode(*tree,*tree\root,*query,@best_idx,@best_dist,@*best_node)
    radius = Sqr(best_dist)
    If radius > max_distance
      ProcedureReturn
    EndIf
    
    
    AddElement(*tree\closests())
    *tree\closests()\ID = best_idx
    *tree\closests()\v = radius
    
    SearchNAtNodeRange(*tree,*best_node,*query,max_distance)
    
    ; now find possible other candidates
    Protected *node.KDNode_t = *best_node
    Protected *parent.KDNode_t
    Protected split_axis.i
   
    
    While *node\parent
      ;Go up
      *parent = *node\parent
      split_axis = *parent\level % #KDTREE_DIM
      
      ; Search theother node
      Protected tmp_idx
      Protected tmp_dist.f = #KD_F32_MAX
      Protected *tmp_node.KDNode_t
      Protected *search_node.KDNode_t
      
      If Abs(*parent\split_value - *query\v[split_axis]) <= max_distance
        ; search opposite node
        If Not *parent\left = *node
          SearchNAtNodeRange(*tree,*parent\left,*query,max_distance)
        Else
          SearchNAtNodeRange(*tree,*parent\right,*query,max_distance)
        EndIf
      EndIf

      
      *node = *parent
    Wend  
    
    SortStructuredList(*tree\closests(),#PB_Sort_Ascending,OffsetOf(KDSort_t\v),TypeOf(KDSort_t\v))
    ForEach *tree\closests()
      Debug "Closest Point ID "+Str(*tree\closests()\ID)+" Distance ----> "+StrF(*tree\closests()\v)
    Next
    
    If max_points >0
      While ListSize(*tree\closests())>max_points
        LastElement(*tree\closests())
        DeleteElement(*tree\closests())
      Wend  
      
    EndIf
    
    
    
    
  EndProcedure
  
  
  ; Build
  ;--------------------------------------------------------------------
  Procedure Build(*tree.KDTree_t,*pnts ,nbp.i, max_levels=99,min_pnts=10)
    *tree\m_nbp = nbp
    *tree\m_levels = max_levels
    *tree\root = NewNode(#Null,0,0)
    *tree\root\ID = *tree\m_id
    *tree\m_id+1
   
    Protected pnt.KDPoint_t
    ReDim *tree\points(nbp-1)
    Protected i
    For i=0 To nbp-1
      *tree\points(i) = *pnts+i*SizeOf(KDPoint_t)
    Next

    For i=0 To nbp-1
      AddElement(*tree\root\indices())
      *tree\root\indices() = i
    Next
    
    Protected *node.KDNode_t
    NewList *to_visit.KDNode_t()
    AddElement(*to_visit())
    *to_visit() = *tree\root
    While ListSize(*to_visit())
      NewList *next_search.KDNode_t()
      While(ListSize(*to_visit()))
        LastElement(*to_visit())
        *node = *to_visit()
        DeleteElement(*to_visit())
        If *node\level <*tree\m_levels
          If ListSize(*node\indices())>min_pnts
            Protected *left.KDNode_t = NewNode(*node,*tree\m_id,*node\level+1)
            Protected *right.KDNode_t = NewNode(*node,*tree\m_id+1,*node\level+1)
            *tree\m_id+2
            
            Split(*tree,*node,*left,*right)
            
            ;Clear current indices
            ClearList( *node\indices())
            
            *node\left = *left
            *node\right = *right
            *node\leftID = *left\ID
            *node\rightID = *right\ID
            
            If ListSize(*left\indices())
              AddElement(*next_search())
              *next_search() = *left
            EndIf
            
            If ListSize(*right\indices())
              AddElement(*next_search())
              *next_search() = *right
            EndIf
            
          EndIf
          
        EndIf
        
      Wend
      CopyList(*next_search(),*to_visit())

    Wend
    
  EndProcedure
  
;   ; DrawKDNode
;   ;--------------------------------------------
;   Procedure DrawKDNode(*tree.KDTree::KDTree_t,*node.KDTree::KDNode_t,shader.i,ID.i=-1)
;     If *node\left
;       DrawKDNode(*tree,*node\left,shader)
;       DrawKDNode(*tree,*node\right,shader)
;     Else
;       If ID = -1 Or ID = *node\ID
;       Protected m.m4f32
;         Matrix4::SetIdentity(@m)
;         Protected min.v3f32,max.v3f32, c.v3f32,s.v3f32
;         KDTree::GetBoundingBox(*tree,*node,@min,@max)
;         Vector3::Sub(@s,@max,@min)
;     ;     Vector3::ScaleInPlace(@s,0.5)
;         Vector3::LinearInterpolate(@c,@min,@max,0.5)
;         
;         Matrix4::SetScale(@m,@s)
;         Matrix4::SetTranslation(@m,@c)
;         glUniform4f(glGetUniformLocation(shader,"color"),*node\r,*node\g,*node\b,0.25)
;         glUniformMatrix4fv(glGetUniformLocation(shader,"offset"),1,#GL_FALSE,@m)
;         glDrawElements(#GL_LINES,24,#GL_UNSIGNED_INT,#Null)
;         EndIf
;   
;     EndIf
;     
;     
;   EndProcedure
;   
;   ; DrawKDTree
;   ;--------------------------------------------
;   Procedure DrawKDTree(*tree.KDTree_t)
;     glBindVertexArray(*tree\vao)
;     DrawNode(*tree,*tree\root)
;     glBindVertexArray(0)
;   EndProcedure
  
EndModule


;========================================================================
; Test Code
;========================================================================
; Define nbp = 12000
; Define pnt.KDTree::KDPoint_t
; Define *pnt.KDTree::KDPoint_t
; Define i
; 
; 
; Define *pnts = AllocateMemory(nbp*SizeOf(pnt))
; 
; For i=0 To nbp-1
;   *pnt = *pnts+i*SizeOf(pnt)
;   *pnt\v[0] = Random(100)
;   *pnt\v[1] = Random(100)
;   *pnt\v[2] = Random(100)
; Next
; 
; Define *tree.KDTree::KDTree_t = KDTree::New()
; KDTree::Build(*tree,*pnts,nbp,12)
; 
; Define query.KDTree::KDPoint_t
; query\v[0] = Random(100)*0.05
; query\v[1] = Random(100)*0.05
; query\v[2] = Random(100)*0.05
; Define retID.i
; Define retDist.f
; KDTree::Search(*tree,@query,@retID,@retDist)
; Define result.s
; result = "Closest Point ID : "+Str(retID)+"\n"
; result + "Closest Distance : "+StrF(retDist)+"\n"
; result + "Num Queries : "+Str(*tree\m_cmps)+"\n"
; MessageRequester("KDTree",result)
; ; Define.KDTree::KDPoint_t min,max
; ; KDTree::GetBoundingBox(*tree,,@min,@max)

  

; IDE Options = PureBasic 5.31 (Windows - x64)
; CursorPosition = 33
; Folding = ----
; EnableXP
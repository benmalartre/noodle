
XIncludeFile "../graph/Node.pbi"

DeclareModule Stack
  Structure Stack_t
    List *nodes.Node::Node_t()
  EndStructure
  
  Declare New()
  Declare Delete(*stack.Stack_t)
  Declare Update(*stack.Stack_t)
  Declare AddNode(*stack.Stack_t,*node.Node::Node_t)
EndDeclareModule

Module Stack
  Procedure New()
    Protected *stack.Stack_t = AllocateMemory(SizeOf(Stack_t))
    InitializeStructure(*stack,Stack_t)
    
    ProcedureReturn *stack
  EndProcedure
  
  Procedure Delete(*stack.Stack_t)
    ClearStructure(*stack,Stack_t)
    FreeMemory(*stack)
  EndProcedure
  
  Procedure Update(*stack.Stack_t)
    Protected inode.Node::INode
    ForEach *stack\nodes()
      inode = *stack\nodes()
      inode\Evaluate()
    Next
    
  EndProcedure
  
  Procedure AddNode(*stack.Stack_t,*node.Node::Node_t)
    AddElement(*stack\nodes())
    *stack\nodes() = *node
  EndProcedure

EndModule


; IDE Options = PureBasic 5.31 (Windows - x64)
; CursorPosition = 36
; Folding = --
; EnableXP
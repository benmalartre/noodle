XIncludeFile "../core/Math.pbi"
XIncludeFile "../core/Array.pbi"
XIncludeFile "../core/Attribute.pbi"
XIncludeFile "../core/Object.pbi"
XIncludeFile "../opengl/Shader.pbi"
XIncludeFile "../objects/Geometry.pbi"
XIncludeFile "../objects/Stack.pbi"

DeclareModule Object3D
  UseModule Math
  Enumeration
    #Object3D_Camera
    #Object3D_Light
    #Object3D_Null
    #Object3D_Polymesh
    #Object3D_Curve
    #Object3D_PointCloud
    #Object3D_InstanceCloud
    #Object3D_Grid
    #Object3D_Model
    #Object3D_Root
    #Object3D_Layer
  EndEnumeration
  

  #DIRTY_STATE_CLEAN = 0
  #DIRTY_STATE_TRANSFORM = 1
  #DIRTY_STATE_DEFORM = 2
  #DIRTY_STATE_TOPOLOGY = 4

  Structure Object3D_t Extends Object::Object_t
    vao.i
    vbo.i
    eab.i
    type.i
    *pgm.Program::Program_t
    *geom.Geometry::Geometry_t
    name.s
    fullname.s
    matrix.m4f32
    initialized.b
    visible.b
    dirty.i
    localT.Transform::Transform_t
    globalT.Transform::Transform_t
    staticT.Transform::Transform_t
    
    *parent.Object3D_t
    List *children.Object3D_t()
    *stack.Stack::Stack_t
    Map *m_attributes.Attribute::Attribute_t()
  EndStructure
  
  
   Macro Object3D_ATTR()
    Protected *m.m4f32 = *Me\globalT\m
    Protected *global = Attribute::New("GlobalTransform",Attribute::#ATTR_TYPE_MATRIX4,Attribute::#ATTR_STRUCT_SINGLE,Attribute::#ATTR_CTXT_SINGLETON,*m,#False,#True)
    Object3D::AddAttribute(*Me,*global)
    *m = *Me\localT\m
    Protected *local = Attribute::New("LocalTransform",Attribute::#ATTR_TYPE_MATRIX4,Attribute::#ATTR_STRUCT_SINGLE,Attribute::#ATTR_CTXT_SINGLETON,*m,#False,#True)
    Object3D::AddAttribute(*Me,*local)
    Protected *viewvis = Attribute::New("ViewVisibility",Attribute::#ATTR_TYPE_BOOL,Attribute::#ATTR_STRUCT_SINGLE,Attribute::#ATTR_CTXT_SINGLETON,@*Me\visible,#False,#True)
    Object3D::AddAttribute(*Me,*viewvis)
  EndMacro
  
  
  
  Interface IObject3D
   ; GetName.s()
    Delete()
    Setup(*shader.Program::Program_t)
    Update()
    Draw()
  EndInterface
  
  
  Declare.b IsA(*obj.Object3D_t,type.i)
  Declare Freeze(*obj.Object3D_t)
  Declare FreezeTransform(*obj.Object3D_t)
  Declare ResetStaticKinematicState(*obj.Object3D_t)
  Declare AddChild(*obj.Object3D_t,*child.Object3D_t)
  Declare.b RemoveChild(*obj.Object3D_t)
;   Declare SetWireframeColor(*obj.Object3D_t,r.d,g.d,b.d)
;   Declare SetMaterial(*obj.Object3D_t,shader.i)
;   Declare GetMaterial(*obj.Object3D_t)
;   Declare GetUniqueID(*obj.Object3D_t)
;   Declare UpdateTransform (*obj.Object3D_t,*t.Transform::Transform_t)
  Declare GetAttribute(*obj.Object3D_t,name.s)
  Declare AddAttribute(*obj.Object3D_t,*attribute.Attribute::Attribute_t)
  Declare DeleteAttribute(*obj.Object3D_t,name.s)
  Declare DeleteAllAttributes(*obj.Object3D_t)
  Declare.b CheckAttributeExist(*obj.Object3D_t,atttrname.s)
;   Declare SetVisibility(visible.b)
EndDeclareModule




Module Object3D
  
  ; Is A
  ;--------------------------------------------------------------
  Procedure.b IsA(*obj.Object3D_t,type.i)
    ProcedureReturn Bool(*obj\type = type)
  EndProcedure
  ; Freeze
  ;--------------------------------------------------------------
  Procedure Freeze(*obj.Object3D_t)
    
  EndProcedure
  
  ; FreezeTransform
  ;--------------------------------------------------------------
  Procedure FreezeTransform(*obj.Object3D_t)
    
  EndProcedure
  
  ; Reset Static Kinematic State
  ;--------------------------------------------------------------
  Procedure ResetStaticKinematicState(*obj.Object3D_t)
    Transform::SetFromOther(*obj\staticT,*obj\globalT)
  EndProcedure
  
  ; Remove Child
  ;--------------------------------------------------------------
  Procedure.b RemoveChild(*obj.Object3D_t)

  EndProcedure
  
  ; Add Child 
  ;--------------------------------------------------------------
  Procedure AddChild(*parent.Object3D_t,*child.Object3D_t)
    If *child\parent
      ForEach *child\parent\children()
        If *child\parent\children() = *child
          DeleteElement(*child\parent\children())
          Break;
        EndIf
      Next
    EndIf
    *child\parent = *parent
    AddElement(*parent\children())
    *parent\children() = *child
  EndProcedure
  
  
  ;-----------------------------------------------
  ; Get Attribute
  ;-----------------------------------------------
  Procedure GetAttribute(*obj.Object3D_t,name.s)
    Debug *obj\name
    Debug name
    If Not *obj\m_attributes(name)
      Debug "C3DObject : Can't find Attribute "+name
      ProcedureReturn #Null
    Else
      ProcedureReturn *obj\m_attributes(name)
    EndIf
    
  EndProcedure
  
  ;-----------------------------------------------
  ; Add Attribute
  ;-----------------------------------------------
  Procedure AddAttribute(*obj.Object3d_t,*attribute.Attribute::Attribute_t)
    If Not *obj : ProcedureReturn : EndIf
    If Not *attribute : ProcedureReturn : EndIf
    
    If *obj\m_attributes(*attribute\name)
      Debug "C3DObject ["+*obj\name+"]: Attribute "+*attribute\name+" Already exists !! "
      ProcedureReturn #Null
    Else
      ;*obj\attributes\Append(*attribute)
      Graph::AttachMapElement(*obj\m_attributes(),*attribute\name,*attribute)
    EndIf
    
  ;   Debug "---------------------------------------------------------------------------------------"
  ;   Debug "3DObject Add Attribute Called..."
  ;   Debug "3DObject : "+*obj\name
  ;   Debug "Nb Attributes : "+Str(*obj\attributes\GetCount()-1)
  ;   Debug "---------------------------------------------------------------------------------------"
    
  EndProcedure
  
  ;-----------------------------------------------
  ; Delete Attribute
  ;-----------------------------------------------
  Procedure DeleteAttribute(*obj.Object3D_t,name.s)
    If Not *obj : ProcedureReturn : EndIf
  
    If *obj\m_attributes(name)
      Protected *attribute.Attribute::Attribute_t = *obj\m_attributes(name)
      DeleteMapElement(*obj\m_attributes(),name)
      Attribute::Delete(*attribute)
    EndIf
    
  EndProcedure
  
  ;-----------------------------------------------
  ; Delete All Attributes
  ;-----------------------------------------------
  Procedure DeleteAllAttributes(*obj.Object3D_t)
    If Not *obj : ProcedureReturn : EndIf
    
    ForEach *obj\m_attributes()
      Protected *attribute.Attribute::Attribute_t = *obj\m_attributes()
      DeleteMapElement(*obj\m_attributes())
      Attribute::Delete(*attribute)
    Next
    
    FreeMap(*obj\m_attributes())
    
  EndProcedure
  
  ;-----------------------------------------------
  ; Check Attribute Exists
  ;-----------------------------------------------
  Procedure.b CheckAttributeExist(*obj.Object3D_t,atttrname.s)
    If *obj\m_attributes(atttrname)
      ProcedureReturn #True
    Else
      ProcedureReturn #False
    EndIf
    
  EndProcedure

  
  
EndModule

; IDE Options = PureBasic 5.31 (Windows - x64)
; CursorPosition = 30
; FirstLine = 27
; Folding = ---
; EnableXP
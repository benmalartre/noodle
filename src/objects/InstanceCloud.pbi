﻿XIncludeFile "../libs/OpenGL.pbi"
XIncludeFile "../libs/OpenGLExt.pbi"
XIncludeFile "../core/Math.pbi"
XIncludeFile "../core/Array.pbi"
XIncludeFile "Shapes.pbi"
XIncludeFile "Object3D.pbi"
XIncludeFile "../opengl/Shader.pbi"
XIncludeFile "Geometry.pbi"
XIncludeFile "PointCloudGeometry.pbi"


;========================================================================================
; InstanceCloud Module Declaration
;========================================================================================
DeclareModule InstanceCloud
  UseModule OpenGL
  UseModule OpenGLExt
  UseModule Math
  
  Structure InstanceCloud_t Extends Object3D::Object3D_t
    *shape.Shape::Shape_t
  EndStructure
  
  Interface IInstanceCloud Extends Object3D::IObject3D
  EndInterface
  
  Declare New(name.s,shape.i=Shape::#SHAPE_CUBE,nbp.i = 1)
  Declare Delete(*Me.InstanceCloud_t)
  Declare Setup(*Me.InstanceCloud_t,*shader.Program::Program_t)
  Declare Update(*Me.InstanceCloud_t)
  Declare Draw(*Me.InstanceCloud_t)
  
  DataSection 
    InstanceCloudVT: 
    Data.i @Delete()
    Data.i @Setup()
    Data.i @Update()
    Data.i @Draw()
  EndDataSection 
  
EndDeclareModule

;========================================================================================
; InstanceCloud Module Implementation
;========================================================================================
Module InstanceCloud
  UseModule OpenGL
  UseModule OpenGLExt

  ; Constructor
  ;----------------------------------------------------
  Procedure New(name.s,shape.i=Shape::#SHAPE_CUBE,nbp.i = 1)
    Protected *Me.InstanceCloud_t = AllocateMemory(SizeOf(InstanceCloud_t))
    InitializeStructure(*Me,InstanceCloud_t)
    *Me\VT = ?InstanceCloudVT
    *Me\classname = "INSTANCECLOUD"
    *Me\name = name
    *Me\geom = PointCloudGeometry::New(nbp)
    *Me\visible = #True
    *Me\shape = Shape::New(shape)
    *Me\stack = Stack::New()
    *Me\type = Object3D::#Object3D_InstanceCloud
    Matrix4::SetIdentity(*Me\matrix)
    ProcedureReturn *Me
  EndProcedure
  
  ; Destructor
  ;----------------------------------------------------
  Procedure Delete(*Me.InstanceCloud_t)
    glDeleteVertexArrays(1,*Me\vao)
    glDeleteBuffers(1,*Me\vbo)
    glDeleteBuffers(1,*Me\eab)
    ClearStructure(*Me,InstanceCloud_t)
    FreeMemory(*Me)
  EndProcedure
  
  ; Coatlicue Deesse Azteque de la mort
  
  
  ; Get Shape Flat Array Data Size
  ;----------------------------------------------------
  Procedure GetShapeDataSize(*Me.InstanceCloud_t)
    Protected *shape.Shape::Shape_t = *Me\shape
    Protected nbp = *shape\nbt * 3
    Protected glfloat.f
    Protected size_s = nbp*9*SizeOf(glfloat)
    ProcedureReturn size_s
  EndProcedure
  
  ; Build Shape Flat Array Data
  ;----------------------------------------------------
  Procedure GetShapeArrayDatas(*Me.InstanceCloud_t,size_s)
    Protected *geom.Geometry::PointCloudGeometry_t = *Me\geom
    Protected *shape.Shape::Shape_t = *Me\shape
    
    Protected *flatdatas = AllocateMemory(size_s)
    Protected i
    Protected offset.i = 0
    Protected f.f
    Protected so = SizeOf(f)*3

    ; Position
    For i=0 To CArray::GetCount(*shape\indices)-1
      CopyMemory(CArray::GetPtr(*shape\positions,PeekL(CArray::GetValue(*shape\indices,i))),*flatdatas+offset,so)
      offset+so
    Next
   
    ; Normal
    offset = 0
    For i=0 To CArray::GetCount(*shape\indices)-1
      CopyMemory(CArray::GetPtr(*shape\normals,PeekL(CArray::GetValue(*shape\indices,i))),*flatdatas+offset,so)
      offset+so
    Next
    
    ; UVWs
    offset = 0
    For i=0 To CArray::GetCount(*shape\indices)-1
      CopyMemory(CArray::GetPtr(*shape\uvws,PeekL(CArray::GetValue(*shape\indices,i))),*flatdatas+offset,so)
      offset+so
    Next

    glBufferSubData(#GL_ARRAY_BUFFER,0,size_s,*flatdatas)

  EndProcedure
  
  ; Build GL Data
  ;----------------------------------------------------
  Procedure  BuildGLData(*Me.InstanceCloud_t)
    Protected *geom.Geometry::PointCloudGeometry_t = *Me\geom
    Protected *shape.Shape::Shape_t = *Me\shape
     GetShapeDataSize(*Me)
    ; Get Point Cloud Datas
    Protected s_glfloat.GLfloat
    Protected s_glint.GLint
    Protected size_t.i = *geom\nbpoints * SizeOf(s_glfloat)
    Protected size_s.i = GetShapeDataSize(*Me)
    
      
      ;---[ Vertex Array Object ]-------------
    glGenVertexArrays(1,@*Me\vao)
    glBindVertexArray(*Me\vao)
    
    ; Create Vertex Buffer Object
    glGenBuffers(1,@*Me\vbo)
    glBindBuffer(#GL_ARRAY_BUFFER,*Me\vbo)
  
    ; Push Buffer to GPU
    ;glBufferData(#GL_ARRAY_BUFFER,size_t*10,*geom\a_floats\GetPtr(),#GL_STREAM_DRAW)
    glBufferData(#GL_ARRAY_BUFFER,size_s*3+size_t*17,#Null,#GL_DYNAMIC_DRAW)
    GetShapeArrayDatas(*Me,size_s)
    glBufferSubData(#GL_ARRAY_BUFFER,size_s*3,size_t*3,CArray::GetPtr(*geom\a_positions,0))
    glBufferSubData(#GL_ARRAY_BUFFER,size_s*3+size_t*3,size_t*3,CArray::GetPtr(*geom\a_normals,0))
    glBufferSubData(#GL_ARRAY_BUFFER,size_s*3+size_t*6,size_t*3,CArray::GetPtr(*geom\a_tangents,0))
    glBufferSubData(#GL_ARRAY_BUFFER,size_s*3+size_t*9,size_t*4,CArray::GetPtr(*geom\a_color,0))
    glBufferSubData(#GL_ARRAY_BUFFER,size_s*3+size_t*13,size_t*3,CArray::GetPtr(*geom\a_scale,0))
    glBufferSubData(#GL_ARRAY_BUFFER,size_s*3+size_t*16,size_t,CArray::GetPtr(*geom\a_size,0))
    
     
  ;   ; Create Element Array Buffer
  ;   glGenBuffers(1,@*p\eabs(*ctx\ID))
  ;   glBindBuffer(#GL_ELEMENT_ARRAY_BUFFER,*p\eabs(*ctx\ID))
  ;   glBufferData(#GL_ELEMENT_ARRAY_BUFFER,*p\shape\GetNbIndices()*SizeOf(s_glint),*p\shape\GetIndices(),#GL_DYNAMIC_DRAW)
    
    ; Shape Datas
    glEnableVertexAttribArray(0)
    glVertexAttribPointer(0,3,#GL_FLOAT,#GL_FALSE,0,0)
    
    glEnableVertexAttribArray(1)
    glVertexAttribPointer(1,3,#GL_FLOAT,#GL_FALSE,0,size_s)
    
    glEnableVertexAttribArray(2)
    glVertexAttribPointer(2,3,#GL_FLOAT,#GL_FALSE,0,size_s*2)
    
    ; Attribute Position
    glEnableVertexAttribArray(3)
    glVertexAttribPointer(3,3,#GL_FLOAT,#GL_FALSE,0,size_s*3)
    
    ; Attribute Normal
    glEnableVertexAttribArray(4)
    glVertexAttribPointer(4,3,#GL_FLOAT,#GL_FALSE,0,size_t*3+size_s*3)
    
    ; Attribute Tangent
    glEnableVertexAttribArray(5)
    glVertexAttribPointer(5,3,#GL_FLOAT,#GL_FALSE,0,size_t*6+size_s*3)
    
    ; Attribute Color
    glVertexAttribPointer(6,4,#GL_FLOAT,#GL_FALSE,0,size_t*9+size_s*3)
    glEnableVertexAttribArray(6)
    
    ;Attribute Scale
    glVertexAttribPointer(7,3,#GL_FLOAT,#GL_FALSE,0,size_t*13+size_s*3)
    glEnableVertexAttribArray(7)
    
    ;Attribute Size
    glVertexAttribPointer(8,1,#GL_FLOAT,#GL_FALSE,0,size_t*16+size_s*3)
    glEnableVertexAttribArray(8)
    
    glVertexAttribDivisor(3,1)
    glVertexAttribDivisor(4,1)
    glVertexAttribDivisor(5,1)
    glVertexAttribDivisor(6,1)
    glVertexAttribDivisor(7,1)
    glVertexAttribDivisor(8,1)
    
    ; Bind Attributes Locations
    glBindAttribLocation(pgm,0,"s_pos")
    glBindAttribLocation(pgm,1,"s_norm")
    glBindAttribLocation(pgm,2,"s_uvws")
    glBindAttribLocation(pgm,3,"position")
    glBindAttribLocation(pgm,4,"normal")
    glBindAttribLocation(pgm,5,"tangent")
    glBindAttribLocation(pgm,6,"color")
    glBindAttribLocation(pgm,7,"scale")
    glBindAttribLocation(pgm,8,"size")
    
  EndProcedure
  
  
   
  ; Setup
  ;----------------------------------------------------
  Procedure Setup(*Me.InstanceCloud_t,*pgm.Program::Program_t)
    
    Protected *geom.Geometry::PointCloudGeometry_t = *Me\geom
    Protected *shape.Shape::Shape_t = *Me\shape
    
    ;---[ Update Geometry ]----------------------------
    PointCloudGeometry::Update(*Me)
    
   
  
    ;If Not *p\initialized : ProcedureReturn #Null : EndIf
    
    ;Attach Shader
    *Me\pgm = *pgm
    Protected pgm = *pgm\pgm
    glUseProgram(pgm)
    
    BuildGLData(*Me)
    
    glLinkProgram(*Me\pgm\pgm);
    ; Check For Errors
    Protected linked.i
    If Not glGetProgramiv(*Me\pgm\pgm, #GL_LINK_STATUS, @linked);
      ;Make sure linked==TRUE
      ;If linked==FALSE, the log contains information on what went wrong
      Protected maxLength.i
      glGetProgramiv(*Me\pgm\pgm, #GL_INFO_LOG_LENGTH, @maxLength);
      maxLength = maxLength + 1                                  ;
      Protected uchar.c
      Protected *pLinkInfoLog = AllocateMemory( maxLength * SizeOf(uchar));
      glGetProgramInfoLog(*Me\pgm\pgm, maxLength, @maxLength, *pLinkInfoLog);
      ;MessageRequester("Error Setup Shader Program for InstanceCloud",PeekS(*pLinkInfoLog))
    EndIf
  
    glBindBuffer(#GL_ARRAY_BUFFER,0)
    glBindVertexArray(0)
    
  
    *Me\initialized = #True
  EndProcedure
  
  ; Update
  ;----------------------------------------------------
  Procedure Update(*Me.InstanceCloud_t)
    Debug "Update PointCloud"
    If *Me\stack
      Stack::Update(*Me\stack)
    EndIf
    
    If *Me\dirty & Object3D::#DIRTY_STATE_TOPOLOGY Or Not *Me\initialized
      Protected Me.Object3D::IObject3D = *Me
      Me\Setup(*Me\pgm)
    Else 
      If *Me\dirty & Object3D::#DIRTY_STATE_DEFORM
;         PointCloudGeometry::RecomputeNormals(*p\geom,1.0)
        glBindVertexArray(*Me\vao)
        glBindBuffer(#GL_ARRAY_BUFFER,*Me\vbo)
        BuildGLData(*Me)
        glBindBuffer(#GL_ARRAY_BUFFER,0)
        glBindVertexArray(0)
        *Me\dirty = Object3D::#DIRTY_STATE_CLEAN
      EndIf
    EndIf
   glCheckError("Update InstanceCloud")
    
    
  EndProcedure
  
  
  ; Draw
  ;----------------------------------------------------
  Procedure Draw(*Me.InstanceCloud_t)
   If *Me\initialized And *Me\visible

    glBindVertexArray(*Me\vao)

    glEnable(#GL_POINT_SMOOTH)
    
    Protected id.v3f32
    glPointSize(12 )
    Protected *geom.Geometry::PolymeshGeometry_t = *Me\geom
    Protected *shape.Shape::Shape_t = *Me\shape
;     Protected msg.s
;     Protected *v.v3f32
;     For i=0 To *geom\nbpoints-1
;       Debug i
;       *v = CArray::GetValue(*geom\a_positions,i)
;       
;       msg + StrF(*v\x)+","+StrF(*v\y)+","+StrF(*v\y)+","+Chr(10)
;     Next
;     
;     MessageRequester("FRAMEWORK",msg)
    
    ;glDrawElementsInstanced(	#GL_TRIANGLES,*Me\shape\nbt*3,#GL_UNSIGNED_INT,CArray::GetPtr(*Me\shape\indices,0),*geom\nbpoints);
    
     glDrawArraysInstanced(#GL_TRIANGLES,0,*Me\shape\nbt*3,*geom\nbpoints)
    glBindVertexArray(0)
    
  EndIf
  EndProcedure
  
  ; Set From Shape
  ;----------------------------------------------------
  Procedure SetFromShape(*Me.InstanceCloud_t,shape.i)

  EndProcedure
EndModule

  
    
    
; IDE Options = PureBasic 5.31 (Windows - x64)
; CursorPosition = 284
; FirstLine = 247
; Folding = --
; EnableUnicode
; EnableXP
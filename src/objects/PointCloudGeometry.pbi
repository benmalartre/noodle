XIncludeFile "../core/Math.pbi"
XIncludeFile "Geometry.pbi"

;========================================================================================
; PointCloudGeometry Module Declaration
;========================================================================================
DeclareModule PointCloudGeometry
  UseModule Math
  UseModule Geometry
  Declare New(nbp.i)
  Declare Delete(*geom.PointCloudGeometry_t)
  Declare Init(*geom.PointCloudGeometry_t)
  Declare Update(*geom.PointCloudGeometry_t)
  Declare PointsOnSphere(*geom.PointCloudGeometry_t)
  Declare PointsOnLine(*geom.PointCloudGeometry_t,*start.v3f32,*end.v3f32)
  Declare RandomizeColor(*geom.Geometry::PointCloudGeometry_t,*base.c4f32 = #Null,randomize.f = 0.5)
EndDeclareModule

;========================================================================================
; PointCloudGeometry Module Implementation
;========================================================================================
Module PointCloudGeometry
  UseModule Geometry
  UseModule Math
  ; Constructor
  ;-----------------------------------------------------------
  Procedure New(nbp.i)
    Protected *geom.PointCloudGeometry_t = AllocateMemory(SizeOf(PointCloudGeometry_t))
    *geom\nbpoints = nbp
    
    *geom\a_positions = CArray::newCArrayV3F32()
    *geom\a_velocities = CArray::newCArrayV3F32()
    *geom\a_normals = CArray::newCArrayV3F32()
    *geom\a_tangents = CArray::newCArrayV3F32()
    *geom\a_color = CArray::newCArrayC4F32()
    *geom\a_indices = CArray::newCArrayLong()
    *geom\a_scale = CArray::newCArrayV3F32()
    *geom\a_size = CArray::newCArrayFloat()
    *geom\a_uvws = CArray::newCArrayV3F32()
    
    CArray::SetCount(*geom\a_positions,nbp)
    CArray::SetCount(*geom\a_velocities,nbp)
    CArray::SetCount(*geom\a_normals,nbp)
    CArray::SetCount(*geom\a_tangents,nbp)
    CArray::SetCount(*geom\a_color,nbp)
    CArray::SetCount(*geom\a_indices,nbp)
    CArray::SetCount(*geom\a_scale,nbp)
    CArray::SetCount(*geom\a_size,nbp)
    CArray::SetCount(*geom\a_uvws,nbp)
    
    Init(*geom)
    ProcedureReturn *geom
  EndProcedure
  
  ; Destructor
  ;-----------------------------------------------------------
  Procedure Delete(*geom.PointCloudGeometry_t)
    CArray::Delete(*geom\a_positions )
    CArray::Delete(*geom\a_velocities )
    CArray::Delete(*geom\a_normals)
    CArray::Delete(*geom\a_tangents )
    CArray::Delete(*geom\a_color)
    CArray::Delete(*geom\a_indices)
    CArray::Delete(*geom\a_scale)
    CArray::Delete(*geom\a_size)
    CArray::Delete(*geom\a_uvws)
    FreeMemory(*geom)
  EndProcedure
  
  ; Init
  ;-----------------------------------------------------------
  Procedure Init(*geom.PointCloudGeometry_t)
    Protected i
    Protected *pos.v3f32,*norm.v3f32,*tan.v3f32
    Protected *col.c4f32
    Protected size.f = 1.0
    For i=0 To *geom\nbpoints-1
      *pos = CArray::GetValue(*geom\a_positions,i)
      Vector3::Set(*pos,(Random(100)*0.01-0.5)*10000,(Random(100)*0.01-0.5)*10000,(Random(100)*0.01-0.5)*10000)
      *norm = CArray::GetValue(*geom\a_normals,i)
      Vector3::Set(*norm,0,1,0)
      *tan = CArray::GetValue(*geom\a_tangents,i)
      Vector3::Set(*tan,0,0,1)
      size = 2
      CArray::SetValueF(*geom\a_size,i,size)
      *col = CArray::GetValue(*geom\a_color,i)
      Color::RandomLuminosity(*col)
    Next
    
  EndProcedure
  
  ; Update
  ;-----------------------------------------------------------
  Procedure Update(*geom.PointCloudGeometry_t)
    
  EndProcedure
  
  ; Points  On Sphere
  ;-----------------------------------------------------------
  Procedure PointsOnSphere(*geom.PointCloudGeometry_t)
    
    CArray::SetCount(*geom\a_positions,*geom\nbpoints)
    CArray::SetCount(*geom\a_normals,*geom\nbpoints)
    CArray::SetCount(*geom\a_tangents,*geom\nbpoints)
    CArray::SetCount(*geom\a_color,*geom\nbpoints)
    CArray::SetCount(*geom\a_scale,*geom\nbpoints)
    CArray::SetCount(*geom\a_size,*geom\nbpoints)
    
    Protected i
    Protected v.v3f32
    Protected c.c4f32
    Protected s.v3f32
    Protected t.v3f32
    
    Vector3::Set(@s,1,1,1)
    
    Define.f r,g,b, x,y ,z
    
    For i=0 To *geom\nbPoints-1
      ;Set Position
      x = Random(255)/255 - 0.5
      y = Random(255)/255 - 0.5
      z = Random(255)/255 - 0.5
      
      Vector3::Set(@v,x,y,z)
      Vector3::NormalizeInPlace(@v)
      Vector3::ScaleInPlace(@v,3)
      
      CArray::SetValue(*geom\a_positions,i,@v)

      ; Set Normals
      Vector3::NormalizeInPlace(@v)
      CArray::SetValue(*geom\a_normals,i,@v)
      
      ; Set Tangents
      Vector3::Set(@c,0,1,0)
      Vector3::Cross(@t,@v,@c)
      CArray::SetValue(*geom\a_tangents,i,@t)

      ; Set Color
      r = (120+Random(50))/255
      g = (20+Random(5))/255
      b = (10+Random(4))/255
      Color::Set(@c,r,g,b,1.0)
      CArray::SetValue(*geom\a_color,i,@c)

      ; Set Scale
      Vector3::Set(@s,0.1,0.1,0.1)
      CArray::SetValue(*geom\a_scale,i,@s)
      
      ; Set Size
      CArray::SetValueF(*geom\a_size,i,1)
      
    Next 

  EndProcedure
  
  ; Points  On Sphere
  ;-----------------------------------------------------------
  Procedure PointsOnLine(*geom.PointCloudGeometry_t,*start.v3f32,*end.v3f32)
    
    CArray::SetCount(*geom\a_positions,*geom\nbpoints)
    CArray::SetCount(*geom\a_normals,*geom\nbpoints)
    CArray::SetCount(*geom\a_tangents,*geom\nbpoints)
    CArray::SetCount(*geom\a_color,*geom\nbpoints)
    CArray::SetCount(*geom\a_scale,*geom\nbpoints)
    CArray::SetCount(*geom\a_size,*geom\nbpoints)
    
    Protected i
    Protected *v.v3f32
    Protected *c.c4f32
    Protected *s.v3f32
    Protected *t.v3f32
    Protected tmp.v3f32
    
    Vector3::Set(@s,1,1,1)
    
    Define.f r,g,b, x,y ,z
    Define delta.v3f32
    Vector3::Sub(@delta,*end,*start)
    
    Define l.f = Vector3::Length(@delta)
    Define st.f = l/(*geom\nbPoints-1.0)
    Define stc.f
    
    For i=0 To *geom\nbPoints-1
      ; step
      stc = st * i / l
      
      ;Set Position
      *v = CArray::GetValue(*geom\a_positions,i)
      Vector3::LinearInterpolate(*v,*start,*end,stc)
      
      ; Set Normal
      *v = CArray::GetValue(*geom\a_normals,i)
      Vector3::NormalizeInPlace(*v)
      
      ; Set Tangent
      *t = CArray::GetValue(*geom\a_tangents,i)
      Vector3::Set(*t,0,1,0)
      Vector3::Cross(@tmp,*v,*t)
      Vector3::SetFromOther(*t,@tmp)
      
      ;Set Color
      *c = CArray::GetValue(*geom\a_color,i)
      r = (120+Random(50))/255
      g = (20+Random(5))/255
      b = (10+Random(4))/255
      Color::Set(*c,r,g,b,1.0)

      ;Set Scale
      *s = CArray::GetValue(*geom\a_scale,i)
      Vector3::Set(*s,0.1,0.1,0.1)
      
      ;Set Size
      CArray::SetValueF(*geom\a_size,i,1)
      
    Next 
  EndProcedure
  
  ; Randomize Colors
  ;----------------------------------------------
  Procedure RandomizeColor(*geom.Geometry::PointCloudGeometry_t,*base.c4f32 = #Null,randomize.f = 0.5)
    Protected i.i
    Protected *c.c4f32
    Protected r.f,g.f,b.f,a.f

    If *base = #Null
      Protected base.c4f32
      Color::Set(@base,0.5,0.5,0.5,1.0)
      *base = @base
    EndIf
    
    For i=0 To CArray::GetCount(*geom\a_color)-1
      *c = CArray::GetValue(*geom\a_color,i)
      r = (Random(255)/255 - 0.5) * randomize
      g = (Random(255)/255 - 0.5) * randomize
      b = (Random(255)/255 - 0.5) * randomize
      a = 1
      Color::Set(*c,r,g,b,a)
    Next
  EndProcedure

EndModule
; IDE Options = PureBasic 5.42 LTS (MacOS X - x64)
; CursorPosition = 83
; FirstLine = 60
; Folding = --
; EnableXP
XIncludeFile "../core/Array.pbi"
XIncludeFile "../core/Utils.pbi"
XIncludeFile "../objects/Geometry.pbi"

;========================================================================================
; Topology Module Declaration
;========================================================================================
DeclareModule Topology
  UseModule Geometry
  UseModule Math
  Declare New(*other.Topology_t = #Null)
  Declare Delete(*Me.Topology_t)
  Declare Set(*topo.Topology_t,*vertices.CArray::CArrayV3F32,*faces.CArray::CArrayLong)
  Declare Copy(*topo.Topology_t,*other.Topology_t)
  Declare Transform(*topo.Topology_t,*m.m4f32)
  Declare TransformArray(*topo.Topology_t,*matrices.CArray::CArrayM4F32,*topo_array.CArray::CArrayPtr)
  Declare Merge(*o.Topology_t,*t1.Topology_t,*t2.Topology_t)
  Declare MergeInPlace(*t.Topology_t,*o.Topology_t)
  Declare MergeArray(*o.Topology_t,*topos.CArray::CArrayPtr)
  Declare Extrusion(*topo.Topology_t,*points.CArray::CArrayM4F32,*section.CArray::CArrayV3F32)
EndDeclareModule

;========================================================================================
; Topology Module Implementation
;========================================================================================
Module Topology
  UseModule Geometry
  
  ; Destuctor
  ;------------------------------------------------------------------
  Procedure Delete(*Me.Topology_t)
    CArray::Delete(*Me\vertices)
    CArray::Delete(*Me\faces)
    ClearStructure(*Me,Topology_t)
    FreeMemory(*Me)
  EndProcedure
  
  
  
  ;  Constructor
  ;---------------------------------------------
  Procedure.i New(*other.Topology_t = #Null)
    ; ---[ Allocate Memory ]----------------------------------------------------
    Protected *Me.Topology_t = AllocateMemory(SizeOf(Topology_t))
    InitializeStructure(*Me,Topology_t)
    If *other = #Null
      *Me\vertices = CArray::newCArrayV3F32()
      *Me\faces = CArray::newCArrayLong()
    Else
      *Me\vertices = CArray::newCArrayV3F32()
      CArray::SetCount(*Me\vertices,CArray::GetCount(*other\vertices))
      CArray::Copy(*Me\vertices,*other\vertices)
      *Me\faces = CArray::newCArrayLong()
      CArray::SetCount(*Me\faces,Carray::GetCount(*other\faces))
      CArray::Copy(*Me\faces,*other\faces)
    EndIf
    
    ProcedureReturn *Me
  EndProcedure
  
  ; ----------------------------------------------------------------------------
  ;  Set
  ; ----------------------------------------------------------------------------
  ;{
  Procedure Set(*topo.Topology_t,*vertices.CArray::CArrayV3F32,*faces.CArray::CArrayLong)
    Protected size_t.i
    size_t = CArray::GetCount(*vertices) * CArray::GetItemSize(*vertices)
  
    CArray::SetCount(*topo\vertices,CArray::GetCount(*vertices))
    CArray::SetCount(*topo\faces,CArray::GetCount(*faces))
    
    CArray::Copy(*topo\vertices,*vertices)
    CArray::Copy(*topo\faces,*faces)
   
  EndProcedure
  ;}
  
  ; ----------------------------------------------------------------------------
  ;  Copy
  ; ----------------------------------------------------------------------------
  ;{
  Procedure Copy(*topo.Topology_t,*other.Topology_t)
    Set(*topo.Topology_t,*other\vertices,*other\faces)
  EndProcedure
  ;}
  
  ; ----------------------------------------------------------------------------
  ;  Transform
  ; ----------------------------------------------------------------------------
  ;{
  Procedure Transform(*topo.Topology_t,*m.m4f32)
    Protected p.v3f32
    Protected i
    For i=0 To CArray::GetCount(*topo\vertices)
      Vector3::SetFromOther(@p,CArray::GetValue(*topo\vertices,i))
      Vector3::MulByMatrix4InPlace(@p,*m)
      CArray::SetValue(*topo\vertices,i,@p)
    Next
  EndProcedure
  ;}
  
  ; ----------------------------------------------------------------------------
  ;  Transform Array
  ; ----------------------------------------------------------------------------
  ;{
  Procedure TransformArray(*topo.Topology_t,*matrices.CArray::CArrayM4F32,*topo_array.CArray::CArrayPtr)
    Protected *t.Topology_t
    Protected *m.m4f32
    Protected i
    For i=0 To CArray::GetCount(*matrices)-1
      *t = New(*topo)
      Transform(*t,CArray::GetValue(*matrices,i))
      CArray::Append(*topo_array,*t)
    Next i
    
  EndProcedure
  ;}
  
  
  ;---------------------------------------------------------
  ; Merge
  ;---------------------------------------------------------
  ;{
  Procedure Merge(*o.Topology_t,*t1.Topology_t,*t2.Topology_t)
    Protected f1 = CArray::GetCount(*t1\faces)
    Protected f2 = CArray::GetCount(*t2\faces)
    Protected v1 = CArray::GetCount(*t1\vertices)
    Protected v2 = CArray::GetCount(*t2\vertices)
    Protected v.v3f32
    Protected f.i
    Protected i
    
    ;Reallocate Memory
    CArray::SetCount(*o\vertices,v1+v2)
    CArray::SetCount(*o\faces,f1+f2)
  
    CopyMemory(CArray::GetPtr(*t1\vertices,0),CArray::GetPtr(*o\vertices,0),v1*SizeOf(v))
    CopyMemory(CArray::GetPtr(*t2\vertices,0),CArray::GetPtr(*o\vertices,0)+v1*SizeOf(v),v2*SizeOf(v))
    
    CopyMemory(CArray::GetPtr(*t1\faces,0),CArray::GetPtr(*o\faces,0),f1*SizeOf(f))
    Protected x
    For i=0 To f2-1
      x = PeekL(CArray::GetValue(*t2\faces,i))
      If x>-2
        CArray::SetValueL(*o\faces,i+f1,x+v1)
      Else
        CArray::SetValueL(*o\faces,i+f1,-2)
      EndIf
      
    Next
   
  EndProcedure
  ;}
  
  ;---------------------------------------------------------
  ; Merge In Place
  ;---------------------------------------------------------
  ;{
  Procedure MergeInPlace(*t.Topology_t,*o.Topology_t)
    Protected f1 = CArray::GetCount(*t\faces)
    Protected f2 = CArray::GetCount(*o\faces)
    Protected v1 = CArray::GetCount(*t\vertices)
    Protected v2 = CArray::GetCount(*o\vertices)
    Protected v.v3f32
    Protected f.i,i.i
    
    ;Reallocate Memory
    CArray::SetCount(*t\vertices,v1+v2)
    CArray::SetCount(*t\faces,f1+f2)
  
    CopyMemory(CArray::GetPtr(*o\vertices,0),CArray::GetPtr(*t\vertices,v1),v2*SizeOf(v))
    If f1>0
      For i=0 To f2-1
        f= PeekL(CArray::GetValue(*o\faces,i+f1))
        If f >-2
          f+f1
          CArray::SetValueL(*o\faces,i+f1,f)
        Else 
            CArray::SetValueL(*o\faces,i+f1,-2)
        EndIf
      Next
    Else
      CArray::Copy(*t\faces,*o\faces)
    EndIf
    
    
  EndProcedure
  ;}
  
  ;---------------------------------------------------------
  ; Merge Array
  ;---------------------------------------------------------
  ;{
    Procedure MergeArray(*o.Topology_t,*topos.CArray::CArrayPtr)
    Protected nbt = CArray::GetCount(*topos)
    
    Protected Dim v_offsets(nbt)
    Protected Dim f_offsets(nbt)
    Protected Dim f_counts(nbt)
    Protected v_offset
    Protected f_offset
    Protected t
    Protected *topo.Topology_t
    Protected v.v3f32
    Protected f.i
    Protected i
    For t=0 To nbt-1
      *topo = CArray::GetValue(*topos ,t)
      v_offsets(t) = v_offset
      f_offsets(t) = f_offset
      f_counts(t) = CArray::GetCount(*topo\faces)
      v_offset + CArray::GetCount(*topo\vertices)
      f_offset + CArray::GetCount(*topo\faces)
    Next
    
    ;Reallocate Memory
    CArray::SetCount(*o\vertices,v_offset)
    CArray::SetCount(*o\faces,f_offset)
  
    For t=0 To 0;nbt-1
      *topo = CArray::GetValue(*topos,t)
      CopyMemory(CArray::GetPtr(*topo\vertices,0),*o\vertices+v_offsets(t)*SizeOf(v),CArray::GetCount(*topo\vertices)*SizeOf(v))
      If t=0
        CopyMemory(CArray::GetPtr(*topo\faces,0),CArray::GetPtr(*o\faces,0)+f_offsets(t)*SizeOf(f),CArray::GetCount(*topo\faces)*SizeOf(f))
      Else
        For i=0 To f_counts(i)-1
          f = PeekL(CArray::GetValue(*topo\faces,i))
          If f>-2
            CArray::SetValueL(*o\faces,i+f_offsets(t),f+v_offsets(i))
          Else
            CArray::SetValueL(*o\faces,i+f_offsets(t),-2)
          EndIf
          
        Next
        
      EndIf
  
    Next
  
  EndProcedure
  ;}
  
  ; ----------------------------------------------------------------------------
  ;  Extrusion
  ; ----------------------------------------------------------------------------
  ;{
  Procedure Extrusion(*topo.Topology_t,*points.CArray::CArrayM4F32,*section.CArray::CArrayV3F32)
    If CArray::GetCount(*points)<2 Or CArray::GetCount(*section)<2: ProcedureReturn : EndIf
    
   
    Protected p.v3f32
    Protected is,ip
    Protected *oP.CArray::CArrayV3F32 = CArray::newCArrayV3F32()
    CArray::Copy(*oP,*section)
    Protected so.i = CArray::GetCount(*section)
    Protected *extrusion.Topology_t = Topology::New()
  
    Protected offset = 0
    Protected cnt = 0
    Protected indices.s
    Protected *m.m4f32
    CArray::SetCount(*extrusion\vertices,0)
    CArray::SetCount(*extrusion\faces,0)
    For ip=0 To CArray::GetCount(*points)-1
      *m = CArray::GetValue(*points,ip)
      ;Matrix4_TransposeInPlace(*m)
      Utils::TransformPositionArray(*oP,*section,*m)
  
      CArray::AppendArray(*extrusion\vertices,*oP)
      If ip>0
        For is=0 To CArray::GetCount(*section)-2
          CArray::AppendL(*extrusion\faces,cnt+is)
          CArray::AppendL(*extrusion\faces,cnt+is+1)
          CArray::AppendL(*extrusion\faces,cnt+is+1+so)
          CArray::AppendL(*extrusion\faces,cnt+is+so)
          CArray::AppendL(*extrusion\faces,-2)
        Next
        cnt+so
      EndIf
    Next
    
    MergeInPlace(*topo,*extrusion)
    Delete(*extrusion)
    CArray::Delete(*oP)
    
  EndProcedure
  ;}
 
  
  
EndModule


; IDE Options = PureBasic 5.31 (Windows - x64)
; CursorPosition = 2
; Folding = ----
; EnableXP
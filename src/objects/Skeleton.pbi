XIncludeFile "../core/Math.pbi"
XIncludeFile "../core/Array.pbi"
XIncludeFile "../libs/OpenGL.pbi"
XIncludeFile "../libs/OpenGLExt.pbi"
XIncludeFile "../objects/Shapes.pbi"
XIncludeFile "../objects/Object3D.pbi"

;================================================================
; Bone Module Declaration
;================================================================
DeclareModule Bone
  UseModule Math
  Structure Bone_t
    localtransform.Transform::Transform_t
    globaltransform.Transform::Transform_t
    statictransform.Transform::Transform_t
    List *children.Bone_t()
    *parent.Bone_t
    ID.i
    name.s
  EndStructure
  
  Declare New(name.s,ID.i,*pos.v3f32,*ori.q4f32,*scl.v3f32,*parent.Bone::Bone_t = #Null)
  Declare Delete(*bone.Bone_t)
  Declare DeleteBranch(*bone.Bone_t)
  Declare ResetStaticKineState(*bone.Bone_t)
  Declare Update(*bone.Bone_t)
EndDeclareModule

;================================================================
; Skeleton Module Declaration
;================================================================
DeclareModule Skeleton
  UseModule Math
  UseModule OpenGL
  UseModule OpenGLExt
  Structure Skeleton_t
    *cloud.InstanceCloud::InstanceCloud_t
    List *roots.Bone::Bone_t()
    List *bones.Bone::Bone_t()
    nbbones.i
  EndStructure
  
EndDeclareModule

;================================================================
; Bone Module Implementation
;================================================================
Module Bone
  UseModule Math
  ; Constructor
  ;--------------------------------------------------------------
  Procedure New(name.s,ID.i,*pos.v3f32,*ori.q4f32,*scl.v3f32,*parent.Bone::Bone_t = #Null)
    Protected *bone.Bone_t = AllocateMemory(SizeOf(Bone_t))
    InitializeStructure(*bone,Bone_t)
    *bone\name = name
    *bone\ID = ID
    *bone\parent = parent
    If *bone\parent
      AddElement(*bone\parent\children())
      *bone\parent\children() = *bone
    EndIf
    
    Transform::Set(*bone\globaltransform,*scl,*ori,*pos)
    Transform::ComputeLocal(*bone\localtransform,*bone\globaltransform,*parent\globaltransform)
    Transform::Set(*bone\statictransform,*scl,*ori,*pos)
    ProcedureReturn *bone
  EndProcedure
  
  ; Destructor
  ;--------------------------------------------------------------
  Procedure Delete(*bone.Bone_t)
    Protected *parent.Bone_t = *bone\parent
    
    If ListSize(*bone\children())
      ForEach *bone\children()
        If *parent
          AddElement(*parent\children())
          *parent\children() = *bone\children()
        Else
          *bone\children()\parent = #Null
        EndIf
      Next
    EndIf
    ClearStructure(*bone,Bone_t)
    FreeMemory(*bone)
    
  EndProcedure
  
  ; Destructor Branch
  ;--------------------------------------------------------------
  Procedure DeleteBranch(*bone.Bone_t)
    If ListSize(*bone\children())
      ForEach *bone\children()
        DeleteBranch(*bone\children())
      Next
    EndIf
    Delete(*bone)
  EndProcedure
  
  ; Reset Static Kinematic State
  ;--------------------------------------------------------------
  Procedure ResetStaticKineState(*bone.Bone_t)
    Transform::SetFromOther(*bone\statictransform,*bone\globaltransform)
  EndProcedure
  
  ; Update
  ;--------------------------------------------------------------
  Procedure Update(*bone.Bone_t)
    
  EndProcedure
  
  
EndModule

;================================================================
; Skeleton Module Implementation
;================================================================
Module Skeleton
  
EndModule

; IDE Options = PureBasic 5.42 LTS (Windows - x64)
; CursorPosition = 5
; FirstLine = 1
; Folding = --
; EnableXP
XIncludeFile "Object3D.pbi"


DeclareModule Model
  UseModule Math
  Structure Model_t Extends Object3D::Object3D_t 
  EndStructure
  
  Interface IModel Extends Object3D::IObject3D
  EndInterface
  
  Declare New(name.s)
  Declare Delete(*model.Model_t)
  Declare Setup(*model.Model_t,*pgm)
  Declare Update(*model.Model_t)
  Declare Draw(*model.Model_t)
EndDeclareModule

Module Model
  
  Procedure New(name.s)
    Protected *Me.Model_t = AllocateMemory(SizeOf(Model_t))
    InitializeStructure(*Me,Model_t)
    *Me\name = name
    *Me\type = Object3D::#Object3D_Model
    Matrix4::SetIdentity(*Me\matrix)
    ProcedureReturn *Me
  EndProcedure
  
  Procedure Delete(*model.Model_t)
    ClearStructure(*Me,Model_t)
    FreeMemory(*Me)
  EndProcedure
  
  Procedure Setup(*model.Model_t,*pgm)
    If Not *model
      MessageRequester("[MODEL]","INVALID MODEL")
      ProcedureReturn 
    EndIf
      
    Protected i
    Protected *child.Object3D::Object3D_t
    Protected child.Object3D::IObject3D
    Protected *geom.Geometry::Geometry_t
    
    ForEach *model\children()
      *child = *model\children()
      child = *child
      *geom = *child\geom
      Debug "[Model] Setup "+*child\name+" ---> "+Str(*geom\nbpoints)
      child\Setup(*pgm)
      Debug "[model] Done!!"
    Next
    
  EndProcedure
  
  Procedure Update(*model.Model_t)
    Protected i
    Protected *child.Object3D::Object3D_t
    Protected child.Object3D::IObject3D
    Protected *geom.Geometry::Geometry_t
    ForEach *model\children()
      *child = *model\children()
      child = *child
      *geom = *child\geom
      Debug "[Model] Update "+*child\name+" ---> "+Str(*geom\nbpoints)
      child\Update()
      Debug "[model] Done!!"
    Next
    
  EndProcedure
  
  Procedure Draw(*model.Model_t)
    Protected i
    Protected *child.Object3D::Object3D_t
    Protected child.Object3D::IObject3D
    ForEach *model\children()
      
      *child = *model\children()
      child = *child
      child\Draw()
      Debug "--------------------------> DRAW :: "+ *child\name
    Next
  EndProcedure
  
EndModule


; IDE Options = PureBasic 5.31 (Windows - x64)
; CursorPosition = 61
; FirstLine = 12
; Folding = --
; EnableXP
XIncludeFile "../core/Math.pbi"
XIncludeFile "../libs/OpenGL.pbi"
XIncludeFile "../libs/OpenGLExt.pbi"
XIncludeFile "Object3D.pbi"

DeclareModule Light
  UseModule Math
  Structure Light_t Extends Object3D::Object3D_t
    position.v3f32
    color.v3f32
    
    linear.f
    quadratic.f
  EndStructure
  
  Interface ILight Extends Object3D::IObject3D
  EndInterface
  
  Declare New()
  Declare Setup(*Me.Light_t,*pgm.Program::Program_t)
  Declare Update(*Me.Light_t)
  Declare Draw(*Me.Light_t)
  Declare Delete(*Me.Light_t)
  Declare PassToShader(*Me.Light_t,shader.i,ID.i)
  
  DataSection 
    LightVT: 
    Data.i @Delete()
    Data.i @Setup()
    Data.i @Update()
    Data.i @Draw()
  EndDataSection 
EndDeclareModule

Module Light
  UseModule OpenGL
  UseModule OpenGLExt
  ;-------------------------------------------------------------
  ; Constructor
  ;-------------------------------------------------------------
  Procedure New()
    Protected *Me.Light_t = AllocateMemory(SizeOf(Light_t))
    Vector3::Set(*Me\color,1,1,1)
    Vector3::Set(*Me\position,5,10,4)
    *Me\linear = 0.2
    *Me\quadratic = 0.5
    *Me\type = Object3D::#Object3D_Light
    ProcedureReturn *Me
    
  EndProcedure
  
  ;-------------------------------------------------------------
  ; Destructor
  ;-------------------------------------------------------------
  Procedure Delete(*Me.Light_t)
    FreeMemory(*Me)
  EndProcedure
  
  ;----------------------------------------------------------------------------
  ; Setup
  ;----------------------------------------------------------------------------
  Procedure Setup(*Me.Light_t,*pgm.Program::Program_t)

  EndProcedure
  
  ;----------------------------------------------------------------------------
  ; Update
  ;----------------------------------------------------------------------------
  Procedure Update(*Me.Light_t)

  EndProcedure
  
  ;----------------------------------------------------------------------------
  ; Draw
  ;----------------------------------------------------------------------------
  Procedure Draw(*Me.Light_t)

  EndProcedure
  
  ;-------------------------------------------------------------
  ; Pass to Shader
  ;-------------------------------------------------------------
  Procedure PassToShader(*Me.Light_t,shader.i,ID.i)
    glUniform3fv(glGetUniformLocation(shader,"lights[" + Str(ID) + "].position"), 1, *Me\position)
    glUniform3fv(glGetUniformLocation(shader,"lights[" + Str(ID) + "].color"), 1, *Me\color)
    glUniform1f(glGetUniformLocation(shader,"lights[" + Str(ID) + "].linear"), *Me\linear)
    glUniform1f(glGetUniformLocation(shader,"lights[" + Str(ID) + "].quadratic"), *Me\quadratic)
  EndProcedure
  
EndModule

; IDE Options = PureBasic 5.42 LTS (Windows - x64)
; CursorPosition = 3
; Folding = --
; EnableXP
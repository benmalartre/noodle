﻿; ============================================================================
;  Scene Module Declaration
; ============================================================================
XIncludeFile "../core/Math.pbi"
XIncludeFile "../core/Array.pbi"
XIncludeFile "../libs/OpenGL.pbi"
XIncludeFile "../libs/OpenGLExt.pbi"
XIncludeFile "../opengl/Shader.pbi"
XIncludeFile "Shapes.pbi"
XIncludeFile "Object3D.pbi"
XIncludeFile "Root.pbi"
XIncludeFile "PolymeshGeometry.pbi"


DeclareModule Scene
  UseModule OpenGL
  UseModule OpenGLExt
  UseModule Math
  
  Structure Scene_t
    filename.s
    root.Root::Root_t
    ;handle.Handle::Handle_t
    ;grid.Grid::Grid2D_t
    *models.CArray::CArrayPtr
    *objects.CArray::CArrayPtr
    *helpers.CArray::CArrayPtr
    dirty.b
    *lights.CArray::CArrayPtr
    *cameras.CArray::CArrayPtr
    *camera.Camera::Camera_t
    *selection.CArray::CArrayPtr
    *rayhit.Object3D::Object3D_t
    dirtycount.i
    
    nbpolygons.l
    nbtriangles.l
    nbvertices.l
    
    Map *m_objects.Object3D::Object3D_t()
    ;*sig_onchanged.CSlot
  EndStructure
  
  
  Interface IScene Extends Object3D::IObject3D
  EndInterface
  
  Declare New(name.s,shape.i)
  Declare Delete(*Me.Scene_t)
  Declare Setup(*Me.Scene_t,*shader.Program::Program_t)
  Declare Update(*Me.Scene_t)
  Declare Draw(*Me.Scene_t)
  
  DataSection 
    SceneVT: 
    Data.i @Delete()
    Data.i @Setup()
    Data.i @Update()
    Data.i @Draw()
  EndDataSection 
  
  Global *current_scene.Scene_t

  
EndDeclareModule

; ----------------------------------------------------------------------------
;  CScene Instance
; ----------------------------------------------------------------------------
; ---[ Singleton Object ]----------------------------

;}


; ============================================================================
;  HELPERS ( CScene )
; ============================================================================
Procedure OScene_ResolveUniqueName(*s.CScene_t,*o.C3DObject_t)

  Protected found = #False
  Protected i = 0
  Protected s.s = ""

  While Not found
    If *o\model <> #Null
      *o\fullname = *o\model\GetName()+"."+*o\name+s
    Else
      *o\fullname = *o\name+s
    EndIf
    
    If FindMapElement(*s\m_objects(),*o\fullname)
      i+1
      s = Str(i)
    Else
      AddMapElement(*s\m_objects(), *o\fullname ,#PB_Map_ElementCheck)
      
      *s\m_objects() = *o
      *o\name = *o\name+s
      found = #True
    EndIf  
  Wend
EndProcedure

Procedure OScene_DeleteUniqueName(*s.CScene_t,*o.C3DObject_t)
  If FindMapElement(*s\m_objects(),*o\fullname)
    DeleteMapElement(*s\m_objects(),*o\fullname)
  EndIf
EndProcedure

; ============================================================================
;  IMPLEMENTATION ( CScene )
; ============================================================================
;{
Procedure OScene_SelectObject(*Me.CScene_t,*obj.C3DObject_t)
  Protected nbs = *Me\selection\GetCount()
  Protected s
  Protected *o.C3DObject_t
  For s = 0 To nbs-1
    *o = *Me\selection\GetValue(s)
    If *o\selected : *o\selected = #False : EndIf
  Next s
  
  *obj\selected = #True
  *Me\selection\SetCount(0)
  *Me\selection\Append(*obj)
  OHandle_SetTarget(*Me\handle,*obj)
EndProcedure

Procedure OScene_AddToSelection(*Me.CScene_t,*obj.CObject_t)
  *Me\selection\Append(*obj)  
EndProcedure

;-----------------------------------------------------------------
; Add Object To Scene Graph
;-----------------------------------------------------------------------
Procedure OScene_AddObject(*scn.CScene_t,*obj.C3DObject)
  
  OScene_ResolveUniqueName(*scn,*obj)
  
  Select *obj\GetType()
    Case #RAA_3DObject_Null
      *scn\helpers\AppendUnique(*obj)
    Case #RAA_3DObject_Model
      *scn\models\AppendUnique(*obj)
    Case #RAA_3DObject_Curve
      *scn\helpers\AppendUnique(*obj)
    Case #RAA_3DObject_Polymesh
      *scn\objects\AppendUnique(*obj)
    Case #RAA_3DObject_PointCloud
      *scn\objects\AppendUnique(*obj)
    Case #RAA_3DObject_Light 
      *scn\lights\AppendUnique(*obj)
    Case #RAA_3DObject_Camera
      *scn\cameras\AppendUnique(*obj)
  EndSelect

EndProcedure


;-------------------------------------------
; Add Object
;-----------------------------------------
Procedure OScene_AddObjectChildren(*Me.CScene_t,*obj.C3DObject_t,*model.CModel)

  ; Add Object Children to scene graph
  Protected i
  Protected *child.C3DObject
  Protected fullname.s
  For i=0 To *obj\children\GetCount()-1
    *child = *obj\children\GetValue(i)
    fullname = *model\GetName()+"|"+*child\GetName()

    OScene_AddObject(*Me,*child)
    

    
    OScene_AddObjectChildren(*Me,*child,*model)
    *Me\dirty = #True
    *Me\dirtycount+1
  Next i
  OScene_AddObject(*Me,*obj)
EndProcedure

Procedure OScene_AddModel(*Me.CScene,*model.CModel_t)
  If *model = #Null Or *Me = #Null
    ProcedureReturn
  EndIf

  *Me\AddChild(*model)
  
EndProcedure

Procedure OScene_AddChild(*Me.CScene_t,*obj.C3DObject_t)
  If *obj = #Null Or *Me = #Null
    ProcedureReturn
  EndIf
  
  *Me\root\AddChild(*obj)
  OScene_AddObject(*Me,*obj)
  
  Protected fullname.s = *obj\name

  *Me\m_objects(fullname) = *obj
  fullname = *obj\name
  
  ; Add Children to scene graph
  Protected i
  Protected *child.C3DObject
  For i=0 To *obj\children\GetCount()-1
    *child = *obj\children\GetValue(i)
    OScene_AddObjectChildren(*Me,*child,*obj)
  Next i
  *Me\dirty = #True
  *Me\dirtycount+1
EndProcedure

Procedure OScene_AddChildren(*Me.CScene_t,*children.CArrayPtr)
  
EndProcedure

Procedure OScene_AddModels(*obj.CArrayPtr)
  
EndProcedure

;-----------------------------------------------------------------
; Remove Object From Scene Graph
;-----------------------------------------------------------------------
Procedure OScene_RemoveObject(*Me.CScene_t,*obj.C3DObject)
  Protected id=-1
  
  OScene_DeleteUniqueName(*Me,*obj)
  Select *obj\GetType()
    Case #RAA_3DObject_Polymesh
      *Me\objects\Find(*obj,@id)
      If id>-1:*Me\objects\Remove(id):EndIf
    Case #RAA_3DObject_Model
    *Me\models\Find(*obj,@id)  
      If id>-1:*Me\models\Remove(id):EndIf
    Case #RAA_3DObject_Null
      *Me\helpers\Find(*obj,@id)
      If id>-1:*Me\helpers\Remove(id):EndIf
    Case #RAA_3DObject_Light
      *Me\lights\Find(*obj,@id)
      If id>-1:*Me\lights\Remove(id):EndIf
    Case #RAA_3DObject_Camera
      *Me\cameras\Find(*obj,@id)
      If id>-1:*Me\cameras\Remove(id):EndIf
    Case #RAA_3DObject_Curve
      *Me\helpers\Find(*obj,@id)
      If id>-1:*Me\helpers\Remove(id):EndIf
  EndSelect

EndProcedure


;-----------------------------------------------------------------
; Delete Object Children(recursive)
;-----------------------------------------------------------------------
Procedure OScene_DeleteObjectChildren(*Me.CScene_t,*obj.C3DObject_t)
  Protected c
  Protected *o.C3DObject = *obj
  Protected *child.C3DObject
  Protected id
  Protected nbc = *obj\children\GetCount()
  If *obj\children\GetCount() >0
    
    For c=0 To *obj\children\GetCount()-1
      *child = *obj\children\GetValue(c)
      OScene_RemoveObject(*Me,*child)
      OScene_DeleteObjectChildren(*Me,*child)
    Next c
 EndIf
 OScene_RemoveObject(*Me,*obj)
 *o\InstanceDestroy()
  
  
EndProcedure

;-----------------------------------------------------------------
; Delete Object
;-----------------------------------------------------------------------
Procedure OScene_DeleteObject(*Me.CScene_t,*obj.C3DObject_t)
  CHECK_PTR1_NULL(*Me)
  CHECK_PTR1_NULL(*obj)
  
  Protected id = -1
  Protected *parent.C3DObject_t = *obj\parent
  If *parent
    *parent\children\Find(*obj,@id)
  EndIf
  Debug "Scene Nb Objects : "+Str(*Me\objects\GetCount())
  OScene_DeleteObjectChildren(*Me,*obj)
  
  Debug "Scene Delete Object ------------->" +*obj\name
  Debug "Scene Nb Objects : "+Str(*Me\objects\GetCount())
  If id>-1 And *parent
    *parent\children\Remove(id)
  EndIf
  
  Protected sig.CSlot = *Me\sig_onchanged
  sig\Trigger( #RAA_SIGNAL_TYPE_PING, #Null )
  
EndProcedure


Procedure OScene_ParentObject(*obj.C3DObject_t,*parent.C3DObject_t)
  Protected obj.C3DObject = *obj
  Protected parent.C3DObject = *parent
  Protected old.C3DObject = *obj\parent
  old\RemoveChild(*obj)
  parent\AddChild(*obj)
  
EndProcedure

Procedure OScene_CutObject(*obj.C3DObject_t)
  
EndProcedure

;-----------------------------------------------------------------
; Setup Objects in GL Context
;-----------------------------------------------------------------------
Procedure OScene_SetupChildren(*scn.CScene_t,*obj.C3DObject_t,*ctx.GLContext_t)
  Protected j

  For j=0 To *obj\children\GetCount()-1
    Define *child.C3DObject = *obj\children\GetValue(j)
    If Not *child\Initialized()
      Protected *c.C3DObject_t = *child
      *child\Setup(*ctx)
    EndIf
    OScene_SetupChildren(*scn,*child,*ctx)
    
  Next j

EndProcedure

Procedure OScene_Setup(*scn.CScene_t,*ctx.GLContext_t)
  If *ctx\useGLFW
    glfwMakeContextCurrent(*ctx\window)
  Else
    SetGadgetAttribute(*ctx\gadgetID,#PB_OpenGL_SetContext,#True)
  EndIf
  
  Protected i,j
  Protected *root.CRoot_t = *scn\root
  Protected *child.C3DObject
  Protected *model.CModel_t

  For i = 0 To *root\children\GetCount()-1
    *child = *root\children\GetValue(i)
    If Not *child\Initialized()
      *child\Setup(*ctx)
    EndIf
    
    OScene_SetupChildren(*scn,*child,*ctx)
  Next i
  
  ; Setup Handle
  OHandle_Setup(*scn\handle,*ctx)
EndProcedure

;-----------------------------------------------------------------
; Clean Objects in GL Context
;-----------------------------------------------------------------------
Procedure OScene_CleanChildren(*obj.C3DObject_t,*ctx.GLContext_t)
  Protected i,j
  Protected *child.C3DObject

  For j=0 To *obj\children\GetCount()-1
    *child = *obj\children\GetValue(j)
    Protected *ochild.C3DObject_t = *child
    
    *child\Clean(*ctx)
    OScene_CleanChildren(*child,*ctx)
  Next j
  
  Protected object.C3DObject = *obj
  object\Clean(*ctx)
  
EndProcedure

Procedure OScene_CleanObject(*scn.CScene_t,*ctx.GLContext_t,*obj.C3DObject_t)

  Protected i
  Protected *child.C3DObject
  
  For i = 0 To *obj\children\GetCount()-1
    *child = *obj\children\GetValue(i)
    OScene_CleanChildren(*child,*ctx)
  Next i
  
  Protected object.C3DObject = *obj
  object\Clean(*ctx)


EndProcedure

Procedure OScene_Clean(*scn.CScene_t,*ctx.GLContext_t)

  Protected i,j
  Protected *child.C3DObject
  Protected *model.CModel_t
  
  For i = 0 To *scn\models\GetCount()-1
    *model = *scn\models\GetValue(i)
    For j=0 To *model\children\GetCount()-1
      *child = *model\children\GetValue(j)
      *child\Clean(*ctx)
      OScene_CleanChildren(*child,*ctx)
    Next j
    
  Next i

EndProcedure
;-----------------------------------------------
; Update Children
;-----------------------------------------------
Procedure OScene_UpdateChildren(*obj.C3DObject_t,*ctx.GLContext_t)

  Protected i
  Protected *child.C3DObject = *obj
  Protected *local.CTransform
  For i = 0 To *obj\children\GetCount()-1
    *child = *obj\children\GetValue(i)
    If Not *child\Initialized()
      *child\Setup(*ctx)
    EndIf
    
    ;If *child\IsA(#RAA_3DObject_Camera) : Continue : EndIf
    *local = *child\GetLocalTransform()
    *child = *obj\children\GetValue(i)

    *child\UpdateTransform(*obj\global)
    *child\Update(*ctx)
    
    *local = *child\GetLocalTransform()
    OScene_UpdateChildren(*child,*ctx)
  Next i
  
EndProcedure
;-----------------------------------------------
; Update Scene
;-----------------------------------------------
;{
Procedure OScene_Update(*scn.CScene_t,*ctx.GLContext_t)
  If *scn\dirty
    Protected i
    If *ctx
      If *ctx \useGLFW
        glfwMakeContextCurrent(*ctx\window)
      Else
         SetGadgetAttribute(*ctx\gadgetID,#PB_OpenGL_SetContext,#True) 
      EndIf
    EndIf
    
    Protected *root.C3DObject_t = *scn\root
    Protected *child.C3DObject
    Protected *c.C3DObject_t
    For i=0 To *root\children\GetCount()-1
      *child = *root\children\GetValue(i)
      
      ;If *child\IsA(#RAA_3DObject_Camera) : Continue : EndIf
      
      *child\UpdateTransform(*root\global)
      *child\Update(*ctx)
  
      OScene_UpdateChildren( *child,*ctx)
    Next
    
    
    Protected sig.CSlot = *scn\sig_onchanged
    sig\Trigger( #RAA_SIGNAL_TYPE_PING, #Null )
    *scn\dirty = #False
  EndIf
  
EndProcedure
;}

;-----------------------------------------------
; Nb 3DObjects
;-----------------------------------------------
;{
Procedure OScene_GetNbObjects(*scn.CScene_t)
  ProcedureReturn *scn\objects\GetCount()
EndProcedure
;}

;-----------------------------------------------
; Nb Polygons
;-----------------------------------------------
;{
Procedure OScene_GetNbPolygons(*scn.CScene_t)
  If *scn\dirty
    *scn\nbpolygons=0
    Protected i
    Protected *o.C3DObject_t
    Protected *m.CPolymesh_t
    For i=0 To *scn\objects\GetCount()-1
      *o = *scn\objects\GetValue(i)
      If *o\type = #RAA_3DObject_Polymesh
        *m = *o
        *scn\nbpolygons + *m\geometry\GetNbPolygons()
      EndIf
    Next
  EndIf
  
  ProcedureReturn *scn\nbpolygons
EndProcedure
;}

;-----------------------------------------------
; Nb Triangles
;-----------------------------------------------
;{
Procedure OScene_GetNbTriangles(*scn.CScene_t)
  If *scn\dirty
    *scn\nbtriangles=0
    Protected i
    Protected *o.C3DObject_t
    Protected *m.CPolymesh_t
    For i=0 To *scn\objects\GetCount()-1
      *o = *scn\objects\GetValue(i)
      If *o\type = #RAA_3DObject_Polymesh
        *m = *o
        *scn\nbtriangles + *m\geometry\GetNbTriangles()
      EndIf
    Next
  EndIf
  
  ProcedureReturn *scn\nbtriangles
EndProcedure
;}
;-----------------------------------------------
; Get Active Camera
;-----------------------------------------------
;{
Procedure OScene_GetActiveCamera(*scn.CScene_t)
  If Not *scn\camera
    *scn\camera = *scn\cameras\GetValue(0)
  EndIf
  ProcedureReturn *scn\camera
EndProcedure
;}

;-----------------------------------------------
; Get Root
;-----------------------------------------------
;{
Procedure OScene_GetRoot(*scn.CScene_t)
  ProcedureReturn *scn\root
EndProcedure
;}


;-----------------------------------------------
; SignalOnChanged
;-----------------------------------------------
Procedure OScene_SignalOnChanged(*scn.CScene_t)
  ret( *scn\sig_onchanged )
EndProcedure

;-----------------------------------------------
; Save
;-----------------------------------------------
Procedure OScene_Save(*scn.CScene_t)
  Debug "------------------------ SAVE SCENE ------------------------------"
  Debug *scn
  Debug *scn\models\GetCount()
  Debug *scn\lights\GetCount()
  
  *scn\sig_onchanged\Trigger(#RAA_SIGNAL_TYPE_PING, #Null)
  Debug "Scene Save Called"
EndProcedure

;-----------------------------------------------
; SaveAs
;-----------------------------------------------
Procedure OScene_SaveAs(*scn.CScene_t, filename.s)
  Debug "Scene Save As Called"
EndProcedure

;-----------------------------------------------
; SaveAs
;-----------------------------------------------
Procedure OScene_New(*scn.CScene_t)
  Debug "Scene New Called"
EndProcedure

;-----------------------------------------------
; Select By ID 
;-----------------------------------------------
Procedure OScene_SelectByID(*scn.CScene_t,id.l)
  Protected i
  Protected *object.C3DObject_t
  Protected *out.C3DObject = #Null
  For i=0 To *scn\objects\GetCount()-1
    *object = *scn\objects\GetValue(i)
    If id = *object\uniqueID
      *object\selected = #True
      *out = *object
      Break
    EndIf 
  Next 
  
  ;Update others selectivity
  If *out<>#Null
    For i=0 To *scn\objects\GetCount()-1
      *object = *scn\objects\GetValue(i)
    If Not *object = *out
      *object\selected = #False
    EndIf 
  Next 
  EndIf
  
  ProcedureReturn *out
EndProcedure

;}

;_____________________________________________________________________________
;  Destructor
;¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯
;{
; ---[ _Free ]----------------------------------------------------------------
Procedure OScene_Free( *Me.CScene_t )

  Protected *model.CModel
  Protected i
  For i=0 To *Me\models\GetCount()-1
    *model = *Me\models\GetValue(i)
    *model\InstanceDestroy()
  Next
  
  *Me\root\InstanceDestroy()
  *Me\models\InstanceDestroy()
  *Me\objects\InstanceDestroy()
  
  OSlot_Release(*Me\sig_onchanged)
  
; ---[ Deallocate Memory ]--------------------------------------------------
  ClearStructure(*Me,CScene_t)
  FreeMemory( *Me )

EndProcedure
;}

;}


; ============================================================================
;  VTABLE ( CObject + CScene )
; ============================================================================
;{
DataSection
  ; C3DObject
  CObject_DAT (Scene)
  
  ; CCamera
  Data.i @OScene_AddChild()
  Data.i @OScene_AddChildren()
  Data.i @OScene_AddModel()
  Data.i @OScene_AddModels()
  Data.i @OScene_SelectObject()
  Data.i @OScene_DeleteObject()
  Data.i @OScene_Setup()
  Data.i @OScene_Update()
  Data.i @OScene_Save()
  Data.i @OScene_SaveAs()
  Data.i @OScene_New()
  Data.i @OScene_SignalOnChanged()
  Data.i @OScene_GetNbObjects()
  Data.i @OScene_GetNbPolygons()
  Data.i @OScene_GetNbTriangles()
  Data.i @OScene_GetActiveCamera()
  Data.i @OScene_GetRoot()
 
EndDataSection
;}

; ============================================================================
;  REFLECTION
; ============================================================================
;{
; ----------------------------------------------------------------------------
;  CScene Object
; ----------------------------------------------------------------------------
Class_DEF( Scene )
;}


; ============================================================================
;  CONSTRUCTORS
; ============================================================================
;{
; ---[ Stack ]----------------------------------------------------------------
Procedure.i nesCScene( *Me.CScene_t,name.s="Untitled-001")
  
  ; ---[ Sanity Check ]-------------------------------------------------------
  CHECK_PTR1_NULL( *Me )
  *Me\filename = name
  ; ---[ Init CObject Base Class ]--------------------------------------------
  CObject_INI( Scene )
  InitializeStructure(*Me,CScene_t)
  
  Protected Me.CScene = *Me
  
  ; ---[ Create Containers ]--------------------------------------------------
  *Me\models = newCArrayPtr()
  *Me\objects = newCArrayPtr()
  *Me\cameras = newCArrayPtr()
  *Me\lights = newCArrayPtr()
  *Me\selection = newCArrayPtr()
  *Me\helpers = newCArrayPtr()
  
  ; ---[ Create Root ]---------------------------------------------------------
  *Me\root = newCRoot("SceneRoot")
  
  ; ---[ Create Camera ]-------------------------------------------------------
  Protected *camera.CCamera_t = newCCamera("Camera",#RAA_Camera_Perspective)
  
  *Me\camera = *camera
  *Me\cameras\Append(*camera)
  Me\AddChild(*camera)
  
  ; ---[ Create Light ]--------------------------------------------------------
  Protected *light.CLight_t = newCLight("Light",#RAA_Light_Infinite)
  *Me\lights\Append(*light)
  Me\AddChild(*light)
  
  ; ---[ Create Handle ]-------------------------------------------------------
  *Me\handle = newCHandle()
  
  ; ---[ Initialize Slot ]-----------------------------------------------------
  *Me\sig_onchanged = newCSlot( *Me )
  
  ; ---[ Return Initialized Object ]------------------------------------------
  ret( *Me )
  
EndProcedure

; ---[ Heap ]-----------------------------------------------------------------
Procedure.i newCScene( name.s = "ActiveScene")
  
  ; ---[ Allocate Object Memory ]---------------------------------------------
  Protected *p.CScene_t = AllocateMemory( SizeOf(CScene_t) )
  
  ; ---[ Init Object ]--------------------------------------------------------
  ret( nesCScene( *p, name) )
  
EndProcedure
;}


; ============================================================================
;  EOF
; ============================================================================


Procedure CreateRandomNull()
  Protected *null.CNull = newCNull("Null")

  Protected q.q4f32
  Define *t.CTransform = *null\GetLocalTransform()
  Quaternion_SetFromAxisAngleValues(@q,0,1,0,Random(360))
  *t\SetRotationFromQuaternion(@q)
  *t\SetTranslationFromXYZValues(Random(10)-5,Random(10)-5,Random(10)-5)
  *t\SetScaleFromXYZValues(2,2,2)
  
  *null\SetGlobalTransform(*t)
  ProcedureReturn *null
EndProcedure


Procedure CreateChildNull(*parent.CNull)
  Protected *null.CNull = newCNull("Null")
  
  Protected q.q4f32
  Define *t.CTransform = *null\GetLocalTransform()
  Quaternion_SetFromAxisAngleValues(@q,0,1,0,Random(360))
  *t\SetRotationFromQuaternion(@q)
  *t\SetTranslationFromXYZValues(Random(10)-5,Random(10)-5,Random(10)-5)
  Protected s.f = Random(100)*0.01
  *t\SetScaleFromXYZValues(s,s,s)
  
  *null\SetGlobalTransform(*t)
  *parent\AddChild(*null)
  
  ProcedureReturn *null
EndProcedure

Procedure CreateRandomCube()
  Protected *cube.CPolymesh = newCPolymesh("Cube",#RAA_SHAPE_CUBE)
 
  Define *t.CTransform = *cube\GetGlobalTransform()
  Protected q.q4f32
  Quaternion_SetFromAxisAngleValues(@q,0,1,0,Random(360))
  Protected *q.q4f32 = *t\GetQuaternion()
  Quaternion_Set(*q,q\x,q\y,q\z,q\w)
   *t\SetScaleFromXYZValues(Random(255)/255+0.5,Random(255)/255+0.5,Random(255)/255+0.5)
  Quaternion_SetFromAxisAngleValues(*q,0,1,0,Random(360))
  *t\SetRotationFromQuaternion(*q)
  *t\SetTranslationFromXYZValues(Random(10),Random(10),Random(10))
  ;*t\SetTranslationFromXYZValues(Random(10)-5,Random(10)-5,Random(10)-5)
 

  
  *cube\SetGlobalTransform(*t)
  
  ProcedureReturn *cube
EndProcedure

Procedure CreateChildCube(*parent.CPolymesh)
  Protected *child.CPolymesh = newCPolymesh("Cube")
  OPolymeshGeometry_Cube(*child\GetGeometry(),1,1,1,1)
  
  Protected q.q4f32 
  Define *t.CTransform = *child\GetLocalTransform()
  
  Protected p.v3f32
  Vector3_Set(@p,1,0,0)
  
  Quaternion_SetFromAxisAngleValues(@q,0,1,0,Random(360))
  Vector3_MulByQuaternionInPlace(@p,@q)
  
  *t\SetTranslation(@p)
  *child\SetGlobalTransform(*t)
;   *t\SetTranslationFromXYZValues(0,0,0)
  *t\SetScaleFromXYZValues(0.1,0.1,0.1)
;   
;   *child\SetGlobalTransform(*t)
  *parent\AddChild(*child)

  ProcedureReturn *child
EndProcedure


Procedure TestInverseMatrix(*model.CModel)
  Define.CPolymesh *c1,*c2,*c3
  *c1 = CreateRandomCube()
  *c2 = CreateRandomCube()
  *c3 = CreateRandomCube()
  
  Define.CTransform_t *t1,*t2,*t3
  *t1 = *c1\GetGlobalTransform()
  *t2 = *c2\GetGlobalTransform()
  *t3 = *c3\GetGlobalTransform()
  
  Define.m4f32_b m1,m2
  Matrix4_Inverse(@m1,*t1\m)
  *t2\m = m1
  *c2\SetGlobalTransform(*t2)
  Matrix4_Inverse(@m2,@m1)
  *t3\m = m2
  *c3\SetGlobalTransform(*t3)
  
  *model\AddChild(*c1)
  *model\AddChild(*c2)
  *model\AddChild(*c3)
  
EndProcedure

; 
; Procedure OScene_BulletCross(*scene.CScene_t,*t.CTransform)
;   Define *cross.btCollisionShape = BTNewCompoundShape()
;   Define *trunk.btCollisionShape = BTNewBoxShape(1,20,1)
;   Define *arm.btCollisionShape = BTNewBoxShape(8,1,1)
;   Protected p.v3f32
;   Protected q.q4f32
;   Vector3_Set(@p,0,20,0)
;   Quaternion_SetFromAxisAngleValues(@q,0,1,0,0)
;   BTAddChildShape(*cross,*trunk,@p,@q)
;   
;   Vector3_Set(@p,0,14.5,0)
;   Quaternion_SetFromAxisAngleValues(@q,0,1,0,0)
;   BTAddChildShape(*cross,*arm,@p,@q)
;   
;   ;Protected *null.CNull = newCNull("Root")
;   Protected *m1.CPolymesh = newCPolymesh()
;   
;   Protected *mesh.CPolymesh = newCPolymesh("Cross",#RAA_SHAPE_CUBE,1.0)
;   
;   Protected *body.btRigidBody = BTCreateRigidBody(*mesh,1.0,*cross)
;   BTAddRigidBody(*raa_bullet_world,*body)
; EndProcedure


Procedure OScene_Test()
  Define *scene.CScene = newCScene()
  Define *root.CModel = newCModel("Model")
  *scene\AddModel(*root)
 
  
  Define *cube1.CPolymesh = CreateRandomCube()
  *root\AddChild(*cube1)
;   
  Define *cube2 = CreateChildCube(*cube1)
  Define *cube3 = CreateChildCube(*cube2)
  
  Define *null1.CNull = CreateRandomNull()
  *root\AddChild(*null1)
  Define *null2.CNull = CreateChildNull(*cube1)
  Define *null3.CNull = CreateChildNull(*root)
  
  
  
; Define *cloud.CPointCloud = newCPointCloud("Cloud",10000)
; OPointCloudGeometry_PointOnSphere(*cloud\GetGeometry())
;   *root\AddChild(*cloud)

  
  ProcedureReturn *scene
EndProcedure


Procedure OScene_CubeGrid()
  Define *scene.CScene = newCScene()
  Define *root.CModel = newCModel("Scene_Root")
  *scene\AddModel(*root)
  
  Protected x,y
  Protected *t.CTransform

  Protected *n.CPolymesh
  For x=0 To 10
    For y=0 To 10
      *n = newCPolymesh("Cube"+Str(x*10+y),#RAA_SHAPE_CUBE,0.0)
      *t = *n\GetGlobalTransform()
      *t\SetTranslationFromXYZValues(x-5,Random(2),y-5)
      *t\SetScaleFromXYZValues(0.5,0.5,0.5)
      *n\SetGlobalTransform(*t)
      *root\AddChild(*n)
    Next y
  Next x
;   
  Protected *ground.CPolymesh = newCPolymesh("Ground",#RAA_SHAPE_GRID,0.0)
   *t = *ground\GetGlobalTransform()
   *t\SetScaleFromXYZValues(20,1,20)
   *ground\SetGlobalTransform(*t)
   *root\AddChild(*ground)
   
   Protected *l.CLight
   For x=0 To 7
     *l = newCLight("Light",#RAA_Light_Point)
     *t = *l\GetGlobalTransform()
     *t\SetTranslationFromXYZValues(Random(10),Random(10),Random(10))
     *l\SetGlobalTransform(*t)
     *root\AddChild(*l)
   Next x 
   
   Protected *cloud.CPointCloud = newCPointCloud("PointCloud",7)
   OPointCloudGeometry_PointOnSphere(*cloud\GetGeometry())
   *t = *cloud\GetGlobalTransform()
   *t\SetTranslationFromXYZValues(0,7,0)
   *t\SetScaleFromXYZValues(1,1,1)
   *cloud\SetGlobalTransform(*t)
   *root\AddChild(*cloud)
    Protected *fat.CPolymesh = newCPolymesh("Fat",#RAA_SHAPE_SPHERE,0.0)
   *t = *fat\GetGlobalTransform()
   *t\SetTranslationFromXYZValues(0,1,0)
   *t\SetScaleFromXYZValues(1,1,1)
   *fat\SetGlobalTransform(*t)
   *root\AddChild(*fat)
;    
;    Protected *n2.CNull
;   For x=0 To 10
;     For y=0 To 10
;       *n2 = newCNull("Null"+Str(x*10+y))
;       *t = *n2\GetGlobalTransform()
;       *t\SetTranslationFromXYZValues(x-5,5,y-5)
;       *t\SetScaleFromXYZValues(1,1,1)
;       *n2\SetGlobalTransform(*t)
;       *root\AddChild(*n2)
;     Next y
;   Next x
  ProcedureReturn *scene
EndProcedure


Procedure OScene_NullHierarchy()
  Define *scene.CScene = newCScene()
  Define *root.CModel = newCModel("Scene_Root")
  *scene\AddModel(*root)
  
   Protected *t.CTransform
   Protected *n.CNull
   Protected *p.C3DObject
   Protected a,b,nbc
   Protected x.f,y.f
   
   For a=0 To 100
     nbc = Random(7)
     *p = *root
     For b=0 To nbc
       *n = newCNull("Null"+Str(a*3+b+1))
       *p\AddChild(*n)
       *t = *n\GetLocalTransform()
       x = a
       y = 1
        *t\SetTranslationFromXYZValues(x,y,0)
        *t\SetScaleFromXYZValues(1,1,1)
        *n\SetLocalTransform(*t)
        
        *p = *n
      Next b
      
    Next a
    
    ProcedureReturn *scene
   
  EndProcedure
  
; ----------------------------------------------------------------------------
;  Add Null At Position
; ----------------------------------------------------------------------------
;{
Procedure OScene_AddNullAtPos(*scene.CScene,*pos.v3f32)
  Protected *null.CNull = newCNull()
  Protected *t.CTransform = *null\GetGlobalTransform()
  *t\SetTranslationFromXYZValues(*pos\x,*pos\y,*pos\z)
  *t\SetScaleFromXYZValues(22,22,22)
  *null\SetGlobalTransform(*t)
  *scene\AddChild(*null)
  ProcedureReturn *null
EndProcedure
; IDE Options = PureBasic 5.42 LTS (MacOS X - x64)
; CursorPosition = 10
; Folding = ----------
; EnableUnicode
; EnableThread
; EnableXP
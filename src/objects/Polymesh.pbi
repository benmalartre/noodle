XIncludeFile "../core/Math.pbi"
XIncludeFile "../core/Array.pbi"
XIncludeFile "../libs/OpenGL.pbi"
XIncludeFile "../libs/OpenGLExt.pbi"
XIncludeFile "../opengl/Shader.pbi"
XIncludeFile "Shapes.pbi"
XIncludeFile "Object3D.pbi"
XIncludeFile "PolymeshGeometry.pbi"


DeclareModule Polymesh
  UseModule OpenGL
  UseModule OpenGLExt
  UseModule Math
  
  Structure Polymesh_t Extends Object3D::Object3D_t
    ;*shape.Shape::Shape_t
    deformdirty.b
    topodirty.b
  EndStructure
  
  Interface IPolymesh Extends Object3D::IObject3D
  EndInterface
  
  Declare New(name.s,shape.i)
  Declare Delete(*Me.Polymesh_t)
  Declare Setup(*Me.Polymesh_t,*shader.Program::Program_t)
  Declare Update(*Me.Polymesh_t)
  Declare Draw(*Me.Polymesh_t)
  Declare SetFromShape(*Me.Polymesh_t,shape.i)
  
  DataSection 
    PolymeshVT: 
    Data.i @Delete()
    Data.i @Setup()
    Data.i @Update()
    Data.i @Draw()
  EndDataSection 
  
EndDeclareModule

Module Polymesh
  UseModule OpenGL
  UseModule OpenGLExt

  ; Constructor
  ;----------------------------------------------------
  Procedure New(name.s,shape.i)
    Protected *Me.Polymesh_t = AllocateMemory(SizeOf(Polymesh_t))
    InitializeStructure(*Me,Polymesh_t)
    *Me\classname = "POLYMESH"
    *Me\name = name
    ;*Me\shape = Shape::New(shape)
    *Me\VT = ?PolymeshVT
    *Me\geom = PolymeshGeometry::New(shape)
    *Me\visible = #True
    *Me\stack = Stack::New()
    *Me\type = Object3D::#Object3D_Polymesh
    Matrix4::SetIdentity(*Me\matrix)
    
    ; ---[ Attributes ]---------------------------------------------------------
    Protected *mesh.Geometry::PolymeshGeometry_t = *Me\geom
    Object3D::Object3D_ATTR()
    ; Singleton Attributes
    Protected *nbp = Attribute::New("NbVertices",Attribute::#ATTR_TYPE_INTEGER,Attribute::#ATTR_STRUCT_SINGLE,Attribute::#ATTR_CTXT_SINGLETON,@*mesh\nbpoints,#True,#True)
    Object3D::AddAttribute(*Me,*nbp)
    Protected *nbe = Attribute::New("NbEdges",Attribute::#ATTR_TYPE_INTEGER,Attribute::#ATTR_STRUCT_SINGLE,Attribute::#ATTR_CTXT_SINGLETON,@*mesh\nbedges,#True,#True)
    Object3D::AddAttribute(*Me,*nbe)
    Protected *nbf = Attribute::New("NbPolygons",Attribute::#ATTR_TYPE_INTEGER,Attribute::#ATTR_STRUCT_SINGLE,Attribute::#ATTR_CTXT_SINGLETON,@*mesh\nbpolygons,#True,#True)
    Object3D::AddAttribute(*Me,*nbf)
    Protected *nbt = Attribute::New("NbTriangles",Attribute::#ATTR_TYPE_INTEGER,Attribute::#ATTR_STRUCT_SINGLE,Attribute::#ATTR_CTXT_SINGLETON,@*mesh\nbtriangles,#True,#True)
    Object3D::AddAttribute(*Me,*nbt)
    Protected *nbs = Attribute::New("NbSamples",Attribute::#ATTR_TYPE_INTEGER,Attribute::#ATTR_STRUCT_SINGLE,Attribute::#ATTR_CTXT_SINGLETON,@*mesh\nbsamples,#True,#True)
    Object3D::AddAttribute(*Me,*nbs)
    Protected *nbi = Attribute::New("NbIndices",Attribute::#ATTR_TYPE_INTEGER,Attribute::#ATTR_STRUCT_SINGLE,Attribute::#ATTR_CTXT_SINGLETON,@*mesh\nbindices,#True,#True)
    Object3D::AddAttribute(*Me,*nbi)
    
    ; Singleton Arrays
    Protected *fc = Attribute::New("FaceCount",Attribute::#ATTR_TYPE_INTEGER,Attribute::#ATTR_STRUCT_ARRAY,Attribute::#ATTR_CTXT_SINGLETON,*mesh\a_facecount,#True,#False)
    Object3D::AddAttribute(*Me,*fc)
    Protected *fi = Attribute::New("FaceIndices",Attribute::#ATTR_TYPE_INTEGER,Attribute::#ATTR_STRUCT_ARRAY,Attribute::#ATTR_CTXT_SINGLETON,*mesh\a_faceindices,#True,#False)
    Object3D::AddAttribute(*Me,*fi)
    
    ; Per Point Attributes
    Protected *pointposition = Attribute::New("PointPosition",Attribute::#ATTR_TYPE_VECTOR3,Attribute::#ATTR_STRUCT_SINGLE,Attribute::#ATTR_CTXT_COMPONENT0D,*mesh\a_positions,#False,#False)
    Object3D::AddAttribute(*Me,*pointposition)
    Protected *pointnormal = Attribute::New("PointNormal",Attribute::#ATTR_TYPE_VECTOR3,Attribute::#ATTR_STRUCT_SINGLE,Attribute::#ATTR_CTXT_COMPONENT0D,*mesh\a_pointnormals,#False,#False)
    Object3D::AddAttribute(*Me,*pointnormal)
    Protected *pointvelocity = Attribute::New("PointVelocity",Attribute::#ATTR_TYPE_VECTOR3,Attribute::#ATTR_STRUCT_SINGLE,Attribute::#ATTR_CTXT_COMPONENT0D,*mesh\a_velocities,#False,#False)
    Object3D::AddAttribute(*Me,*pointvelocity)
    
    ; Per Sample Attributes
    Protected *normals = Attribute::New("Normals",Attribute::#ATTR_TYPE_VECTOR3,Attribute::#ATTR_STRUCT_SINGLE,Attribute::#ATTR_CTXT_COMPONENT0D2D,*mesh\a_normals,#False,#False)
    Object3D::AddAttribute(*Me,*normals)
    Protected *uvws = Attribute::New("UVWs",Attribute::#ATTR_TYPE_VECTOR3,Attribute::#ATTR_STRUCT_SINGLE,Attribute::#ATTR_CTXT_COMPONENT0D2D,*mesh\a_uvws,#False,#False)
    Object3D::AddAttribute(*Me,*uvws)
    Protected *pointcolor = Attribute::New("Colors",Attribute::#ATTR_TYPE_COLOR,Attribute::#ATTR_STRUCT_SINGLE,Attribute::#ATTR_CTXT_COMPONENT0D2D,*mesh\a_colors,#False,#False)
    Object3D::AddAttribute(*Me,*pointcolor)
    Protected *data.CArray::CArrayPtr = CArray::newCArrayPtr()
    CArray::Append(*data,*mesh\topo)
    
    ; Topology Attribute
    Protected *topology = Attribute::New("Topology",Attribute::#ATTR_TYPE_TOPOLOGY,Attribute::#ATTR_STRUCT_SINGLE,Attribute::#ATTR_CTXT_SINGLETON,*data,#False,#True)
    Object3D::AddAttribute(*Me,*topology)
;     *Me\texture = 0
  
    ProcedureReturn *Me
  EndProcedure
  
  ; Destructor
  ;----------------------------------------------------
  Procedure Delete(*Me.Polymesh_t)
    glDeleteVertexArrays(1,*Me\vao)
    glDeleteBuffers(1,*Me\vbo)
    glDeleteBuffers(1,*Me\eab)
    ClearStructure(*Me,Polymesh_t)
    FreeMemory(*Me)
  EndProcedure
  
  ;-----------------------------------------------------
  ; Buil GL Data 
  ;-----------------------------------------------------
  Procedure BuildGLData(*p.Polymesh_t)
    Debug "POLYMESH Build GL Datas"
    ;---[ Get Underlying Geometry ]--------------------
    Protected *geom.Geometry::PolymeshGeometry_t = *p\geom
    Protected nbv = *geom\nbsamples
    If nbv <3 : ProcedureReturn : EndIf
    
    Protected GLfloat_s.GLfloat
    
    ; Get Polymesh Datas
    Protected s3 = SizeOf(GLfloat_s) * 3
    Protected s4 = SizeOf(GLfloat_s) * 4
    Protected size_p.i = nbv * s3
    Protected size_c.i = nbv * s4
    Protected size_t.i = 4*size_p + size_c
    
    ; Allocate Memory
    Protected *flatdata = AllocateMemory(size_p)
    Protected i
    Protected *v.v3f32
    Protected *c.c4f32
    
    ; Push Buffer to GPU
    glBufferData(#GL_ARRAY_BUFFER,size_t,#Null,#GL_DYNAMIC_DRAW)
    
    ; POSITIONS
    ;-------------------------------------------------------------
    For i=0 To nbv-1
      *v = CArray::GetValue(*geom\a_positions,PeekL(CArray::GetValue(*geom\a_triangleindices,i)))
      CopyMemory(*v,*flatdata+i*s3,s3)
    Next i
    glBufferSubData(#GL_ARRAY_BUFFER,0,size_p,*flatdata)
    FreeMemory(*flatdata)
    
    ; NORMALS
    ;-------------------------------------------------------------
    glBufferSubData(#GL_ARRAY_BUFFER,size_p,size_p,CArray::GetPtr(*geom\a_normals,0))
    
    ; TANGENTS
    ;-------------------------------------------------------------
    glBufferSubData(#GL_ARRAY_BUFFER,2*size_p,size_p,CArray::GetPtr(*geom\a_tangents,0))
    
    ; UVWS
    ;-------------------------------------------------------------
    glBufferSubData(#GL_ARRAY_BUFFER,3*size_p,size_p,CArray::GetPtr(*geom\a_uvws,0))
    
     ; COLORS
     ;-------------------------------------------------------------
    Protected c
    Protected *cl.c4f32
    
    glBufferSubData(#GL_ARRAY_BUFFER,4*size_p,size_c,CArray::GetPtr(*geom\a_colors,0))
    
    ; Attibute Position 0
    glEnableVertexAttribArray(0)
    glVertexAttribPointer(0,3,#GL_FLOAT,#GL_FALSE,0,0)
    
    ;Attibute Normal 1
    glEnableVertexAttribArray(1)
    glVertexAttribPointer(1,3,#GL_FLOAT,#GL_FALSE,0,size_p)
    
    ;Attibute Tangent 2
    glEnableVertexAttribArray(2)
    glVertexAttribPointer(2,3,#GL_FLOAT,#GL_FALSE,0,2*size_p)
    
    ;Attibute UVWs 2
    glEnableVertexAttribArray(3)
    glVertexAttribPointer(3,3,#GL_FLOAT,#GL_FALSE,0,3*size_p)
    
    ; Attribute Color 3
    glEnableVertexAttribArray(4)
    glVertexAttribPointer(4,4,#GL_FLOAT,#GL_FALSE,0,4*size_p)
    
    glBindAttribLocation(*p\pgm\pgm, 0, "position")
    glBindAttribLocation(*p\pgm\pgm, 1, "normal")
    glBindAttribLocation(*p\pgm\pgm, 2, "tangent")
    glBindAttribLocation(*p\pgm\pgm, 3, "uvws")
    glBindAttribLocation(*p\pgm\pgm, 4, "color");
    
    

  EndProcedure
    
  ; Setup
  ;----------------------------------------------------
  Procedure Setup(*p.Polymesh_t,*pgm.Program::Program_t)
    
    *p\pgm = *pgm

    ;---[ Get Underlying Geometry ]--------------------
    Protected *geom.Geometry::PolymeshGeometry_t = *p\geom
    
    Protected nbv = CArray::GetCount(*geom\a_triangleindices)
    If nbv <3 : ProcedureReturn : EndIf
   
    ; Setup Static Kinematic STate
    ;ResetStaticKinematicState(*p)
    
    
    ; Create or ReUse Vertex Array Object
    If Not *p\vao
      glGenVertexArrays(1,@*p\vao)
    EndIf
    glBindVertexArray(*p\vao)
    
    ; Create or ReUse Vertex Buffer Object
    If Not *p\vbo
      glGenBuffers(1,@*p\vbo)
    EndIf
    glBindBuffer(#GL_ARRAY_BUFFER,*p\vbo)
    
    ; Create or ReUse Edge Elements Buffer
    If Not *p\eab
      glGenBuffers(1,@*p\eab)
    EndIf 
    Define l.l
    glBindBuffer(#GL_ELEMENT_ARRAY_BUFFER,*p\eab)
    glBufferData(#GL_ELEMENT_ARRAY_BUFFER,*geom\nbedges * 2 * CArray::GetItemSize(*geom\a_edgeindices),CArray::GetPtr(*geom\a_edgeindices,0),#GL_DYNAMIC_DRAW)
    
    ; Fill Buffer
    BuildGLData(*p)
    
    glLinkProgram(*p\pgm\pgm);
    ; Check For Errors
    Protected linked.i
    If Not glGetProgramiv(*p\pgm\pgm, #GL_LINK_STATUS, @linked);
      ;Make sure linked==TRUE
      ;If linked==FALSE, the log contains information on what went wrong
      Protected maxLength.i
      glGetProgramiv(*p\pgm\pgm, #GL_INFO_LOG_LENGTH, @maxLength);
      maxLength = maxLength + 1                                  ;
      Protected uchar.c
      Protected *pLinkInfoLog = AllocateMemory( maxLength * SizeOf(uchar));
      glGetProgramInfoLog(*p\pgm\pgm, maxLength, @maxLength, *pLinkInfoLog);
      ;MessageRequester("Error Setup Shader Program for Polymesh",PeekS(*pLinkInfoLog))
    EndIf

    ; Unbind
    glBindVertexArray(0)
   
    
    *p\initialized = #True
    *p\dirty = Object3D::#DIRTY_STATE_CLEAN
  EndProcedure
  
  ;-----------------------------------------------------
  ; Clean
  ;-----------------------------------------------------
  ;{
  Procedure Clean(*p.Polymesh_t)

      If *p\vao : glDeleteVertexArrays(1,@*p\vao) : EndIf

      If *p\vbo: glDeleteBuffers(1,@*p\vbo) : EndIf

      If *p\eab: glDeleteBuffers(1,@*p\eab) : EndIf

  EndProcedure
  ;}
  
  ;-----------------------------------------------------
  ; Update
  ;-----------------------------------------------------
  ;{
  Procedure Update(*p.Polymesh_t)
    Debug "Update Polymesh"
    If *p\stack
      Stack::Update(*p\stack)
    EndIf
    
    If *p\dirty & Object3D::#DIRTY_STATE_TOPOLOGY Or Not *p\initialized
      Protected p.Object3D::IObject3D = *p
      p\Setup(*p\pgm)
    Else 
      If *p\dirty & Object3D::#DIRTY_STATE_DEFORM
        PolymeshGeometry::RecomputeNormals(*p\geom,1.0)
        glBindVertexArray(*p\vao)
        glBindBuffer(#GL_ARRAY_BUFFER,*p\vbo)
        BuildGLData(*p)
        glBindBuffer(#GL_ARRAY_BUFFER,0)
        glBindVertexArray(0)
        *p\dirty = Object3D::#DIRTY_STATE_CLEAN
      EndIf
    EndIf
   glCheckError("Update Polymesh")
  EndProcedure
  ;}
  
  ;-----------------------------------------------------
  ; Draw
  ;-----------------------------------------------------
  ;{
  Procedure Draw(*p.Polymesh_t)
    ;Skip invisible Object
    If Not *p\visible  Or Not *p\initialized: ProcedureReturn : EndIf
    
    Protected *geom.Geometry::PolymeshGeometry_t = *p\geom
    glBindVertexArray(*p\vao)
    glUniformMatrix4fv(glGetUniformLocation(*p\pgm\pgm,"model"),1,#GL_FALSE,*p\matrix)
    glDrawArrays(#GL_TRIANGLES,0,CArray::GetCount(*geom\a_triangleindices)) 
    
    glBindVertexArray(0)
  EndProcedure
  ;}
  
  ; Set From Shape
  ;----------------------------------------------------
  Procedure SetFromShape(*Me.Polymesh_t,shape.i)

  EndProcedure
EndModule

  
    
    
; IDE Options = PureBasic 5.31 (Windows - x64)
; CursorPosition = 257
; FirstLine = 194
; Folding = ---
; EnableXP
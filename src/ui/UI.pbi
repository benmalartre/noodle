InitScintilla()

; -----------------------------------------
; UI Module Declaration
; -----------------------------------------
DeclareModule UI
  Enumeration
    #UI_DUMMY
    #UI_GRAPH
    #UI_3D
    #UI_SHADER
    #UI_COLOR
    #UI_LOG
    #UI_TIMELINE
  EndEnumeration
  
  Structure UI_t
    *VT
    name.s
    x.i
    y.i
    lastx.i
    lasty.i
    offsetx.i
    offsety.i
    width.i
    height.i
    container.i
    type.i
    dirty.b
    down.b
    zoom.i
    *top
  EndStructure
  
  Interface IUI
    Init()
    Event(event.i)
    Term()
  EndInterface
  
  
;   Global img_local_only.i
;   Global img_server_only.i
;   Global img_local_new.i
;   Global img_server_new.i
;   Global img_sync.i
;   Global img_folder_sync.i
;   Global img_folder_server_only.i
;   Global img_folder_local_only.i
;   
;   Declare Init()
;   Declare Term()
  
   
  
EndDeclareModule

; -----------------------------------------
; UI Module Implementation
; -----------------------------------------
Module UI
  Procedure GetName(*ui.UI_t)
    MessageRequester(*ui\name,*ui\name)
  EndProcedure
  
    
;   UsePNGImageDecoder()
;    ; Init
;   ;-------------------------------
;   Procedure Init()
;     img_local_only = LoadImage(#PB_Any,GetCurrentDirectory()+"ico/FileLocalOnly_raw.png")
;     img_server_only = LoadImage(#PB_Any,GetCurrentDirectory()+"ico/FileServerOnly_raw.png")
;     img_local_new = LoadImage(#PB_Any,GetCurrentDirectory()+"ico/FileLocalNew_raw.png")
;     img_server_new = LoadImage(#PB_Any,GetCurrentDirectory()+"ico/FileServerNew_raw.png")
;     img_sync = LoadImage(#PB_Any,GetCurrentDirectory()+"ico/FileSync_raw.png")
;     img_folder_sync = LoadImage(#PB_Any,GetCurrentDirectory()+"ico/FolderSync_raw.png")
;     img_folder_server_only = LoadImage(#PB_Any,GetCurrentDirectory()+"ico/FolderServerOnly_raw.png")
;   EndProcedure
;   
;   ; Term
;   ;-------------------------------
;   Procedure Term()
;     If IsImage(img_local_only):FreeImage(img_local_only):EndIf
;     If IsImage(img_server_only):FreeImage(img_server_only):EndIf
;     If IsImage(img_local_new):FreeImage(img_local_new):EndIf
;     If IsImage(img_server_new):FreeImage(img_server_new):EndIf
;     If IsImage(img_sync):FreeImage(img_sync):EndIf
;   EndProcedure
  
EndModule


; IDE Options = PureBasic 5.42 LTS (Windows - x64)
; CursorPosition = 23
; Folding = -
; EnableXP
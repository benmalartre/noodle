
XIncludeFile "UI.pbi"
; XIncludeFile "Command.pbi"

; -----------------------------------------
; DummyUI Module Declaration
; -----------------------------------------
DeclareModule DummyUI
  UseModule UI
  Structure DummyUI_t Extends UI_t
    gadgetID.i
  EndStructure
  
  Interface IDummyUI Extends IUI
  EndInterface

  Declare New(name.s,x.i,y.i,w.i,h.i)
  Declare Delete(*ui.DummyUI_t)
  Declare Init(*ui.DummyUI_t)
  Declare Event(*ui.DummyUI_t,event.i)
  Declare Term(*ui.DummyUI_t)
  Declare Draw(*ui.DummyUI_t)
  
  DataSection 
    DummyVT: 
    Data.i @Init()
    Data.i @Event()
    Data.i @Term()
  EndDataSection 
  
EndDeclareModule

; -----------------------------------------
; DummyUI Module Implementation
; -----------------------------------------
Module DummyUI

  ; New
  ;-------------------------------
  Procedure New(name.s,x.i,y.i,w.i,h.i)
    Protected *ui.DummyUI_t = AllocateMemory(SizeOf(DummyUI_t))
    InitializeStructure(*ui,DummyUI_t)
    *ui\name = name
    *ui\container = ContainerGadget(#PB_Any,x,y,w,h)
    *ui\width = w
    *ui\height = h
    *ui\gadgetID = CanvasGadget(#PB_Any,0,0,w,h)
    *ui\VT = ?DummyVT
   
    CloseGadgetList()
    ProcedureReturn *ui
  EndProcedure
  
  ; Delete
  ;-------------------------------
  Procedure Delete(*ui.DummyUI_t)
    ClearStructure(*ui,DummyUI_t)
    FreeMemory(*ui)
  EndProcedure

  
  ; Draw
  ;-------------------------------
  Procedure Draw(*ui.DummyUI_t)
    Debug "------------ DRAW "

    StartDrawing(CanvasOutput(*ui\gadgetID))
    Box(0,0,GadgetWidth(*ui\gadgetID),GadgetHeight(*ui\gadgetID),RGB(Random(255),Random(255),Random(255)))
    StopDrawing()
  EndProcedure
  
  ; Init
  ;-------------------------------
  Procedure Init(*ui.DummyUI_t)
    Debug "DUmmyUI Init Called!!!"
  EndProcedure
  
  ; Event
  ;-------------------------------
  Procedure Event(*ui.DummyUI_t,event.i)
    
    Draw(*ui)
    Select event
      Case #PB_Event_SizeWindow
        Debug  "######################### RESIZE GADGET "
        ResizeGadget(*ui\gadgetID,0,0,GadgetWidth(*ui\container),GadgetHeight(*ui\container))
            
      Case #PB_Event_Gadget
        Protected g = EventGadget()
        Select g
          
          
        EndSelect
        
    EndSelect
    
  EndProcedure
  
  ; Term
  ;-------------------------------
  Procedure Term(*ui.DummyUI_t)
    Debug "DUmmyUI Term Called!!!"
  EndProcedure
  
EndModule
; IDE Options = PureBasic 5.31 (Windows - x64)
; CursorPosition = 85
; FirstLine = 24
; Folding = --
; EnableXP

XIncludeFile "UI.pbi"
XIncludeFile "../libs/OpenGL.pbi"
XIncludeFile "../libs/OpenGLExt.pbi"
XIncludeFile "../objects/Camera.pbi"
XIncludeFile "View.pbi"

; -----------------------------------------
; ViewportUI Module Declaration
; -----------------------------------------
DeclareModule ViewportUI
  UseModule UI
  Structure ViewportUI_t Extends UI_t
    gadgetID.i
    *camera.Camera::Camera_t
  EndStructure
  
  Interface IViewportUI Extends IUI
  EndInterface

  Declare New(*parent.View::View_t,name.s)
  Declare Delete(*ui.ViewportUI_t)
  Declare Init(*ui.ViewportUI_t)
  Declare Event(*ui.ViewportUI_t,event.i)
  Declare Term(*ui.ViewportUI_t)
  
  DataSection 
    ViewportUIVT: 
    Data.i @Init()
    Data.i @Event()
    Data.i @Term()
  EndDataSection 
  
EndDeclareModule

; -----------------------------------------
; ViewportUI Module Implementation
; -----------------------------------------
Module ViewportUI
  UseModule OpenGL
  UseModule OpenGLExt
  ; New
  ;-------------------------------
  Procedure New(*parent.View::View_t,name.s)
    Protected *ui.ViewportUI_t = AllocateMemory(SizeOf(ViewportUI_t))
    InitializeStructure(*ui,ViewportUI_t)
    *ui\name = name
    
    Protected x = *parent\x
    Protected y = *parent\y
    Protected w = *parent\width
    Protected h = *parent\height
    
    *ui\container = ContainerGadget(#PB_Any,x,y,w,h)
    *ui\gadgetID = OpenGLGadget(#PB_Any,0,0,w,h,#PB_OpenGL_Keyboard)
    
    SetGadgetAttribute(*ui\gadgetID,#PB_OpenGL_SetContext,#True)
    GLLoadExtensions()
    
    *ui\width = w
    *ui\height = h
    *ui\VT = ?ViewportUIVT
   
    CloseGadgetList()
    
    View::SetContent(*parent,*ui)
    
    ProcedureReturn *ui
  EndProcedure
  
  ; Delete
  ;-------------------------------
  Procedure Delete(*ui.ViewportUI_t)
    If IsGadget(*ui\gadgetID) : FreeGadget(*ui\gadgetID):EndIf
    If IsGadget(*ui\container) : FreeGadget(*ui\container):EndIf
    ClearStructure(*ui,ViewportUI_t)
    FreeMemory(*ui)
  EndProcedure

 
  ; Init
  ;-------------------------------
  Procedure Init(*ui.ViewportUI_t)
    Debug "ViewportUI Init Called!!!"
  EndProcedure
  
  ; Event
  ;-------------------------------
  Procedure Event(*ui.ViewportUI_t,event.i)
;     SetGadgetAttribute(*ui\gadgetID,#PB_OpenGL_SetContext,#True)
;     glClearColor(Random(100)*0.01,Random(100)*0.01,Random(100)*0.01,1.0)
;     glClear(#GL_COLOR_BUFFER_BIT|#GL_DEPTH_BUFFER_BIT)
;     SetGadgetAttribute(*ui\gadgetID,#PB_OpenGL_FlipBuffers,#True)

    Select event
      Case #PB_Event_SizeWindow
;         ResizeGadget(*ui\panel,0,0,GadgetWidth(*ui\container),30)
            
      Case #PB_Event_Gadget
        If EventGadget() = *ui\gadgetID
          If *ui\camera : Camera::Event(*ui\camera,*ui\gadgetID) : EndIf
        EndIf

    EndSelect
    
  EndProcedure
  
  ; Term
  ;-------------------------------
  Procedure Term(*ui.ViewportUI_t)
    Debug "ViewportUI Term Called!!!"
  EndProcedure
  
EndModule
; IDE Options = PureBasic 5.31 (Windows - x64)
; CursorPosition = 20
; Folding = --
; EnableXP
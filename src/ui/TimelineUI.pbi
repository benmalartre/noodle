XIncludeFile "../core/Time.pbi"
XIncludeFile "UI.pbi"
; XIncludeFile "Command.pbi"

; -----------------------------------------
; TimelineUI Module Declaration
; -----------------------------------------
DeclareModule TimelineUI
  UseModule UI
  
  Global play_btn_img.i
  Global stop_btn_img.i
  Global first_frame_btn_img.i
  Global last_frame_btn_img.i
  Global loop_btn_img.i
  Global timeline_initialized.b = #False
  
  Structure TimelineUI_t Extends UI_t
    play_btn.i
    stop_btn.i
    first_frame_btn.i
    last_frame_btn.i
    loop_btn.i
    current_frame_txt.i
    current_frame_input.i
    first_frame_txt.i
    first_frame_input.i
    last_frame_txt.i
    last_frame_input.i
    frames_canvas.i
  EndStructure
  
  Interface ITimelineUI Extends IUI
  EndInterface

  Declare New(name.s,x.i,y.i,w.i,h.i)
  Declare Delete(*ui.TimelineUI_t)
  Declare Init(*ui.TimelineUI_t)
  Declare Event(*ui.TimelineUI_t,event.i)
  Declare Term(*ui.TimelineUI_t)
  Declare Draw(*ui.TimelineUI_t)
  
  DataSection 
    DummyVT: 
      Data.i @Init()
      Data.i @Event()
      Data.i @Term()
    play_btn_img_DT:
      IncludeBinary "../../ico/play_btn.png"
    stop_btn_img_DT:
      IncludeBinary "../../ico/stop_btn.png"
    last_frame_btn_img_DT:
      IncludeBinary "../../ico/last_frame_btn.png"
    first_frame_btn_img_DT:
      IncludeBinary "../../ico/first_frame_btn.png"
    loop_btn_img_DT:
      IncludeBinary "../../ico/loop_btn.png"
  EndDataSection 
  
EndDeclareModule

; -----------------------------------------
; TimelineUI Module Implementation
; -----------------------------------------
Module TimelineUI
  Procedure Initialize()
    play_btn_img = CatchImage(#PB_Any,?play_btn_img_DT)
    stop_btn_img = CatchImage(#PB_Any,?stop_btn_img_DT)
    first_frame_btn_img = CatchImage(#PB_Any,?first_frame_btn_img_DT)
    last_frame_btn_img = CatchImage(#PB_Any,?last_frame_btn_img_DT)
    loop_btn_img = CatchImage(#PB_Any,?loop_btn_img_DT)
    
  EndProcedure
  
  ; New
  ;-------------------------------
  Procedure New(name.s,x.i,y.i,w.i,h.i)
    ; Load Images in memory
    If Not timeline_initialized : Initialize() : EndIf
    
    Protected *ui.TimelineUI_t = AllocateMemory(SizeOf(TimelineUI_t))
    InitializeStructure(*ui,TimelineUI_t)
    *ui\name = name
    *ui\container = ContainerGadget(#PB_Any,x,y,w,h)
    *ui\width = w
    *ui\height = h
    
    *ui\first_frame_btn = ButtonImageGadget(#PB_Any,0,0,25,h,ImageID(first_frame_btn_img))
    *ui\play_btn = ButtonImageGadget(#PB_Any,25,0,25,h,ImageID(play_btn_img),#PB_Button_Toggle)
    *ui\stop_btn = ButtonImageGadget(#PB_Any,50,0,25,h,ImageID(stop_btn_img))
    *ui\last_frame_btn = ButtonImageGadget(#PB_Any,75,0,25,h,ImageID(last_frame_btn_img))
    *ui\loop_btn = ButtonImageGadget(#PB_Any,100,0,25,h,ImageID(loop_btn_img))

;     *ui\current_frame_txt = TextGadget(#PB_Any,200,0,100,h,"Current Frame :")
;     *ui\current_frame_input = StringGadget(#PB_Any,200,0,100,h,Str(Time::current_frame))
;     *ui\first_frame_txt = TextGadget(#PB_Any,200,0,100,h,"First Frame :")
;     *ui\first_frame_input = StringGadget(#PB_Any,200,0,100,h,Str(Time::first_frame))
;     *ui\last_frame_txt = TextGadget(#PB_Any,200,0,100,h,"Last Frame :")
;     *ui\last_frame_input = StringGadget(#PB_Any,200,0,100,h,Str(Time::last_frame))
    *ui\frames_canvas = CanvasGadget(#PB_Any,125,0,w-125,h)
    *ui\VT = ?DummyVT
    Event(*ui,#PB_Event_SizeWindow)
    CloseGadgetList()
    ProcedureReturn *ui
  EndProcedure
  
  ; Delete
  ;-------------------------------
  Procedure Delete(*ui.TimelineUI_t)
    ClearStructure(*ui,TimelineUI_t)
    FreeMemory(*ui)
  EndProcedure

  
  ; Draw
  ;-------------------------------
  Procedure Draw(*ui.TimelineUI_t)
    StartDrawing(CanvasOutput(*ui\frames_canvas))
    Box(0,0,GadgetWidth(*ui\frames_canvas),GadgetHeight(*ui\frames_canvas),RGB(175,175,175))
    StopDrawing()
  EndProcedure
  
  ; Init
  ;-------------------------------
  Procedure Init(*ui.TimelineUI_t)
    Debug "TimelineUI Init Called!!!"
  EndProcedure
  
  ; Event
  ;-------------------------------
  Procedure Event(*ui.TimelineUI_t,event.i)
    
    Draw(*ui)
    Select event
      Case #PB_Event_SizeWindow
        Protected ty = GadgetHeight(*ui\container)-25
        ResizeGadget(*ui\first_frame_btn,0,ty,25,20)
        ResizeGadget(*ui\play_btn,25,ty,25,20)
        ResizeGadget(*ui\stop_btn,50,ty,25,20)
        ResizeGadget(*ui\last_frame_btn,75,ty,25,20)
        ResizeGadget(*ui\loop_btn,100,ty,25,20)
        ;ResizeGadget(*ui\current_frame_txt,75,0,25,GadgetHeight(*ui\container))
        ResizeGadget(*ui\frames_canvas,0,0,GadgetWidth(*ui\container),GadgetHeight(*ui\container)-25)
        Draw(*ui)
      Case #PB_Event_Gadget
        Protected g = EventGadget()
        Select g
          
          
        EndSelect
        
    EndSelect
    
  EndProcedure
  
  ; Term
  ;-------------------------------
  Procedure Term(*ui.TimelineUI_t)
    Debug "TimelineUI Term Called!!!"
  EndProcedure
  
  
EndModule



; IDE Options = PureBasic 5.31 (Windows - x64)
; CursorPosition = 165
; FirstLine = 90
; Folding = --
; EnableXP
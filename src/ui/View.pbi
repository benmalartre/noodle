XIncludeFile "UI.pbi"

;============================================================
; View Module Declaration
;============================================================
DeclareModule View
  Enumeration
    #VIEW_NONE
    #VIEW_TOP
    #VIEW_BOTTOM
    #VIEW_LEFT
    #VIEW_RIGHT
    #VIEW_OTHER
  EndEnumeration
  
  

  Structure View_t
    
    *manager ;CViewManager(Not declared so commented)
    *content.UI::UI_t ;
    *right.View_t
    *left.View_t
    *top.View_t
    
    name.s                          ; View Name
    lorr.b                          ; left or right view
    
    x.i                             ; View Position X
    y.i                             ; View Position Y
    width.i                         ; View Actual Width
    height.i                        ; View Actual Height
    id.i                            ; unique ID
    axis.b                          ; splitter axis
    perc.i                          ; splitter percentage
    
    fixed_size.i                    ; static size  (for fixed view)
    fixed.b                         ; static height (for fixed
    
    lastx.i
    lasty.i
    offsetx.i
    offsety.i
    zoom.i
    
    splitterID.i                    ; Canvas Splitter ID(if not leaf)

    parentID.i                      ; Parent Window ID
    leaf.b
    active.b
    dirty.b                         ; view need a refresh
    down.b
    type.i
    
    lsplitter.i
    rsplitter.i
    tsplitter.i
    bsplitter.i
    
    
  EndStructure
  
  Declare New(x.i,y.i,width.i,height.i,*top,axis.b=#False,name.s="View",lorr.b=#True,scroll.b=#True)
  Declare Delete(*view.View_t)
  Declare Draw(*view.View_t)
;   Declare DrawDebug(*view.View_t)
  Declare.b MouseInside(*view,x.i,y.i)
  Declare TouchBorder(*view,x.i,y.i,w.i)
  Declare TouchBorderEvent(*view,border.i)
  Declare GetActive(*view,x.i,y.i)
  Declare Split(*view,fixed.b=#True,options.i=0,perc.i=50)
  Declare Resize(*view,x.i,y.i,width.i,height.i)
  Declare Event(*view,event.i)
  Declare EventSplitter(*view.View_t,border.i)
  Declare SetContent(*view.View_t,*content.UI::UI_t)
  
EndDeclareModule

;============================================================
; ViewManager Module Declaration
;============================================================
DeclareModule ViewManager
  #VIEW_BORDER_SENSIBILITY = 3
  #VIEW_SPLITTER_DROP = 7
  
  Enumeration
    #SHORTCUT_UNDO
    #SHORTCUT_REDO
  EndEnumeration
  
    
  Structure ViewManager_t
    name.s
    *main.View::View_t
    *active.View::View_t
    Map *views.View::View_t()

    lastx.i
    lasty.i
    window.i
  
  EndStructure
  
  Global *view_manager.ViewManager_t
  
  Declare New(name.s,x.i,y.i,width.i,height.i,options = #PB_Window_SystemMenu|#PB_Window_ScreenCentered)
  Declare Delete(*manager.ViewManager_t)
  Declare Event(*manager.ViewManager_t,event.i)
  
EndDeclareModule


;============================================================
; View module Implementation
;============================================================
Module View
  ;----------------------------------------------------------
  ; Constructor
  ;----------------------------------------------------------
  Procedure New(x.i,y.i,width.i,height.i,*top.View_t,axis.b = #False,name.s="View",lorr.b=#True,scroll.b=#True)
    Protected *view.View_t = AllocateMemory(SizeOf(View_t))
    *view\x = x
    *view\y = y
    *view\width = width
    *view\height = height
  
    *view\right = #Null
    *view\left = #Null
    *view\leaf = #True
    *view\active = #False
    *view\dirty = #True
    *view\name = name
    *view\lorr = lorr
    *view\content = #Null
   
;     *view\gadgetID = ContainerGadget(#PB_Any,x,y,width,height)
;   
;     SetGadgetColor(*view\gadgetID,#PB_Gadget_BackColor,COLOR_MAIN_BG)
;     ;*view\canvasID = FrameGadget(#PB_Any,0,0,width,height,"test");CanvasGadget(#PB_Any,0,0,width,height)
;     
;     CloseGadgetList()
    
    *view\axis = axis
    *view\type = 0;#VIEW_EMPTY
    
    ;increment view id counter
    view_id_counter + 1
    *view\id = view_id_counter
    
    If *top = #Null
      *view\manager = #Null
      *view\top = #Null
    Else
      *view\top = *top
      *view\manager = *top\manager
    EndIf
    
    Debug "------------------ Created View :"
    Debug *view\x
    Debug *view\y
    Debug *view\width
    Debug *view\height
    
    ProcedureReturn *view
  EndProcedure
  
  ;----------------------------------------------------------
  ; Delete View
  ;----------------------------------------------------------
  Procedure Delete(*view.View_t)
;     If *view\gadgetID : FreeGadget(*view\gadgetID) : EndIf
;     If *view\canvasID : FreeGadget(*view\canvasID) : EndIf
;     If *view\imageID  : FreeImage(*view\imageID)   : EndIf
    
    FreeMemory(*view)
    
  EndProcedure
  

  
  ;----------------------------------------------------------
  ; Resize
  ;----------------------------------------------------------
  Procedure Resize(*view.View_t,x.i,y.i,width.i,height.i)
  
    Protected *manager.ViewManager::ViewManager_t = *view\manager
    
    *view\x = x
    *view\y = y
    *view\width = width
    *view\height = height
    Protected mx,my
    
;     If Not *view = *manager\main And IsGadget(*view\gadgetID)
;       ResizeGadget(*view\gadgetID,*view\x,*view\y,*view\width,*view\height)
;     EndIf

    If *view\leaf

      Protected *ui.UI::UI_t = *view\content
      Protected ui.UI::IUI = *ui
      If *ui
        ResizeGadget(*ui\container,x,y,width,height)  
        ui\Event(#PB_Event_SizeWindow)
      EndIf
      
;       Protected *content.View_t = *view\content
;       Protected ev_data.EventTypeDatas_t
;       ev_data\x = x
;       ev_data\y = y
;       ev_data\width = width
;       ev_data\height = height
;       If *content
;         *content\Event(#PB_Event_SizeWindow,@ev_data)
;       EndIf
    Else
      Protected hs = ViewManager::#VIEW_BORDER_SENSIBILITY/2
      If *view\axis
        ResizeGadget(*view\splitterID,*view\x+*view\width* *view\perc/100-hs,*view\y,2*hs,*view\height)
        Resize(*view\left,*view\x,*view\y,*view\width* *view\perc/100-hs,*view\height)
        Resize(*view\right,*view\x+*view\width* *view\perc/100+hs,*view\y,*view\width-*view\width* *view\perc/100-hs,*view\height)
      Else
        ResizeGadget(*view\splitterID,*view\x,*view\y + *view\height * *view\perc/100-hs,*view\width,2*hs)
        Resize(*view\left,*view\x,*view\y,*view\width,*view\height* *view\perc/100-hs)
        Resize(*view\right,*view\x,*view\y+*view\height* *view\perc/100+hs,*view\width,*view\height-*view\height* *view\perc/100-hs)
      EndIf
    EndIf
  EndProcedure
  
  ;----------------------------------------------------------------------------------
  ; Get Percentage
  ;----------------------------------------------------------------------------------
  Procedure GetPercentage(*view.View_t,mx.i,my.i)
    ;If Not *view\fixed
      If Not *view\axis
        If my<*view\y Or my>*view\y+*view\height 
          ProcedureReturn
        EndIf
        *view\perc = (my-*view\y) * 100 /*view\height
      Else
        If mx<*view\x Or mx>*view\x+*view\width 
          ProcedureReturn
        EndIf
        *view\perc = (mx-*view\x) * 100 /*view\width
      EndIf
  ;   Else
  ;     ; when a view is fixed , it's size is in pixels...
  ;     If *view\axis
  ;       Debug "View ----------------------------------> Vertical"
  ;       Protected h = *view\fixed_size
  ;       Protected nh.f = *view\height/h*100
  ;       *view\perc = nh;
  ;     Else
  ;       Debug "View ----------------------------------> Horizontal"
  ;       Protected w = *view\fixed_size
  ;       Protected nw.f = *view\width/w*100
  ;       *view\perc = nw;
  ;     EndIf
  ;     
  ;   EndIf
    
    EndProcedure
    
  ;----------------------------------------------------------------------------------
  ; Split
  ;----------------------------------------------------------------------------------
  Procedure SetSplitter(*view.View_t,l,r,t,b)
    *view\lsplitter = l
    *view\rsplitter = r
    *view\tsplitter = t
    *view\bsplitter = b
  EndProcedure
  
  Procedure Split(*view.View_t,fixed.b=#True,options.i=0,perc.i=50)
    If *view\leaf
      
;       If *view\gadgetID : FreeGadget(*view\gadgetID):EndIf
      Protected hs = ViewManager::#VIEW_BORDER_SENSIBILITY/2
      Protected *content = *view\content
      Protected *manager.ViewManager::ViewManager_t = *view\manager
      UseGadgetList(WindowID(*view\parentID))
      
;       OpenGadgetList(*view\gadgetID)
      If options & #PB_Splitter_Vertical
        Protected mx = *view\width*perc/100
        *view\left = New(*view\x,*view\y,mx-hs,*view\height,*view,#True,"Left",#True)
        SetSplitter(*view\left,*view\lsplitter,*view,*view\tsplitter,*view\bsplitter)
        *view\left\content = *content
        *view\left\parentID = *view\parentID
        *view\right = New(*view\x+ mx+hs,*view\y,*view\width-mx-hs,*view\height,*view,#True,"Right",#False)
        SetSplitter(*view\right,*view,*view\rsplitter,*view\tsplitter,*view\bsplitter)
        *view\right\parentID = *view\parentID
        If fixed Or options & #PB_Splitter_FirstFixed Or options & #PB_Splitter_SecondFixed
          *view\left\fixed = #True
          *view\right\fixed = #True
          *view\fixed_size = perc
        EndIf
        
        *view\splitterID = CanvasGadget(#PB_Any,*view\x+mx-hs,*view\y,2*hs,*view\height)
        SetGadgetAttribute(*view\splitterID,#PB_Canvas_Cursor,#PB_Cursor_LeftRight)
      
      Else
        Protected my = *view\height*perc/100
        *view\left = New(*view\x,*view\y,*view\width,my-hs,*view,#False,"Left",#True)
        SetSplitter(*view\left,*view\lsplitter,*view\rsplitter,*view\tsplitter,*view)
        *view\left\content = *content
        *view\left\parentID = *view\parentID
        *view\right = New(*view\x,*view\y+ my+hs,*view\width,*view\height-my-hs,*view,#False,"Right",#False)
        SetSplitter(*view\right,*view\lsplitter,*view\rsplitter,*view,*view\bsplitter)
        *view\right\parentID = *view\parentID
        If fixed Or options & #PB_Splitter_FirstFixed Or options & #PB_Splitter_SecondFixed
          *view\left\fixed = #True
          *view\right\fixed = #True
          *view\fixed_size = perc
        EndIf

        *view\splitterID = CanvasGadget(#PB_Any,*view\x,*view\y+my-hs,*view \width,2*hs)
        SetGadgetAttribute(*view\splitterID,#PB_Canvas_Cursor,#PB_Cursor_UpDown)
      EndIf
      
     
      *view\fixed = Bool(options & #PB_Splitter_FirstFixed Or options & #PB_Splitter_SecondFixed)
      *view\axis = Bool(options & #PB_Splitter_Vertical)
      *view\leaf = #False
      *view\perc = perc
;       CloseGadgetList()
      
       
      ProcedureReturn *view
    Else
      ProcedureReturn #Null
    EndIf
    
    
  EndProcedure
  
  ;----------------------------------------------------------------------------------
  ; Touch Border Event
  ;----------------------------------------------------------------------------------
  Procedure.i TouchBorderEvent(*view.View_t,border.i)
    Protected *manager.ViewManager::ViewManager_t = *view\manager
    Protected btn.i
    If *view\fixed : ProcedureReturn : EndIf

    If EventType() = #PB_EventType_LeftButtonDown
      Protected drag.b = #True
      
      ; Get Affected View
      Protected *affected.View_t
      Select border
        Case #VIEW_TOP
          *affected = *view\tsplitter
        Case #VIEW_LEFT
          *affected = *view\lsplitter
        Case #VIEW_RIGHT
          *affected = *view\rsplitter
        Case #VIEW_BOTTOM
          *affected = *view\bsplitter
      EndSelect
      
      ; No Parent View ---> Cannot resize
      If Not *affected : ProcedureReturn : EndIf
      
      Protected sx,sy,sw, sh
      Protected mx = WindowMouseX(*manager\window)
      Protected my = WindowMouseY(*manager\window)
        
          
      ;PostEvent(#PB_Event_Gadget,*manager\window,#Null,#PB_EventType_DragStart)
      ;If DragPrivate(ViewManager::#VIEW_SPLITTER_DROP ,#PB_Drag_Move)

        Define e
        Repeat 
          e = WaitWindowEvent()
          ; Get Mouse Position
          mx = WindowMouseX(*manager\window)
          my = WindowMouseY(*manager\window)
          ; Resize Window Event
          ;If EventType() = #PB_EventType_LeftButtonUp
          
          ;If e = #PB_Event_WindowDrop Or e = #PB_Event_GadgetDrop
          If e = #PB_Event_Gadget And EventType() = #PB_EventType_LeftButtonUp
            GetPercentage(*affected,mx,my)
            drag = #False
            ViewManager::Event(*manager,#PB_Event_SizeWindow)
            
          EndIf
    
        Until drag = #False
      ;EndIf
        
     ;ViewManager::Event(*manager,#PB_Event_SizeWindow)
    EndIf
    
  EndProcedure

  
  ;----------------------------------------------------------------------------------
  ; Touch Border
  ;----------------------------------------------------------------------------------
  Procedure.i TouchBorder(*view.View_t,x.i,y.i,w.i)
    
    ;Left border
    If Abs(*view\x - x)<w                 And  *view\y<y    And *view\y+*view\height>y : ProcedureReturn #VIEW_LEFT : EndIf
    
    ;Right border
    If Abs((*view\x+*view\width) - x)<w   And  *view\y<y    And *view\y+*view\height>y : ProcedureReturn #VIEW_RIGHT : EndIf
    
    ;Top border
    If Abs(*view\y - y)<w                 And  *view\x<x    And *view\x+*view\width>x : ProcedureReturn #VIEW_TOP : EndIf
    
    ;Bottom border
     If Abs((*view\y+*view\height) - y)<w  And  *view\x<x    And *view\x+*view\width>x : ProcedureReturn #VIEW_BOTTOM : EndIf
    
    ProcedureReturn #VIEW_NONE
    
  EndProcedure
  
  ;------------------------------------------------------------------
  ; Splitter Event
  ;------------------------------------------------------------------
  Procedure EventSplitter(*view.View_t,border.i)
    If *view And Not *view\fixed
      ; Get Affected View
      Protected *affected.View_t
      Select border
        Case #VIEW_TOP
          *affected = *view\tsplitter
        Case #VIEW_LEFT
          *affected = *view\lsplitter
        Case #VIEW_RIGHT
          *affected = *view\rsplitter
        Case #VIEW_BOTTOM
          *affected = *view\bsplitter
      EndSelect
      
      If *affected And *affected\splitterID
        StartDrawing(CanvasOutput(*affected\splitterID  ))
        ;         Box(0,0,GadgetWidth(*view\top\splitterID),GadgetHeight(*view\top\splitterID),RGB(Random(100)*0.01,Random(100)*0.01,Random(100)*0.01))
        Box(0,0,GadgetWidth(*affected\splitterID),GadgetHeight(*affected\splitterID),RGB(Random(255),Random(255),Random(255)))
        StopDrawing()
       
        
      EndIf
      
    EndIf
    
  EndProcedure
  
  
  ;----------------------------------------------------------------------------------
  ; Mouse Inside
  ;----------------------------------------------------------------------------------
  Procedure.b MouseInside(*view.View_t, x.i,y.i)
  
    If x>*view\x And x<*view\x+*view\width And y>*view\y And y<*view\y+*view\height
      ProcedureReturn #True
    EndIf
     
    ProcedureReturn #False
      
  EndProcedure
  
  ;----------------------------------------------------------------------------------
  ; Draw
  ;----------------------------------------------------------------------------------

  
  Procedure Draw(*view.View_t)
    
    If *view\leaf And *view\dirty
      ;OViewControl_OnEvent(*view\control,#PB_Event_Repaint,#Null)
  ;     StartDrawing(CanvasOutput(*view\canvasID))
  ;     Box(0,0,*view\width,*view\height,RGB(100,100,100))
  ;     DrawImage(ImageID(*view\imageID),*view\offsetx,*view\offsety) 
  ;     *view\dirty = #False
  ;     DrawingMode(#PB_2DDrawing_Outlined)
  ;     RoundBox(0,0,*view\width,*view\height,2,2,RGB(120,120,120))
  ;     StopDrawing()
    Else
      If *view\left : Draw(*view\left) : EndIf
      If *view\right : Draw(*view\right) : EndIf
    EndIf
    
  EndProcedure
  
  ;----------------------------------------------------------------------------------
  ; Get Active View
  ;----------------------------------------------------------------------------------
  Procedure GetActive(*view.View_t,x.i,y.i)
    Protected *manager.ViewManager::ViewManager_t = *view\manager
    
    Protected active.b = *view\active 
    
    If *view\leaf
      If MouseInside(*view,x,y) = #True
        *view\active = #True
        
        If active <>#True : *view\dirty  = #True : EndIf
        ProcedureReturn #True
      Else
        *view\active = #False
        If active = #True : *view\dirty = #True : EndIf
        ProcedureReturn #False
      EndIf
    Else
      If *view\left : GetActive(*view\left,x,y) : EndIf
      If *view\right : GetActive(*view\right,x,y) : EndIf
    EndIf
    
  
  EndProcedure
  
  ;-----------------------------------------------------------------------------------
  ; Drag View (if image > canvas)
  ;-----------------------------------------------------------------------------------
  Procedure Drag(*view.View_t)
  ;   Protected limit_x = GadgetWidth(*view\canvasID)-ImageWidth(*view\imageID)
  ;   Protected limit_y = GadgetHeight(*view\canvasID)-ImageHeight(*view\imageID)
  ;   
  ;   Protected mx = GetGadgetAttribute(*view\canvasID,#PB_Canvas_MouseX)
  ;   Protected my = GetGadgetAttribute(*view\canvasID,#PB_Canvas_MouseY)
  ;   
  ;   *view\offsetx + mx-*view\lastx
  ;   *view\offsety + my-*view\lasty
  ;   
  ; ;   Debug "Offset X : "+Str(*view\offsetx)
  ; ;   Debug "Offset Y : "+Str(*view\offsety)
  ;   
  ;   *view\lastx = mx
  ;   *view\lasty = my
  ;   
  ;   Clamp(*view\offsetx,limit_x,0)
  ;   Clamp(*view\offsety,limit_y,0)
  ;   OView_Draw(*view)
    
  EndProcedure
  
  ;-----------------------------------------------------------------------------------
  ; View Event
  ;-----------------------------------------------------------------------------------
  Procedure Event(*Me.View_t,event.i)

    Protected *manager.ViewManager::ViewManager_t = *Me\manager
    
    If *Me\leaf
      If *Me\content <> #Null
        
        Protected *content.UI::IUI = *Me\content
        *content\Event(event)
      EndIf
      
    Else
      If event = #PB_Event_SizeWindow
        MessageRequester("VIEW","RESIZE WINDOW EVENT")
        Resize(*Me,0,0,WindowWidth(*Me\parentID),WindowHeight(*Me\parentID))  
      ElseIf event = #PB_Event_Timer
        Event(*Me\left,#PB_Event_Timer)
        Event(*Me\right,#PB_Event_Timer)
      ElseIf event = #PB_Event_Repaint
        Event(*Me\left,#PB_Event_Repaint)
        Event(*Me\right,#PB_Event_Repaint)
      EndIf
      
    EndIf
  
  EndProcedure
  
  ;-----------------------------------------------------------------------------------
  ; Set Content
  ;-----------------------------------------------------------------------------------
  Procedure SetContent(*Me.View_t,*content.UI::UI_t)
    If *Me\content
      Debug "Delete OLD content!!!"
    EndIf
   
    *Me\content = *content
    *content\top = *Me
  
  EndProcedure
  
  ;-----------------------------------------------------------------------------------
  ; Get Window
  ;-----------------------------------------------------------------------------------
  Procedure GetWindow(*Me.View_t)
    Protected *manager.ViewManager::ViewManager_t = *Me\manager
    ProcedureReturn *manager\window
  EndProcedure
  
  ;-----------------------------------------------------------------------------------
  ; Get Scroll Area
  ;-----------------------------------------------------------------------------------
  Procedure GetScrollArea(*Me.View_t)
;     If *Me\scrollable
;       *Me\scrolling = #False
;       If *Me\width>*Me\iwidth : *Me\scrollmaxx = 0 : Else : *Me\scrollmaxx = *Me\iwidth-*Me\width : EndIf
;       If *Me\height>*Me\iheight : *Me\scrollmaxy = 0 : Else : *Me\scrollmaxy = *Me\iheight-*Me\height : EndIf
;     EndIf
    
  EndProcedure
  
  ;-----------------------------------------------------------------------------------
  ; Scroll
  ;-----------------------------------------------------------------------------------
  Procedure Scroll(*Me.View_t,mode.b =#False)
;     If *Me\scrollable And (*Me\scrolling Or mode = #True)
;       If mode = #True
;         Protected d = GetGadgetAttribute(*Me\gadgetID,#PB_Canvas_WheelDelta)
;         *Me\scrolly + d*22
;       Else
;         
;         Protected x = GetGadgetAttribute(*Me\gadgetID,#PB_Canvas_MouseX)
;         Protected y = GetGadgetAttribute(*Me\gadgetID,#PB_Canvas_MouseY)
;         *Me\scrollx + (x-*Me\scrolllastx)
;         *Me\scrolly + (y-*Me\scrolllasty)
;         *Me\scrolllastx = x
;         *Me\scrolllasty = y
;       EndIf
;       
;       If *Me\scrollx>0 : *Me\scrollx = 0 : EndIf
;       If *Me\scrolly>0 : *Me\scrolly = 0 : EndIf
;       If *Me\scrollx<-*Me\scrollmaxx : *Me\scrollx = -*Me\scrollmaxx : EndIf
;       If *Me\scrolly<-*Me\scrollmaxy : *Me\scrolly = -*Me\scrollmaxy : EndIf
;       
;     EndIf
  EndProcedure
  
EndModule

;==========================================================================
; ViewManager module Implementation
;==========================================================================
Module ViewManager
  
  Procedure Resize(*manager.ViewManager_t)    
    If Not *manager : ProcedureReturn : EndIf
    Protected w = WindowWidth(*manager\window,#PB_Window_InnerCoordinate)
    Protected h = WindowHeight(*manager\window,#PB_Window_InnerCoordinate)
      
;       Protected ev_data.EventTypeDatas_t
;       ev_data\x = 0
;       ev_data\y = 0
;       ev_data\width = w
;       ev_data\height = h
      View::Resize(*manager\main,0,0,w,h)
      View::Event(*manager\main,#PB_EventType_SizeItem)
    EndProcedure
    
  ;----------------------------------------------------------------------------------
  ; Recurse View
  ;----------------------------------------------------------------------------------
  Procedure RecurseView(*manager.ViewManager_t,*view.View::View_t)
    If *view\leaf
      If *view\active
        If *manager\active And *manager\active <> *view
          *manager\active\active = #False
          *manager\active\dirty = #True
          View::Event(*manager\active,#PB_EventType_LostFocus)
        EndIf
        *manager\active = *view
      EndIf
    Else
      If *view\left : RecurseView(*manager,*view\left) : EndIf
      If *view\right : RecurseView(*manager,*view\right) : EndIf
    EndIf
    
  EndProcedure
  
  ;----------------------------------------------------------------------------------
  ; Get Active View
  ;----------------------------------------------------------------------------------
  Procedure.i GetActiveView(*manager.ViewManager_t,x.i,y.i)
    Protected *view.View::View_t = *manager\main
    View::GetActive(*view,x,y)
    RecurseView(*manager,*manager\main)
    
  EndProcedure
  
  ;----------------------------------------------------------------------------------
  ; Drag
  ;----------------------------------------------------------------------------------
  Procedure Drag(*manager.ViewManager_t)
    ;Debug "View Manager Drag View Top ID: "+Str(*manager\active\top\id)  
  EndProcedure
  
  ;----------------------------------------------------------------------------------
  ; Set Map Element
  ;----------------------------------------------------------------------------------
  Procedure SetMapElement(*manager.ViewManager_t,*view.View::View_t)
    If *view\leaf
      Protected name.s = *view\content\name
      ; Check if already in map
      AddMapElement(*manager\views(),name)
      *manager\views() = *view\content
      Debug ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ADD VIEW MAP ELEMENT : "+name
    Else
      SetMapElement(*manager,*view\left)
      SetMapElement(*manager,*view\right)
    EndIf
    
  EndProcedure
  
  ;----------------------------------------------------------------------------------
  ; Update Map
  ;----------------------------------------------------------------------------------
  Procedure UpdateMap(*manager.ViewManager_t)
    ClearMap(*manager\views())
    SetMapElement(*manager,*manager\main)
  EndProcedure

  
  ;----------------------------------------------------------------------------------
  ; Event
  ;----------------------------------------------------------------------------------
  Procedure Event(*manager.ViewManager_t,event.i)
    Protected x,y,w,h,i,gadgetID,state
    Protected dirty.b = #False
    Protected *view.View::View_t = #Null
    If *manager = #Null Or event = -1: ProcedureReturn: EndIf
    

    Select event
      Case Globals::#EVENT_PARAMETER_CHANGED
        MessageRequester("Parameter Changed",PeekS(EventData()))
      Case Globals::#EVENT_BUTTON_PRESSED
        MessageRequester("Button Pressed",PeekS(EventData()))
      Case #PB_Event_Repaint
        View::Event(*manager\main,#PB_Event_Repaint)
      Case #PB_Event_Timer
        Select EventTimer()
;           Case #RAA_TIMELINE_TIMER
;             ; Get Timeline View
;             If FindMapElement(*manager\views(),"Timeline")
;               Protected *timeline.CView = *manager\views()
;               *timeline\Event(#PB_Event_Timer,#Null)
;             EndIf
            
        EndSelect
        
      Case #PB_Event_Menu
        Select EventMenu()
          Case #SHORTCUT_UNDO
            ;OCommandManager_Undo(*raa_cmd_manager)
          Case #SHORTCUT_REDO
            ;OCommandManager_Redo(*raa_cmd_manager)
          Default
            View::Event(*manager\active,#PB_Event_Menu)
            
        EndSelect
        
            
      Case #PB_Event_SizeWindow
        Resize(*manager)
      Case #PB_Event_MaximizeWindow
        Resize(*manager)
      Case #PB_Event_MoveWindow
        Resize(*manager)
      Case #PB_Event_Menu
        ProcedureReturn
      Default
        Protected mx = WindowMouseX(*manager\window)
        Protected my = WindowMouseY(*manager\window)
        
        GetActiveView(*manager,mx,my)
        
        If *manager\active
          Protected touch = View::TouchBorder(*manager\active,mx,my,#VIEW_BORDER_SENSIBILITY)
          
          If touch
            View::EventSplitter(*manager\active,touch)
            View::TouchBorderEvent(*manager\active,touch)
          
          Else
            View::Event(*manager\active,event)
          EndIf
        Else
          Debug "No Active View!!!"
          
        EndIf
    EndSelect
  
  EndProcedure

  ;------------------------------------------------------------------
  ; Destuctor
  ;------------------------------------------------------------------
  Procedure Delete(*e.ViewManager_t)
    FreeMemory(*e)
  EndProcedure
  
  ; ----------------------------------------------------------------------------------
  ; Constructor
  ; ----------------------------------------------------------------------------------
  Procedure New(name.s,x.i,y.i,width.i,height.i,options = #PB_Window_SystemMenu|#PB_Window_ScreenCentered)
    Protected *Me.ViewManager_t = AllocateMemory(SizeOf(ViewManager_t))
    
    InitializeStructure(*Me,ViewManager_t)
  
    
    ;Protected options.i = #PB_Window_BorderLess|#PB_Window_Maximize
    ;Protected options.i = #PB_Window_SystemMenu|#PB_Window_SizeGadget|#PB_Window_MaximizeGadget|#PB_Window_MinimizeGadget
    *Me\name = name
    *Me\window = OpenWindow(#PB_Any, 0, 0, width, height, *Me\name, options)  
;     SetWindowColor(*Me\window,RGB(240,240,240))
    EnableWindowDrop(*Me\window,#PB_Drop_Private,#PB_Drag_Move,#VIEW_SPLITTER_DROP)
    *Me\main = View::New(x.i,y.i,WindowWidth(*Me\window),WindowHeight(*Me\window),#Null,#False,"Raafal",#True)
    *Me\main\manager = *Me
    *Me\main\parentID = *Me\window
    *Me\active = *Me\main
  
    
   
;   AddKeyboardShortcut(*manager\window,#PB_Shortcut_Return                 ,#RAA_SHORTCUT_ENTER)
;     AddKeyboardShortcut(*Me\window,#PB_Shortcut_Tab                    ,#SHORTCUT_NEXT)
;     AddKeyboardShortcut(*Me\window,#PB_Shortcut_Shift|#PB_Shortcut_Tab ,#SHORTCUT_PREVIOUS)
;     AddKeyboardShortcut(*Me\window,#PB_Shortcut_Shift|#PB_Shortcut_R   ,#SHORTCUT_RESET)
;     AddKeyboardShortcut(*Me\window,#PB_Shortcut_Delete                 ,#SHORTCUT_DELETE)
;     AddKeyboardShortcut(*Me\window,#PB_Shortcut_Control|#PB_Shortcut_C ,#SHORTCUT_COPY)
;     AddKeyboardShortcut(*Me\window,#PB_Shortcut_Control|#PB_Shortcut_V ,#SHORTCUT_PASTE)
;     AddKeyboardShortcut(*Me\window,#PB_Shortcut_Control|#PB_Shortcut_X ,#SHORTCUT_CUT)
;     AddKeyboardShortcut(*Me\window,#PB_Shortcut_Control|#PB_Shortcut_Z ,#SHORTCUT_UNDO)
;     AddKeyboardShortcut(*Me\window,#PB_Shortcut_Control|#PB_Shortcut_Y ,#SHORTCUT_REDO)
;     AddKeyboardShortcut(*Me\window,#PB_Shortcut_Escape                 ,#SHORTCUT_QUIT)

    *view_manager = *Me
    
    ProcedureReturn *Me
    
  EndProcedure
 
EndModule
; IDE Options = PureBasic 5.31 (Windows - x64)
; CursorPosition = 105
; FirstLine = 45
; Folding = ------
; EnableXP
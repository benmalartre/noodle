﻿
XIncludeFile "../core/Log.pbi"
XIncludeFile "UI.pbi"

; -----------------------------------------
; LogUI Module Declaration
; -----------------------------------------
DeclareModule LogUI
  Structure LogUI_t Extends UI::UI_t
    area.i
    frame.i
  EndStructure
  
  Declare New(name.s,x.i,y.i,w.i,h.i)
  Declare Delete(*Me.LogUI_t)
;   Declare Draw(*Me.LogUI_t)
  Declare Init(*Me.LogUI_t)
  Declare Event(*Me.LogUI_t,event.i)
  Declare Term(*Me.LogUI_t)
  
  DataSection 
    LogUIVT: 
    Data.i @Init()
    Data.i @Event()
    Data.i @Term()
  EndDataSection 
EndDeclareModule

; -----------------------------------------
; LogUI Module Implementation
; -----------------------------------------
Module LogUI
  ; Constructor
  ;-------------------------------
  Procedure New(name.s,x.i,y.i,w.i,h.i)
    Protected *ui.LogUI_t = AllocateMemory(SizeOf(LogUI_t))
    *ui\name = name
    *ui\x = x
    *ui\y = y
    *ui\width = w
    *ui\height = h
    *ui\container = ContainerGadget(#PB_Any,x,y,w,h)
    *ui\frame = FrameGadget(#PB_Any,0,0,w,h,"Log")
    
    
    *ui\area = EditorGadget(#PB_Any,5,20,w-10,h-25,s)
    SetGadgetAttribute(*ui\area,#PB_Editor_ReadOnly,#True)
    SetGadgetColor(*ui\area,#PB_Gadget_BackColor,RGB(20,20,20))
    SetGadgetColor(*ui\area,#PB_Gadget_FrontColor,RGB(100,255,160))

    ;SetGadgetColor(*ui\area,#PB_Gadget_BackColor,RGB(222,222,222))
    CloseGadgetList()
    *ui\VT = ?LogUIVT
    ProcedureReturn *ui
  EndProcedure
  
  ; Destructor
  ;-------------------------------
  Procedure Delete(*Me.LogUI_t)
    If IsGadget(*Me\container): FreeGadget(*Me\container): EndIf
    FreeMemory(*Me)
  EndProcedure
  
  ; Init
  ;-------------------------------
  Procedure Init(*Me.LogUI_t)
    
  EndProcedure
  
  ; Event
  ;-------------------------------
  Procedure Event(*Me.LogUI_t,event.i)
    Select event
      Case #PB_Event_SizeWindow
        ResizeGadget(*Me\frame,0,0,GadgetWidth(*Me\container),GadgetHeight(*Me\container))
        ResizeGadget(*Me\area,5,20,GadgetWidth(*Me\container)-10,GadgetHeight(*Me\container)-25)
      Case #PB_Event_Gadget
        Select EventGadget()
          Case *Me\area
            Select EventType()
              Case #PB_EventType_Change
                ClearGadgetItems(*Me\area)
                ForEach Log::*LOGMACHINE\msgs()
                  AddGadgetItem(*Me\area,-1,Log::*LOGMACHINE\msgs()\msg)
                Next
                SetGadgetState(*Me\area,CountGadgetItems(*Me\area)-1)
            EndSelect
        EndSelect
        
        
    EndSelect
    
  EndProcedure
  
  ; Term
  ;-------------------------------
  Procedure Term(*Me.LogUI_t)
    
  EndProcedure
  
  
;   Procedure Draw(*Me.LogUI_t)
;     ClearGadgetItems(*Me\area)
;     ForEach Log::*LOGMACHINE\msgs()
;       AddGadgetItem(*Me\area,-1,Log::*LOGMACHINE\msgs()\msg)
;     Next
;     SetGadgetState(*Me\area,CountGadgetItems(*Me\area)-1)
;     
;   EndProcedure
  
  
EndModule

; IDE Options = PureBasic 5.42 LTS (Windows - x64)
; Folding = --
; EnableUnicode
; EnableXP
﻿XIncludeFile "Types.pbi"

;====================================================================================
; NODES MODULE IMPLEMENTATION
;====================================================================================
Module Nodes
  Procedure DeleteNodeDescription(*desc.NodeDescription_t)
    FreeMemory(*desc)
  EndProcedure
  
  Procedure NewNodeDescription(name.s,category.s,constructor.i)
    Protected *desc.NodeDescription_t = AllocateMemory(SizeOf(NodeDescription_t))
    *desc\name = name
    *desc\label = Mid(name, 1, Len(name)-4) 
    
    *desc\category = category
    *desc\constructor = constructor
    ProcedureReturn *desc
  EndProcedure

  
  
  Procedure DeleteNodeCategory(*category.NodeCategory_t)
    ClearMap(*category\nodes())
    FreeMemory(*category)
  EndProcedure
  
  Procedure NewNodeCategory(label.s,*desc.NodeDescription_t)
    
    Protected *category.NodeCategory_t = AllocateMemory(SizeOf(NodeCategory_t))
    InitializeStructure(*category,NodeCategory_t)
    *category\label = label
    *category\nodes(*desc\name) = *desc
    ;*category\expended = #True
    ProcedureReturn *category
  EndProcedure
  
  Procedure AppendNode(*category.NodeCategory_t,*desc.NodeDescription_t)
    *category\nodes(*desc\name) = *desc
  EndProcedure



  Procedure AppendDescription(*desc.NodeDescription_t)
    
    ; Push Node Map
    Nodes::*graph_nodes(*desc\name) = *desc
    
    If *desc\category = "" : ProcedureReturn : EndIf
    ;Push Category Map
    Protected *category.NodeCategory_t = Nodes::*graph_nodes_category(*desc\category)
    If Not *category
      Nodes::*graph_nodes_category(*desc\category) = newNodeCategory(*desc\category,*desc)
    Else
      AppendNode(*category,*desc)
    EndIf
    
  EndProcedure
EndModule



;----------------------------------------------------------------------------
; Nodes
;----------------------------------------------------------------------------
CompilerIf #PB_Compiler_OS = #PB_OS_Windows
  IncludePath "..\nodes"
CompilerElse
  IncludePath "../nodes"
CompilerEndIf
  
  ; Hierarchy
  XIncludeFile "SceneNode.pbi"
  XIncludeFile "Object3DNode.pbi"

;   ; Constants
;   XIncludeFile "raafal.graph.node.bool.pbi"
;   XIncludeFile "raafal.graph.node.integer.pbi"
  XIncludeFile "FloatNode.pbi"
;   XIncludeFile "raafal.graph.node.vector3.pbi"
;   XIncludeFile "raafal.graph.node.quaternion.pbi"
;   XIncludeFile "raafal.graph.node.string.pbi"
;   XIncludeFile "raafal.graph.node.time.pbi"
;   
;   ; Arrays
;   XIncludeFile "raafal.graph.node.buildarray.pbi"
;   XIncludeFile "raafal.graph.node.buildarrayfromconstant.pbi"
;   XIncludeFile "raafal.graph.node.buildindexarray.pbi"
;   ; XIncludeFile "raafal.graph.node.arraysubindices.pbi"
;   
;   ; Utils
;   XIncludeFile "raafal.graph.node.if.pbi"
  XIncludeFile "AddNode.pbi"
;   XIncludeFile "raafal.graph.node.modulo.pbi"
;   XIncludeFile "raafal.graph.node.random.pbi"
; ;   XIncludeFile "raafal.graph.node.noise.pbi"
;   XIncludeFile "raafal.graph.node.multiply.pbi"
;   XIncludeFile "raafal.graph.node.multiplybyscalar.pbi"
;   XIncludeFile "raafal.graph.node.trigonometry.pbi"
;   XIncludeFile "raafal.graph.node.integertofloat.pbi"
;   XIncludeFile "raafal.graph.node.floattovector3.pbi"
;   XIncludeFile "raafal.graph.node.floattocolor.pbi"
;   XIncludeFile "raafal.graph.node.vector3tofloat.pbi"
;   XIncludeFile "raafal.graph.node.srttomatrix.pbi"
;   XIncludeFile "raafal.graph.node.subtract.pbi"
;   
;   ; Operators
  XIncludeFile "TreeNode.pbi"
;   XIncludeFile "raafal.graph.node.addpoint.pbi"
  XIncludeFile "GetDataNode.pbi"
  XIncludeFile "SetDataNode.pbi"
;   
;   ; Topology
;   XIncludeFile "raafal.graph.node.primitivemesh.pbi"
;   XIncludeFile "raafal.graph.node.mergetopoarray.pbi"
;   
;   ; Shaders
;   XIncludeFile "raafal.graph.node.layer.pbi"
;   XIncludeFile "raafal.graph.node.shader.pbi"
;   XIncludeFile "raafal.graph.node.framebuffer.pbi"
; ;   XIncludeFile "raafal.graph.node.renderbuffer.pbi"
;   
  IncludePath "../"
; IDE Options = PureBasic 5.42 LTS (Windows - x64)
; CursorPosition = 46
; FirstLine = 43
; Folding = --
; EnableUnicode
; EnableThread
; EnableXP
XIncludeFile "../core/Math.pbi"
XIncludeFile "../core/Array.pbi"
XIncludeFile "../core/Attribute.pbi"
; XIncludeFile "../objects/3DObject.pbi"

; ============================================================================
; GRAPH TYPES MODULE DECLARATION
; ============================================================================
DeclareModule Graph
  ; ==========================================================================
  ;  GLOBALS
  ; ==========================================================================
  Enumeration
    #Graph_Selection_None
    #Graph_Selection_Node
    #Graph_Selection_Connexion
    #Graph_Selection_Port
    #Graph_Selection_Rectangle
    #Graph_Selection_Dive
    #Graph_Selection_Climb
  EndEnumeration
  
  #Graph_Background_Line = $555555
  #Graph_Background_Color = $666666
  #Graph_Compound_Border = 24
  #Graph_Compound_Color = $999999

  #Node_PortRadius = 4
  #Node_PortContour = #True
  #Node_PortShiftX = 0
  #Node_PortSpacing = 20
  #Node_DrawShadow = #True
  #Node_ShadowX = 2
  #Node_ShadowY = 3
  #Node_ShadowR = 7
  #Node_CornerRadius = 7
  #Node_TitleHeight = 7
  #Node_BorderUnselected = $66333333
  #Node_BorderSelected = $FFEEEEEE
  #Node_EditButtonRadius = 5
  #Node_EditButtonShiftX = 10
  #Node_EditButtonShiftY = 10
  #Node_EditButtonColor = $33999999
  
  Enumeration 
    #Node_StateOK
    #Node_StateError
    #Node_StateInvalid
    #Node_StateUndefined
  EndEnumeration
  
  Enumeration
    #Graph_Context_Hierarchy
    #Graph_Context_Shader
    #Graph_Context_Modeling
    #Graph_Context_Compositing
    #Graph_Context_Operator  
    #Graph_Context_Simulation
  EndEnumeration
  
  Global Dim graph_context.s(6)
  graph_context(0) = "Hierarchy"
  graph_context(1) = "Shader"
  graph_context(2) = "Modeling"
  graph_context(3) = "Compositing"
  graph_context(4) = "Operator"
  graph_context(5) = "Simulation"
  
  Global GRAD_OUT = RGBA(0,0,0,0)
  Global GRAD_IN = RGBA(0,0,0,100)
  Global GRAPH_FONT_NODE
  Global GRAPH_FONT_PORT
  
  ; ==========================================================================
  ; DECLARE
  ; ==========================================================================
  Declare GetDataNode_ResolveReference(*node)
  Declare SetDataNode_ResolveReference(*node)
  Declare SwitchContext(id.i)
  
  ; ==========================================================================
  ; MACROS
  ; ==========================================================================
  Macro AttachListElement(p,e)
    AddElement(p)
    p = e
  EndMacro
  
  Macro ExtractListElement(p,e)
    e = p
    DeleteElement(p)
  EndMacro
  
  Macro AttachMapElement(m,k,e)
    AddMapElement(m,k)
    m = e
  EndMacro
  
  Macro ExtractMapElement(m,k,e)
    e = m
    DeleteMapElement(m,k)
  EndMacro

EndDeclareModule

; ============================================================================
; NODE PORT MODULE DECLARATION
; ============================================================================
DeclareModule NodePort
  UseModule Math
  UseModule Graph
  ;---------------------------------------------------------------------------
  ; GraphNodePort
  ;---------------------------------------------------------------------------
  Structure NodePort_t
    viewx.i
    viewy.i
    viewr.i
    io.b
    connected.b
    selected.b
    id.i
    name.s
    decoratedname.s
    
    polymorph.b
    datatype.i
    datacontext.i
    datastructure.i
    
    currenttype.i
    currentcontext.i
    currentstructure.i
    
    constant.b
    
    ; Reference
    refchanged.b
    reference.s
    daisyreference.s
    
    ;Connexion
    *source.NodePort_t
    *connexion;.Connexion_t
    
    *node       ;Parent Node
    *value
    
    
    color.q
    percx.i
    percy.i
  EndStructure
  
  ;---------------------------------------------------------------------------
  ; Functions Declaration
  ;---------------------------------------------------------------------------
  Declare New(*parent,name.s,io.b=#False,datatype.i=Attribute::#ATTR_TYPE_UNDEFINED,datacontext.i=Attribute::#ATTR_CTXT_ANY,datastructure.i=Attribute::#ATTR_STRUCT_ANY)
  Declare Delete(*port.NodePort_t)
  Declare Echo(*port.NodePort_t)
  Declare GetColor(*port.NodePort_t)
  Declare Init(*port.NodePort_t)
  Declare InitFromReference(*port.NodePort_t,*attr.Attribute::Attribute_t)
  Declare.s AcquireReferenceData(*port.NodePort_t)
  Declare AcquireInputData(*port.NodePort_t)
  Declare AcquireOutputData(*port.NodePort_t)
  Declare Update(*port.NodePort_t,type.i=Attribute::#ATTR_TYPE_UNDEFINED,context.i=Attribute::#ATTR_CTXT_ANY,struct.i=Attribute::#ATTR_STRUCT_ANY)
  Declare GetDataType(*Me.NodePort_t)
  Declare IsConnectable(*Me.NodePort_t,*Other.NodePort_t)
  Declare DecorateName(*Me.NodePort_t,width.i)
  Declare AcceptConnexion(*p.NodePort_t,datatype.i=Attribute::#ATTR_TYPE_UNDEFINED,datacontext.i=Attribute::#ATTR_CTXT_ANY,datastructure.i=Attribute::#ATTR_STRUCT_ANY)
EndDeclareModule

; ============================================================================
; COMPOUND NODE PORT MODULE DECLARATION
; ============================================================================
DeclareModule CompoundNodePort
  ;---------------------------------------------------------------------------
  ; CompoundNodePort
  ;---------------------------------------------------------------------------
  ;{
  Structure CompoundNodePort_t Extends NodePort::NodePort_t
    
  EndStructure
  ;}
EndDeclareModule

Module CompoundNodePort
EndModule


; ============================================================================
; GRAPH CONNEXION MODULE DECLARATION
; ============================================================================
DeclareModule Connexion
  UseModule Math
  ;---------------------------------------------------------------------------
  ; GraphConnexion
  ;---------------------------------------------------------------------------
  Structure Connexion_t
    a.v2f32
    b.v2f32
    c.v2f32
    d.v2f32
    linear.b
    accuracy.f
    method.i
    color.i
    connected.b
    antialiased.b
    *start.NodePort::NodePort_t
    *end.NodePort::NodePort_t
  EndStructure
  
  #Graph_Bezier_DashedLines = #False
  #Graph_Bezier_Thickness = 2
  
  Macro DrawLine(x1,y1,x2,y2,color,antialiased)
    Select antialiased
      Case #True
        ;NormalL(x1,y1,x2,y2,color,#Graph_Bezier_Thickness)
        LineXY(x1,y1,x2,y2,color)
      Case #False
        LineXY(x1,y1,x2,y2,color)
    EndSelect
  EndMacro

  
  Declare New(*p.NodePort::NodePort_t)
  Declare Create(*c.Connexion_t,*s.NodePort::NodePort_t,*e.NodePort::NodePort_t)
  Declare Init(*b.Connexion_t,color.i)
  Declare Delete(*c.Connexion_t)
  Declare ViewPosition(*c.Connexion_t)
  Declare Drag(*c.Connexion_t,x.i,y.i)
  Declare RecursePossible(*connexion.Connexion_t,datatype.i,datacontext.i,datastructure.i,way.b)
  Declare.b Possible(*c.Connexion_t,*p.NodePort::NodePort_t)
  Declare.b Connect(*c.Connexion_t,*p.NodePort::NodePort_t)
  Declare SetHead(*c.Connexion_t,*p.NodePort::NodePort_t)
  Declare ShareParentNode(*c.Connexion_t)
  Declare Draw(*c.Connexion_t)
  Declare Set(*c.Connexion_t,x1.i,y1.i,x2.i,y2.i)
EndDeclareModule

; ============================================================================
; NODE MODULE DECLARATION
; ============================================================================
DeclareModule Node
  ;---------------------------------------------------------------------------
  ; GraphNode
  ;---------------------------------------------------------------------------
  Structure Node_t
    *VT
    ;Description
    name.s
    selected.b
    label.s
    type.s
    *parent.CGraphNode_t
    *parent3dobject.C3DObject_t
  
    ;Global infos 
    posx.l
    posy.l
    width.l
    height.l
    
    ;Relative to view
    viewx.i
    viewy.i
    viewwidth.i
    viewheight.i
    z.i
    
    ;color
    red.i
    green.i
    blue.i
    color.i
    
    ;size
    step1.f
    step2.f
    
    ;state
    state.i
    errorstr.s
    leaf.b
    root.b
  
    ;ports
    List *inputs.NodePort::NodePort_t()
    List *outputs.NodePort::NodePort_t()
  
    ;embedded nodes
    List *nodes.Node::Node_t()
    List *connexions.Connexion::Connexion_t()
  
    ;current port
    *port.CGraphNodePort 
  EndStructure
  
  Global NODE_BORDER_WIDTH.i
  Global NODE_FONT_SIZE.i
  Global NODE_ZOOM_CURRENT.i
  Global NODE_FONT_WIDTH.i
  
  Interface INode
    Evaluate()
  EndInterface
  
  ; ============================================================================
  ;  Constructor Macro
  ; ============================================================================
  Macro Node_INI(cls,p,t,x,y,w,h,c)
    
    ; ---[ Init Members ]-------------------------------------------------------
    *Me\parent = p
;     *Me\parent3dobject = p\parent3dobject
    *Me\type = t
    *Me\posx = x
    *Me\posy = y
    *Me\width = w
    *Me\height = h
    *Me\color = c
    *Me\state = Graph::#Node_StateUndefined
    *Me\leaf = #True
    
    ; ---[ Initialize Structure ]-----------------------------------------------
    InitializeStructure(*Me,cls#_t)
  
    ; ---[ Init Node ]----------------------------------------------------------
    Init(*Me)
    
  EndMacro
  
  ;---------------------------------------------------------------------------
  ; Functions Declaration
  ;---------------------------------------------------------------------------
  Declare New(*tree.Node::Node_t,name.s="",x.i=0,y.i=0,w.i=100,h.i=50,c.i=0)
  Declare Delete(*node.Node_t)
  Declare Update(*node.Node_t)
  Declare.s GetName(*n.Node_t)
  Declare GetSize(*n.Node_t)
  Declare Draw(*n.Node_t,fontnode.i,fontport.i)
  Declare ViewPosition(*n.Node_t,z.i,x.i,y.i)
  Declare.b IsLeaf(*n.Node_t)
  Declare SetColor(*n.Node_t,r.i,g.i,b.i)
  Declare Drag(*n.Node_t,x.i,y.i,zoom.i)
  Declare.i IsUnderMouse(*n.Node_t,x.l,y.l)
  Declare.b InsideNode(*node.Node_t,*parent.Node_t)
  Declare.i Pick(*n.Node_t,x.l,y.l,connect.b=#False)
  Declare.b PickPort(*n.Node_t,*p.NodePort::NodePort_t,id.i,x.i,y.i)
  Declare.i GetPortByID(*n.Node_t,id.i)
  Declare.i GetPortByName(*n.Node_t,name.s)
  Declare.i SetInputPortID(*n.Node_t,*p.NodePort::NodePort_t,id.i = -1)
  Declare SetOutputPortID(*n.Node_t,*p.NodePort::NodePort_t,id.i = -1)
  Declare UpdatePorts(*n.Node_t,datatype.i,datacontext.i,datastructure.i)
  Declare AddInputPort(*n.Node_t,name.s,datatype.i=Attribute::#ATTR_TYPE_UNDEFINED,datacontext.i=Attribute::#ATTR_CTXT_ANY,datastructure.i=Attribute::#ATTR_STRUCT_ANY)
  Declare RemoveInputPort(*n.Node_t,id.i)
  Declare AddOutputPort(*n.Node_t,name.s,datatype.i=Attribute::#ATTR_TYPE_UNDEFINED,datacontext.i=Attribute::#ATTR_CTXT_ANY,datastructure.i=Attribute::#ATTR_STRUCT_ANY)
  Declare Inspect(*n.Node_t)

EndDeclareModule

; ============================================================================
; COMPOUND NODE PORT MODULE DECLARATION
; ============================================================================
DeclareModule CompoundNode
  ;---------------------------------------------------------------------------
  ; CompoundNode
  ;---------------------------------------------------------------------------
  Structure CompoundNode_t Extends Node::Node_t
    introspected.b
    iexpanded.b
    iexpand.i
    oexpanded.b
    oexpand.i
    exposeinput.b
    exposeoutput.b
  ;   List *nodes.CGraphNode_t()
  ;   List *connexions.CGraphConnexion_t()
  EndStructure
  
  Declare New(*nodes.CArray::CArrayPtr,*parent.Node::Node_t,x.i,y.i,w.i,h.i,c.i)
  Declare Delete(*node.CompoundNode_t)
  Declare Implode(*compound.CompoundNode_t,*nodes.CArray::CArrayPtr)
  Declare Explode(*node.CompoundNode_t)

EndDeclareModule

; ============================================================================
;  GRAPH TREE MODULE DECLARARTION
; ============================================================================
DeclareModule Tree
  Structure Tree_t
    
    ; ---[ Definition ]--------------------------------------
    name.s
    
    ; ---[ State ]-------------------------------------------
    dirty.b
    
    ; ---[ Objects ]-----------------------------------------
    *root.Node::Node_t
    *current.Node::Node_t       ; Currently inspected node
  
  ;   ; ---[ Lists ]-------------------------------------------
    List *a_nodes.Node::Node_t()
    List *a_connexions.Connexion::Connexion_t()
    
    ; ---[ current evaluate dbranch nodes ]------------------
    *nodes.CArray::CArrayPtr
  
  EndStructure


  ; ----------------------------------------------------------------------------
  ;  FORWARD DECLARATION
  ; ----------------------------------------------------------------------------
  Declare New(*obj,name.s="Tree",context.i=Graph::#Graph_Context_Operator)
  Declare Delete(*tree.Tree_t)
  Declare RecurseNodes(*Me.Tree_t,*current.Node::Node_t)
  Declare EvaluatePort(*Me.Tree_t,*port.NodePort::NodePort_t)
  Declare Update(*Me.Tree_t)
  Declare AddNode(*Me.Tree_t,name.s,x.i,y.i,w.i,h.i,c.i)  
  Declare RemoveNode(*Me.Tree_t,*other.Node::Node_t)
  Declare RemoveLastNode(*Me.Tree_t)
  Declare ConnectNodes(*Me.Tree_t,*parent.Node::Node_t,*s.NodePort::NodePort_t,*e.NodePort::NodePort_t)
  Declare DisconnectNodes(*Me.Tree_t,*parent.Node::Node_t,*start.NodePort::NodePort_t,*end.NodePort::NodePort_t)
  Declare DisconnectNode(*Me.Tree_t,*n.Node::Node_t)
  Declare DeleteSelected(*Me.Tree_t)
EndDeclareModule

;====================================================================================
; NODES MODULE DECLARATION
;====================================================================================
DeclareModule Nodes
  ;----------------------------------------------------------------------------
  ; Node Description
  ;----------------------------------------------------------------------------
  Prototype GRAPHNODECONSTRUCTOR(*tree.Node::Node_t,name.s,x.i,y.i,w.i,h.i,c.i)
  
  Structure NodeDescription_t
    name.s
    label.s
    category.s
    constructor.GRAPHNODECONSTRUCTOR
    color_id.i
    selected.b
  EndStructure
  
  ;----------------------------------------------------------------------------
  ; Node Category
  ;----------------------------------------------------------------------------
  Structure NodeCategory_t
    label.s
    expended.b
    Map *nodes.NodeDescription_t()
  EndStructure
  
  ;----------------------------------------------------------------------------
  ; Nodes Global Management 
  ;----------------------------------------------------------------------------
  Global NewMap *graph_nodes.NodeDescription_t()
  Global NewMap *graph_nodes_category.NodeCategory_t()
  
  Declare NewNodeDescription(name.s,category.s,constructor.i)
  Declare DeleteNodeDescription(*desc.NodeDescription_t)
  
  Declare NewNodeCategory(label.s,*desc.NodeDescription_t)
  Declare DeleteNodeCategory(*category.NodeCategory_t)
  Declare AppendNode(*category.NodeCategory_t,*desc.NodeDescription_t)
  Declare AppendDescription(*desc.NodeDescription_t)
EndDeclareModule


; ============================================================================
;  EOF
; ============================================================================
; IDE Options = PureBasic 5.42 LTS (Windows - x64)
; CursorPosition = 307
; FirstLine = 263
; Folding = ---
; EnableXP
﻿XIncludeFile "../core/Array.pbi"
XIncludeFile "Types.pbi"
XIncludeFile "../objects/Object3D.pbi"

;====================================================================================
; GRAPH MODULE IMPLEMENTATION
;====================================================================================
Module Graph
  Procedure  GetDataNode_ResolveReference(*n)
    Protected *node.Node::Node_t = *n
    Debug "GetData Node Rsolve Reference Called..."
    Protected *obj.Object3D::Object3D_t = *node\parent3dobject
  
    Protected refname.s = NodePort::AcquireReferenceData(*node\inputs())
    
    Protected fields.i = CountString(refname, ".")+1
    Protected base.s = StringField(refname, 1,".")
    ;*node\label = refname
    Protected *output.NodePort::NodePort_t = Node::GetPortByName(*node,"Data")
    If base ="Self" Or base ="This"
      Protected *attribute.Attribute::Attribute_t = *obj\m_attributes(StringField(refname, 2,"."))
      If *attribute
        *output\currenttype = *attribute\datatype
        *output\currentcontext = *attribute\datacontext
        *output\currentstructure = *attribute\datastructure
        NodePort::Init(*output)
        *output\value = *attribute\data
      EndIf
      
    Else
      *output\value = #Null
    EndIf
  EndProcedure
  
  Procedure SetDataNode_ResolveReference(*n)
    Protected *node.Node::Node_t = *n
    Protected *ref.NodePort::NodePort_t = Node::GetPortByName(*node,"Reference")
    Protected refname.s = NodePort::AcquireReferenceData(*ref)
    Debug "[SetDataNode] Reference Name : "+refname
    
    If refname
      Protected fields.i = CountString(refname, ".")+1
      Protected base.s = StringField(refname, 1,".")
      
      If base ="Self" Or base ="This"
        Protected *obj.Object3D::Object3D_t = *node\parent3dobject
        Protected *attribute.Attribute::Attribute_t = *obj\m_attributes(StringField(refname, 2,"."))
        If *attribute
          Protected *input.NodePort::NodePort_t = Node::GetPortByName(*node,"Data")
  
          NodePort::InitFromReference(*input,*attribute)
          *node\state = Graph::#Node_StateOK
          *node\errorstr = ""
        EndIf
        
        Debug "Search Attribute Named : "+StringField(refname, 2,".")+" ---> "+Str(*attribute)
      EndIf
    Else
      *node\state = Graph::#Node_StateError
      *node\errorstr = "[ERROR] Input Empty"
    EndIf
    
    
  ;   Protected *obj.C3DObject_t = *node\parent3dobject
  ;   Protected *data.CGraphNodePort_t = OGraphNode_GetPortByName(*node,"Data")
  ;   Protected *ref.CGraphNodePort_t = OGraphNode_GetPortByName(*node,"Reference")
  ;   Protected refname.s = OGraphNodePort_AcquireReferenceData(*ref)
  ;   
  ;   If refname = ""
  ;     *node\state = #RAA_GraphNode_StateUndefined
  ;     *node\errorstr = ""
  ;     *node\attribute = #Null
  ;     ProcedureReturn
  ;   Else
  ;     Debug "[SetDataNode] Reference Name : "+refname
  ;   EndIf
  ;   
  ;   Protected fields.i = CountString(refname, ".")+1
  ;   Protected base.s = StringField(refname, 1,".")
  ; 
  ;   If base ="Self" Or base ="This"
  ;     *node\attribute = *obj\m_attributes(StringField(refname, 2,"."))
  ;     Debug "Search Attribute Named : "+StringField(refname, 2,".")+" ---> "+Str(*node\attribute)
  ;     
  ;     If Not *node\attribute
  ;       ; Here we create a nex Attribute
  ;       *node\state = #RAA_GraphNode_StateInvalid
  ;     Else
  ;       *data\currenttype = *node\attribute\datatype    
  ;       Debug "Attribute Name : "+*node\attribute\name
  ;       Debug "Input Current Type : "+Str(*data\currenttype)
  ;       Debug "Attribute Current Type : "+Str(*node\attribute \datatype)
  ;       
  ;       If *data\connected
  ;         If Not *data\source\currenttype = *node\attribute\datatype
  ;           *node\state = #RAA_GraphNode_StateError
  ;           *node\errorstr = "[SetDataNode] input data type doesn't match output data type"
  ;           Debug "[SetDataNode] failed get attribute"
  ;         Else
  ;           *node\state = #RAA_GraphNode_StateOK
  ;           *node\errorstr = ""
  ;           Debug "[SetDataNode] succesfully get attribute"
  ;         EndIf
  ;       Else
  ;         *node\state = #RAA_GraphNode_StateOK
  ;       EndIf
  ;       
  ;       
  ;     EndIf
  ;     
  ;   Else
  ;     *node\attribute = #Null
  ;     *node\state = #RAA_GraphNode_StateUndefined
  ;     *node\errorstr = "[SetDataNode] input data is undefined"
  ;   EndIf
  
  EndProcedure
  
  ;------------------------------
  ; Switch Context
  ;------------------------------
  Procedure SwitchContext(ID)
    Select ID
      Case Graph::#Graph_Context_Compositing
        MessageRequester("GRAPH SWITCH CONTEXT","COMPOSITING")
       
     Case Graph::#Graph_Context_Hierarchy
       MessageRequester("GRAPH SWITCH CONTEXT","HIERARCHY")
       
     Case Graph::#Graph_Context_Modeling
       MessageRequester("GRAPH SWITCH CONTEXT","MODELING")
       
     Case Graph::#Graph_Context_Operator
       MessageRequester("GRAPH SWITCH CONTEXT","OPERATOR")
       
     Case Graph::#Graph_Context_Shader
       MessageRequester("GRAPH SWITCH CONTEXT","SHADER")
       
     Case Graph::#Graph_Context_Simulation
       MessageRequester("GRAPH SWITCH CONTEXT","SIMULATION")
       
    EndSelect
    
    
  EndProcedure

EndModule

; IDE Options = PureBasic 5.42 LTS (Windows - x64)
; CursorPosition = 2
; Folding = -
; EnableUnicode
; EnableXP
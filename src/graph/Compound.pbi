﻿XIncludeFile "../core/Array.pbi"
XIncludeFile "Types.pbi"

;====================================================================================
; COMPOUND NODE MODULE IMPLEMENTATION
;====================================================================================
Module CompoundNode
  Procedure  New(*nodes.CArray::CArrayPtr,*parent.Node::Node_t,x.i,y.i,w.i,h.i,c.i)
    ; ---[ Allocate Node Memory ]---------------------------------------------
    Protected *Me.CompoundNode_t = AllocateMemory(SizeOf(CompoundNode_t))
    
    ; ---[ Init Members ]-------------------------------------------------------
    *Me\parent = *parent
    *Me\label = "CompoundNode"
    *Me\name = "CompoundNode"
    ;*Me\parent3dobject = *parent\parent3dobject
    *Me\type = "CompoundNode"
    *Me\posx = x
    *Me\posy = y
    *Me\width = w
    *Me\height = h
    *Me\color = c
    *Me\state = Graph::#Node_StateUndefined
    
    *Me\iexpanded = #True
    *Me\iexpand = 200
    
    ; ---[ Initialize Structure ]-----------------------------------------------
    InitializeStructure(*Me,CompoundNode_t)
  
    ProcedureReturn (*Me)
    
  EndProcedure
  
  Procedure Delete(*Me.CompoundNode_t)
    FreeMemory(*Me)
  EndProcedure
  
  Procedure Implode(*Me.CompoundNode_t,*nodes.CArray::CArrayPtr)
    Protected msg.s = "EXplode Compound "+*Me\name+Chr(10)
    ForEach *Me\nodes()
      msg + *Me\nodes()\name+Chr(10)
    Next
    MessageRequester("IMPLODE NODE",msg)
  EndProcedure
  
  Procedure Explode(*Me.CompoundNode_t)
    Protected msg.s = "EXplode Compound "+*Me\name+Chr(10)
    ForEach *Me\nodes()
      msg + *Me\nodes()\name+Chr(10)
    Next
    MessageRequester("EXPLODE NODE",msg)
  EndProcedure
  
EndModule
; IDE Options = PureBasic 5.42 LTS (MacOS X - x64)
; CursorPosition = 49
; FirstLine = 23
; Folding = -
; EnableUnicode
; EnableXP
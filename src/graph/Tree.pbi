﻿XIncludeFile "../core/Array.pbi"
XIncludeFile "../core/Math.pbi"
XIncludeFile "Types.pbi"
XIncludeFile "../objects/Object3D.pbi"


; ============================================================================
; GRAPH TREE MODULE IMPLEMENTATION
; ============================================================================
Module Tree
  ;-----------------------------------------------------------------------------
  ; Recurse Node
  ;-----------------------------------------------------------------------------
  Procedure RecurseNodes(*Me.Tree_t,*current.Node::Node_t)
    If Not *current : ProcedureReturn : EndIf
  
    ForEach *current\inputs()
      If *current\inputs()\connected
        
        CArray::Append(*Me\nodes,*current\inputs()\source\node)
        RecurseNodes(*Me,*current\inputs()\source\node)
      EndIf
    Next
  EndProcedure

  ;-----------------------------------------------------------------------------
  ; Evaluate Port
  ;-----------------------------------------------------------------------------
  Procedure EvaluatePort(*Me.Tree_t,*port.NodePort::NodePort_t)
    ;recurse to leaf node
    If Not *port\connected Or *port\connexion = #Null : ProcedureReturn(void) : EndIf
    CArray::SetCount(*Me\nodes,0)
    CArray::Append(*Me\nodes,*port\source\node)
  
    RecurseNodes(*Me,*port\source\node)
    Protected *current.Node::Node_t
   
    Protected i = CArray::GetCount(*Me\nodes)-1
    Repeat 
      *current = CArray::GetValue(*Me\nodes,i)
      Node::Update(*current);\Evaluate()
      i-1
    Until i<0
  EndProcedure

  ;-----------------------------------------------------------------------------
  ; Update
  ;-----------------------------------------------------------------------------
  Procedure Update(*Me.Tree_t)
    ForEach *Me\root\inputs()
      EvaluatePort(*Me,*Me\root\inputs())
    Next
    
  EndProcedure
  
  ;-----------------------------------------------------------------------------
  ; Add Node
  ;-----------------------------------------------------------------------------
  Procedure.i AddNode(*Me.Tree_t,name.s,x.i,y.i,w.i,h.i,c.i)  
    Protected *node.Node::Node_t = Node::New(*Me,name.s,x,y,w,h,c)
   
    If Not *node
      ProcedureReturn #Null
    ElseIf name = "TreeNode" Or name = "LayerNode"
      ProcedureReturn *node
    Else
      Graph::AttachListElement(*Me\current\nodes(),*node) 
      *Me\dirty = #True
      ProcedureReturn *node
    EndIf
    
  EndProcedure


  ;----------------------------------------------------------------------------------------
  ; Remove Node
  ;----------------------------------------------------------------------------------------
  Procedure RemoveNode(*Me.Tree_t,*other.Node::Node_t)
    If *other : Debug *other\name:EndIf
      
    Protected *node.Node::Node_t = #Null
    ForEach *Me\current\nodes()
      Debug *Me\current\nodes()\label
      If *Me\current\nodes() = *other
        Graph::ExtractListElement(*Me\current\nodes(),*node)
        Break
      EndIf
    Next
    
    If *node
      DisconnectNode(*Me,*node)
      Node::Delete(*node)
    
      *Me\dirty = #True
    EndIf
    
  EndProcedure

  ;----------------------------------------------------------------------------------------
  ; Remove Last Node
  ;----------------------------------------------------------------------------------------
  Procedure RemoveLastNode(*Me.Tree_t)
    LastElement(*Me\current\nodes())
    
    Protected *node.Node::Node_t
    Graph::ExtractListElement(*Me\current\nodes(),*node)
    Node::Delete(*node)
  EndProcedure

  ;-----------------------------------------------------------------------------
  ;Connect/Disconnect Nodes
  ;-----------------------------------------------------------------------------
  Procedure ConnectNodes(*Me.Tree_t,*parent.Node::Node_t,*s.NodePort::NodePort_t,*e.NodePort::NodePort_t)
    
    If *s\io And *e\connected
      ProcedureReturn
    ElseIf *e\io And *s\connected
      ProcedureReturn
    EndIf
    
      
    Protected *connexion.Connexion::Connexion_t = Connexion::New(*s)
    
    *connexion\start = *s
    *connexion\end = *e
    
    ;Reverse connexion as it's from OUT to IN
    If *s\io
      *e\source = *s
      
      Connexion::Create(*connexion,*e,*s)
    Else
      *s\source = *e
      Connexion::Create(*connexion,*s,*e)
      
    EndIf
    
    *connexion\end\connected = #True
    LastElement(*parent\connexions())
    Graph::AttachListElement(*parent\connexions(),*connexion)
    
  EndProcedure

  ;-----------------------------------------------------------------------------
  ; Disconnect Nodes
  ;-----------------------------------------------------------------------------
  Procedure DisconnectNodes(*Me.Tree_t,*parent.Node::Node_t,*start.NodePort::NodePort_t,*end.NodePort::NodePort_t)
    ; Check connexion
    If Not  *start\connected Or Not *end\connected : ProcedureReturn : EndIf
    Protected *cnx = *end\connexion
    
    Protected found.b = #False
    
    ; Remove connexion from List
    ForEach *parent\connexions()
      If *parent\connexions() = *cnx
        DeleteElement(*parent\connexions())
        found = #True
        Break
      EndIf
    Next
    
    If found
      ; Delete connexion
      Connexion::Delete(*cnx)
      *start\connected = #False
      *end\connected = #False
      
      ; Set Dirty Flag
      *Me\dirty = #True
    EndIf
    
  EndProcedure
  ;}

  ;-----------------------------------------------------------------------------
  ; Disconnect Node
  ;-----------------------------------------------------------------------------
  ;{
  Procedure DisconnectNode(*Me.Tree_t,*n.Node::Node_t)
    Protected *port.NodePort::NodePort_t
    Protected *connexion.Connexion::Connexion_t
  
    ForEach *Me\current\connexions()
      If *Me\current\connexions()\start\node = *n Or *Me\current\connexions()\end\node = *n
        *Me\current\connexions()\end\connected = #False
        *Me\current\connexions()\end\value = #Null
        Graph::ExtractListElement(*Me\current\connexions(),*connexion)
        FreeMemory(*connexion)
      EndIf
      
    Next
    
  
  EndProcedure
  ;}

  ;-----------------------------------------------------------------------------
  ; Delete Selected
  ;-----------------------------------------------------------------------------
  ;{
  Procedure DeleteSelected(*Me.Tree_t)
    Protected *node.Node::Node_t
    Protected id=0
    ForEach *Me\root\nodes()
      *node = *Me\root\nodes()
      If *node\selected And Not *node\type = "TreeNode"
        RemoveNode(*Me,id)
      EndIf
      id + 1
      
    Next
    
  EndProcedure
  ;}

  ;-----------------------------------------------------------------------------
  ; Create Compound
  ;-----------------------------------------------------------------------------
  Procedure CreateCompound(*tree.Tree_t,*nodes.CArray::CArrayPtr,*parent.Node::Node_t)
  
    If Not *tree
      Debug "[View Graph] Create Compound : Tree Invalid"
      ProcedureReturn
    EndIf
    
    
    Protected *compound.CompoundNode::CompoundNode_t = CompoundNode::New(*nodes,*parent,0,0,200,100,RGB(100,100,100))
  
    *tree\dirty = #True
    Protected *n.Node::Node_t
    Protected *extracted.Node::Node_t
    Protected *cnx.Connexion::Connexion_t
    Protected i.i
    
    ; Gather and Swap Nodes
    For i=0 To CArray::GetCount(*nodes)-1
      *n = CArray::GetValue(*nodes,i)
      
      ForEach *parent\nodes()
        If *parent\nodes() = *n
          Break
        EndIf
      Next
      
      Graph::ExtractListElement(*parent\nodes(),*extracted)
      Graph::AttachListElement(*compound\nodes(),*extracted)
    Next
    
    ; Gather , Swap and Reconnect Connexions
    For i=0 To CArray::GetCount(*nodes)-1
      *n = CArray::GetValue(*nodes,i)
      ForEach *n\inputs()
          If *n\inputs()\connected
            *cnx = *n\inputs()\connexion
            If Connexion::ShareParentNode(*cnx)
              Graph::AttachListElement(*compound\connexions(),*cnx)
              ForEach *parent\connexions()
                If *parent\connexions() = *cnx
                  Graph::ExtractListElement(*parent\connexions(),*cnx)
                  Break
                EndIf
              Next
            Else
              Debug "----------------------------------------- Create Compound -----------------------------------"
              Debug "Shared Connexion we have to Recreate It!!!"
            EndIf
            
          EndIf
        Next
      Next i
     
    
    
    Graph::AttachListElement(*parent\nodes(),*compound) 
    
  EndProcedure



  ; ----------------------------------------------------------------------------
  ;  Destructor
  ; ----------------------------------------------------------------------------
  ;{
  Procedure Delete( *Me.Tree::Tree_t )
    ; ---[ Deallocate Underlying Arrays ]---------------------------------------
    Protected *root.Node::Node_t = *Me\root
    
    ForEach *Me\root\connexions()
      Connexion::Delete(*Me\root\connexions())
    Next
    Protected *node.Node::Node_t
    ForEach *Me\root\nodes()
      *node = *Me\root\nodes()
      Node::Delete(*node)
    Next
    
    Node::Delete(*root)
    
    ; ---[ Deallocate Memory ]--------------------------------------------------
    FreeMemory( *Me )
  
  EndProcedure
  ;}


; ============================================================================
;  CONSTRUCTORS
; ============================================================================
;{

; ---[ Heap & Stack]----------------------------------------------------------
Procedure.i New(*obj.Object3D::Object3D_t,name.s="Tree",context.i=Graph::#Graph_Context_Operator)
  ; ---[ Allocate Object Memory ]---------------------------------------------
  Protected *Me.Tree_t = AllocateMemory( SizeOf(Tree_t) )
  
  ; ---[ Initialize Structures ]---------------------------------------------
  InitializeStructure(*Me,Tree_t)
  
  ; ---[ Init Object ]-------------------------------------------------------
  *Me\name = name
  
  ; ---[ Init Members ]------------------------------------------------------
  *Me\nodes = CArray::newCArrayPtr()
;   *Me\parent3dobject = *obj
  ;*Me\root = OGraphTree_AddNode(*Me,"RootNode",0,0,100,100,RGB(100,100,100))
  Select context
    Case Graph::#Graph_Context_Operator
      *Me\root = AddNode(*Me,"TreeNode",0,0,100,100,RGB(50,150,100))
    Case Graph::#Graph_Context_Hierarchy
      *Me\root = AddNode(*Me,"SceneNode",0,0,100,100,RGB(20,200,75))
    Case Graph::#Graph_Context_Shader
      *Me\root = AddNode(*Me,"LayerNode",0,0,100,100,RGB(180,120,20))
  EndSelect
  
  *Me\current = *Me\root
  
  ; ---[ Push Parent Object Stack ]------------------------------------------
  Stack::AddNode(*obj\stack,*Me)
  
  ProcedureReturn( *Me)
  
EndProcedure
;}
EndModule

; ============================================================================
;  EOF
; ============================================================================
; IDE Options = PureBasic 5.42 LTS (Windows - x64)
; CursorPosition = 61
; Folding = ----
; EnableUnicode
; EnableThread
; EnableXP
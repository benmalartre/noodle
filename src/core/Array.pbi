﻿XIncludeFile "Math.pbi"

;========================================================================================
; CArray Module Declaration
;========================================================================================
DeclareModule CArray
  UseModule Math
  Enumeration
    #ARRAY_BOOL
    #ARRAY_CHAR
    #ARRAY_INT
    #ARRAY_LONG
    #ARRAY_FLOAT
    #ARRAY_PTR
    #ARRAY_V2F32
    #ARRAY_V3F32
    #ARRAY_C4F32
    #ARRAY_C4U8
    #ARRAY_Q4F32
    #ARRAY_M3F32
    #ARRAY_M4F32
    
  EndEnumeration
  
  Structure CArrayT
    type.i
    itemSize.i
    itemCount.i
    *data
  EndStructure
  
  Structure CArrayBool Extends CArrayT
  EndStructure
  
  Structure CArrayChar Extends CArrayT
  EndStructure
  
  Structure CArrayInt Extends CArrayT
  EndStructure
  
  Structure CArrayLong Extends CArrayT
  EndStructure
  
  Structure CArrayFloat Extends CArrayT
  EndStructure
  
  Structure CArrayV2F32 Extends CArrayT
  EndStructure
  
  Structure CArrayV3F32 Extends CArrayT
  EndStructure
  
  Structure CArrayC4F32 Extends CArrayT
  EndStructure
  
  Structure CArrayC4U8 Extends CArrayT
  EndStructure
  
  Structure CArrayQ4F32 Extends CArrayT
  EndStructure
  
  Structure CArrayM3F32 Extends CArrayT
  EndStructure
  
  Structure CArrayM4F32 Extends CArrayT
  EndStructure
  
  Structure CArrayPtr Extends CArrayT
  EndStructure
  
  Declare GetPtr(*array.CArrayT,index.i)
  Declare GetValue(*array.CArrayT,index.i)
  Declare.b GetValueB(*array.CArrayT,index.i)
  Declare.c GetValueC(*array.CArrayT,index.i)
  Declare.i GetValueI(*array.CArrayT,index.i)
  Declare.l GetValueL(*array.CArrayT,index.i)
  Declare.f GetValueF(*array.CArrayT,index.i)
  Declare.i GetValuePtr(*array.CArrayT,index.i)
  Declare SetValue(*array.CArrayT,index.i,*value)
  Declare SetValueB(*array.CArrayT,index.i,value.b)
  Declare SetValueC(*array.CArrayT,index.i,value.c)
  Declare SetValueI(*array.CArrayT,index.i,value.i)
  Declare SetValueL(*array.CArrayT,index.i,value.l)
  Declare SetValueF(*array.CArrayT,index.i,value.f)
  Declare SetValuePtr(*array.CArrayT,index.i,value.f)
  Declare Append(*array.CArrayT,*value)
  Declare AppendB(*array.CArrayT,value.b)
  Declare AppendC(*array.CArrayT,value.c)
  Declare AppendI(*array.CArrayT,value.i)
  Declare AppendL(*array.CArrayT,value.l)
  Declare AppendF(*array.CArrayT,value.f)
  Declare AppendArray(*array.CArrayT,*other.CArrayT)
  Declare AppendUnique(*array.CArrayT,*unique)
  Declare GetCount(*array.CArrayT)
  Declare SetCount(*array.CArrayT,count.i)
  Declare GetItemSize(*array.CArrayT)
  Declare Copy(*array.CArrayT,*src.CArrayT)
  Declare Delete(*array.CArrayT)
  
  Declare newCArrayBool()
  Declare newCArrayChar()
  Declare newCArrayInt()
  Declare newCArrayLong()
  Declare newCArrayFloat()
  Declare newCArrayV2F32()
  Declare newCArrayV3F32()
  Declare newCArrayC4U8()
  Declare newCArrayC4F32()
  Declare newCArrayQ4F32()
   Declare newCArrayM3F32()
  Declare newCArrayM4F32()
  Declare newCArrayPtr()
EndDeclareModule

;========================================================================================
; CArray Module Implementation
;========================================================================================
Module CArray
  ;----------------------------------------------------------------
  ; GetPtr
  ;----------------------------------------------------------------
  Procedure GetPtr(*array.CArrayT,index.i)
    If index>=0 And index<*array\itemCount
      offset.i = index* *array\itemSize
      ProcedureReturn *array\data+offset
    EndIf
  EndProcedure
  
   ;----------------------------------------------------------------
  ; Get Item SIze
  ;----------------------------------------------------------------
  Procedure GetItemSize(*array.CArrayT)
    ProcedureReturn *Array\itemSize 
  EndProcedure
  
  ;----------------------------------------------------------------
  ;Copy
  ;----------------------------------------------------------------
  Procedure Copy(*array.CArrayT,*src.CArrayT)
    If Not *array\itemCount = *src\itemCount Or Not *array\itemSize = *src\itemSize
      *array\itemSize = *src\itemSize
      SetCount(*array,*src\itemCount)
    EndIf
    
    CopyMemory(*src\data,*array\data,*src\itemCount * *src\itemSize)
  EndProcedure
  
  ;----------------------------------------------------------------
  ; GetValue
  ;----------------------------------------------------------------
  Procedure GetValue(*array.CArrayT,index.i)
    If index>=0 And index<*array\itemCount
       offset.i = index* *array\itemSize

        ProcedureReturn *array\data+offset

    EndIf
    
  EndProcedure
  
  ;----------------------------------------------------------------
  ; GetValueB
  ;----------------------------------------------------------------
  Procedure.b GetValueB(*array.CArrayT,index.i)
    If index>=0 And index<*array\itemCount
       offset.i = index* *array\itemSize
        ProcedureReturn PeekB(*Array\data+offset)
    EndIf
  EndProcedure
  
  ;----------------------------------------------------------------
  ; GetValueC
  ;----------------------------------------------------------------
  Procedure.c GetValueC(*array.CArrayT,index.i)
    If index>=0 And index<*array\itemCount
       offset.i = index* *array\itemSize
        ProcedureReturn PeekC(*Array\data+offset)
    EndIf
  EndProcedure
  
  ;----------------------------------------------------------------
  ; GetValueI
  ;----------------------------------------------------------------
  Procedure.i GetValueI(*array.CArrayT,index.i)
    If index>=0 And index<*array\itemCount
       offset.i = index* *array\itemSize
        ProcedureReturn PeekI(*Array\data+offset)
    EndIf
  EndProcedure
  
  ;----------------------------------------------------------------
  ; GetValueL
  ;----------------------------------------------------------------
  Procedure.l GetValueL(*array.CArrayT,index.i)
    If index>=0 And index<*array\itemCount
       offset.i = index* *array\itemSize
        ProcedureReturn PeekL(*Array\data+offset)
    EndIf
  EndProcedure
  
  ;----------------------------------------------------------------
  ; GetValueL
  ;----------------------------------------------------------------
  Procedure.f GetValueF(*array.CArrayT,index.i)
    If index>=0 And index<*array\itemCount
       offset.i = index* *array\itemSize
        ProcedureReturn PeekF(*Array\data+offset)
    EndIf
  EndProcedure
  
  ;----------------------------------------------------------------
  ; GetValuePtr
  ;----------------------------------------------------------------
  Procedure GetValuePtr(*array.CArrayT,index.i)
    If index>=0 And index<*array\itemCount
       offset.i = index* *array\itemSize
        ProcedureReturn PeekI(*Array\data+offset)
    EndIf
  EndProcedure
  
  ;----------------------------------------------------------------
  ; SetValue
  ;----------------------------------------------------------------
  Procedure SetValue(*array.CArrayT,index.i,*value)
    If index>=0 And index<*array\itemCount
      offset.i = index* *array\itemSize
      CopyMemory(*value,*array\data+offset,*array\itemSize)
    EndIf
  EndProcedure
  
  ;----------------------------------------------------------------
  ; SetValueB
  ;----------------------------------------------------------------
  Procedure SetValueB(*array.CArrayT,index.i,value.b)
    If index>=0 And index<*array\itemCount
      offset.i = index* *array\itemSize
      PokeB(*array\data+offset,value)
      ;CopyMemory(@value,*array\data+offset,*array\itemSize)
    EndIf
  EndProcedure
  
   ;----------------------------------------------------------------
  ; SetValueC
  ;----------------------------------------------------------------
  Procedure SetValueC(*array.CArrayT,index.i,value.c)
    If index>=0 And index<*array\itemCount
      offset.i = index* *array\itemSize
      PokeC(*array\data+offset,value)
      ;CopyMemory(@value,*array\data+offset,*array\itemSize)
    EndIf
  EndProcedure
  
  ;----------------------------------------------------------------
  ; SetValueI
  ;----------------------------------------------------------------
  Procedure SetValueI(*array.CArrayT,index.i,value.i)
    If index>=0 And index<*array\itemCount
      offset.i = index* *array\itemSize
      PokeI(*array\data+offset,value)
      ;CopyMemory(@value,*array\data+offset,*array\itemSize)
    EndIf
  EndProcedure
  
  ;----------------------------------------------------------------
  ; SetValueL
  ;----------------------------------------------------------------
  Procedure SetValueL(*array.CArrayT,index.i,value.l)
    If index>=0 And index<*array\itemCount
      offset.i = index* *array\itemSize
      PokeL(*array\data+offset,value)
      ;CopyMemory(@value,*array\data+offset,*array\itemSize)
    EndIf
  EndProcedure
  
  ;----------------------------------------------------------------
  ; SetValueF
  ;----------------------------------------------------------------
  Procedure SetValueF(*array.CArrayT,index.i,value.f)
    If index>=0 And index<*array\itemCount
      offset.i = index* *array\itemSize
      PokeF(*array\data+offset,value)
;       CopyMemory(@value,*array\data+offset,*array\itemSize)
    EndIf
  EndProcedure
  
  ;----------------------------------------------------------------
  ; SetValuePtr
  ;----------------------------------------------------------------
  Procedure SetValuePtr(*array.CArrayT,index.i,value.f)
    If index>=0 And index<*array\itemCount
      offset.i = index* *array\itemSize
      PokeI(*array\data+offset,value)
;       CopyMemory(@value,*array\data+offset,*array\itemSize)
    EndIf
  EndProcedure
  
  ;----------------------------------------------------------------
  ; Append
  ;----------------------------------------------------------------
  Procedure Append(*array.CArrayT,*item)
    Protected nb = *array\itemCount
    
    If *array\data = #Null
      *array\data = AllocateMemory(1* *array\itemSize)
    Else
      *array\data = ReAllocateMemory(*array\data,(nb+1)* *array\itemSize)
    EndIf
    
    CopyMemory(*item,*array\data+nb* *array\itemSize,*array\itemSize)
    *array\itemCount = nb +1
  EndProcedure
  
  ;----------------------------------------------------------------
  ; AppendB
  ;----------------------------------------------------------------
  Procedure AppendB(*array.CArrayT,item.b)
    Protected nb = *array\itemCount
    
    If *array\data = #Null
      *array\data = AllocateMemory(1* *array\itemSize)
    Else
      *array\data = ReAllocateMemory(*array\data,(nb+1)* *array\itemSize)
    EndIf
    
    PokeB(*array\data+nb* *array\itemSize,item)
    *array\itemCount + 1
  EndProcedure
  
  ;----------------------------------------------------------------
  ; AppendC
  ;----------------------------------------------------------------
  Procedure AppendC(*array.CArrayT,item.c)
    Protected nb = *array\itemCount
    
    If *array\data = #Null
      *array\data = AllocateMemory(1* *array\itemSize)
    Else
      *array\data = ReAllocateMemory(*array\data,(nb+1)* *array\itemSize)
    EndIf
    
    PokeC(*array\data+nb* *array\itemSize,item)
    *array\itemCount + 1
  EndProcedure
  
  ;----------------------------------------------------------------
  ; AppendI
  ;----------------------------------------------------------------
  Procedure AppendI(*array.CArrayT,item.i)
    Protected nb = *array\itemCount
    
    If *array\data = #Null
      *array\data = AllocateMemory(1* *array\itemSize)
    Else
      *array\data = ReAllocateMemory(*array\data,(nb+1)* *array\itemSize)
    EndIf
    
    PokeI(*array\data+nb* *array\itemSize,item)
    *array\itemCount + 1
  EndProcedure
  
  ;----------------------------------------------------------------
  ; AppendI
  ;----------------------------------------------------------------
  Procedure AppendL(*array.CArrayT,item.l)
    Protected nb = *array\itemCount
    
    If *array\data = #Null
      *array\data = AllocateMemory(1* *array\itemSize)
    Else
      *array\data = ReAllocateMemory(*array\data,(nb+1)* *array\itemSize)
    EndIf
    
    PokeL(*array\data+nb* *array\itemSize,item)
    *array\itemCount + 1
  EndProcedure
  
  ;----------------------------------------------------------------
  ; AppendF
  ;----------------------------------------------------------------
  Procedure AppendF(*array.CArrayT,item.f)
    Protected nb = *array\itemCount
    
    If *array\data = #Null
      *array\data = AllocateMemory(1* *array\itemSize)
    Else
      *array\data = ReAllocateMemory(*array\data,(nb+1)* *array\itemSize)
    EndIf
    
    PokeF(*array\data+nb* *array\itemSize,item)
    *array\itemCount + 1
  EndProcedure
  
  ;----------------------------------------------------------------
  ; AppendArray
  ;----------------------------------------------------------------
  Procedure AppendArray(*array.CArrayT,*other.CArrayT)
    Protected nba = *array\itemCount
    Protected nbo = *other\itemCount
    
    If *array\itemSize = *other\itemSize
      If *array\data = #Null
        *array\data = AllocateMemory(nbo* *array\itemSize)
      Else
        *array\data = ReAllocateMemory(*array\data,(nbo+nba)* *array\itemSize)
      EndIf
      
      CopyMemory(*other\data,*array\data+nba* *array\itemSize,nbo * *array\itemSize)
      *array\itemCount + nbo
    EndIf
    
  EndProcedure
  
  ;----------------------------------------------------------------
  ; AppendArray
  ;----------------------------------------------------------------
  Procedure AppendUnique(*array.CArrayT,*unique)
    Protected nbi = *array\itemCount
    Protected i
    Protected *mem
    If *array\type < #ARRAY_V2F32
      Select *array\type
        Case #ARRAY_BOOL
          AppendB(*array,*unique)
        Case #ARRAY_CHAR
          AppendC(*array,*unique)
        Case #ARRAY_INT
           AppendI(*array,*unique)
        Case #ARRAY_LONG
           AppendL(*array,*unique)
        Case #ARRAY_FLOAT
           AppendF(*array,*unique)
         Case #ARRAY_PTR
           Debug "Append Unique PTR : "+StrF(*unique)
           For i = 0 To nbi-1
            If *unique = PeekI(CArray::GetValue(*array,i))
              Debug "Already There..."
              ; Item Already in Array
              ProcedureReturn 
            EndIf
          Next
           AppendI(*array,*unique)
      EndSelect
      
      
    Else
        
      For i = 0 To nbi-1
        If *unique = CArray::GetValue(*array,i)
          Debug "Already There..."
          ; Item Already in Array
          ProcedureReturn 
        EndIf
        
      Next
      Debug "Append ---> "+Str(*unique)
      Append(*array,*unique)
    EndIf
    
    
  EndProcedure
  
  ;----------------------------------------------------------------
  ; GetCount
  ;----------------------------------------------------------------
  Procedure GetCount(*array.CArrayT)
    ProcedureReturn *array\itemCount
  EndProcedure
  
  ;----------------------------------------------------------------
  ; SetCount
  ;----------------------------------------------------------------
  Procedure SetCount(*array.CArrayT,count.i)
    If count = 0
      If *array\data And MemorySize(*array\data)
        FreeMemory(*array\data)
        *Array\data = #Null
      EndIf
      *array\itemCount = 0
    Else
      If Not count = *array\itemCount 
        *array\itemCount = count
        If *array\data = #Null
          *array\data = AllocateMemory(count * *array\itemSize)
        Else
          *array\data = ReAllocateMemory(*array\data,count* *array\itemSize)
        EndIf
      EndIf
    EndIf
    
  EndProcedure
  
  ;----------------------------------------------------------------
  ; CArrayBool
  ;----------------------------------------------------------------
  Procedure newCArrayBool()
    Protected *array.CArrayBool = AllocateMemory(SizeOf(CArrayBool))
    Protected b.b
    *array\type = #ARRAY_BOOL
    *array\itemCount = 0
    *array\itemSize = SizeOf(b)
    ProcedureReturn *array
  EndProcedure
  
  ;----------------------------------------------------------------
  ; CArrayChar
  ;----------------------------------------------------------------
  Procedure newCArrayChar()
    Protected *array.CArrayChar = AllocateMemory(SizeOf(CArrayChar))
    Protected c.c
    *array\type = #ARRAY_CHAR
    *array\itemCount = 0
    *array\itemSize = SizeOf(c)
    ProcedureReturn *array
  EndProcedure
  
  ;----------------------------------------------------------------
  ; CArrayInt
  ;----------------------------------------------------------------
  Procedure newCArrayInt()
    Protected *array.CArrayInt = AllocateMemory(SizeOf(CArrayInt))
    Protected i.i
    *array\type = #ARRAY_INT
    *array\itemCount = 0
    *array\itemSize = SizeOf(i)
    ProcedureReturn *array
  EndProcedure
  
  ;----------------------------------------------------------------
  ; CArrayLong
  ;----------------------------------------------------------------
  Procedure newCArrayLong()
    Protected *array.CArrayLong = AllocateMemory(SizeOf(CArrayLong))
    Protected l.l
    *array\type = #ARRAY_LONG
    *array\itemCount = 0
    *array\itemSize = SizeOf(l)
    ProcedureReturn *array
  EndProcedure
  
  ;----------------------------------------------------------------
  ; CArrayFloat
  ;----------------------------------------------------------------
  Procedure newCArrayFloat()
    Protected *array.CArrayFloat = AllocateMemory(SizeOf(CArrayFloat))
    Protected f.f
    *array\type = #ARRAY_FLOAT
    *array\itemCount = 0
    *array\itemSize = SizeOf(f)
    ProcedureReturn *array
  EndProcedure
  
  ;----------------------------------------------------------------
  ; CArrayV2F32
  ;----------------------------------------------------------------
  Procedure newCArrayV2F32()
    Protected *array.CArrayV2F32 = AllocateMemory(SizeOf(CArrayV2F32))
    *array\type = #ARRAY_V2F32
    *array\itemCount = 0
    *array\itemSize = SizeOf(v2f32)
    *array\data = #Null
    ProcedureReturn *array
  EndProcedure
  
  ;----------------------------------------------------------------
  ; CArrayV3F32
  ;----------------------------------------------------------------
  Procedure newCArrayV3F32()
    Protected *array.CArrayV3F32 = AllocateMemory(SizeOf(CArrayV3F32))
    *array\type = #ARRAY_V3F32
    *array\itemCount = 0
    *array\itemSize = SizeOf(v3f32)
    *array\data = #Null
    ProcedureReturn *array
  EndProcedure
  
  ;----------------------------------------------------------------
  ; CArrayC4U8
  ;----------------------------------------------------------------
  Procedure newCArrayC4U8()
    Protected *array.CArrayC4U8 = AllocateMemory(SizeOf(CArrayC4U8))
    *array\type = #ARRAY_C4U8
    *array\itemCount = 0
    *array\itemSize = SizeOf(c4u8)
    ProcedureReturn *array
  EndProcedure
  
  
  ;----------------------------------------------------------------
  ; CArrayC4F32
  ;----------------------------------------------------------------
  Procedure newCArrayC4F32()
    Protected *array.CArrayC4F32 = AllocateMemory(SizeOf(CArrayC4F32))
    *array\type = #ARRAY_C4F32
    *array\itemCount = 0
    *array\itemSize = SizeOf(c4f32)
    ProcedureReturn *array
  EndProcedure
  
  ;----------------------------------------------------------------
  ; CArrayQ4F32
  ;----------------------------------------------------------------
  Procedure newCArrayQ4F32()
    Protected *array.CArrayQ4F32 = AllocateMemory(SizeOf(CArrayQ4F32))
    *array\type = #ARRAY_Q4F32
    *array\itemCount = 0
    *array\itemSize = SizeOf(q4f32)
    ProcedureReturn *array
  EndProcedure
  
  ;----------------------------------------------------------------
  ; CArrayM3F32
  ;----------------------------------------------------------------
  Procedure newCArrayM3F32()
    Protected *array.CArrayM3F32 = AllocateMemory(SizeOf(CArrayM3F32))
    *array\type = #ARRAY_M3F32
    *array\itemCount = 0
    *array\itemSize = SizeOf(m3f32)
    ProcedureReturn *array
  EndProcedure
  
  ;----------------------------------------------------------------
  ; CArrayM4F32
  ;----------------------------------------------------------------
  Procedure newCArrayM4F32()
    Protected *array.CArrayM4F32 = AllocateMemory(SizeOf(CArrayM4F32))
    *array\type = #ARRAY_M4F32
    *array\itemCount = 0
    *array\itemSize = SizeOf(m4f32)
    ProcedureReturn *array
  EndProcedure
  
  ;----------------------------------------------------------------
  ; CArrayPtr
  ;----------------------------------------------------------------
  Procedure newCArrayPtr()
    Protected *array.CArrayPtr = AllocateMemory(SizeOf(CArrayPtr))
    Protected i.i
    *array\type = #ARRAY_PTR
    *array\itemCount = 0
    *array\itemSize = SizeOf(i)
    ProcedureReturn *array
  EndProcedure
  
  ;----------------------------------------------------------------
  ; Destructor
  ;----------------------------------------------------------------
  Procedure Delete(*array.CArrayT)
    FreeMemory(*array)
  EndProcedure
  
EndModule

  


; IDE Options = PureBasic 5.42 LTS (Windows - x64)
; CursorPosition = 477
; FirstLine = 466
; Folding = --------
; EnableXP
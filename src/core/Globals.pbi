﻿XIncludeFile "Math.pbi"

;========================================================================================
; Globals Module Declaration
;========================================================================================
DeclareModule Globals
  Enumeration
    #SHORTCUT_COPY = 240
    #SHORTCUT_CUT
    #SHORTCUT_PASTE
    #SHORTCUT_UNDO
    #SHORTCUT_REDO
    #SHORTCUT_ENTER
    #SHORTCUT_DELETE
    #SHORTCUT_RESET
    #SHORTCUT_NEXT
    #SHORTCUT_PREVIOUS
    #SHORTCUT_QUIT
    #SHORTCUT_TAB
  EndEnumeration
  
  Enumeration #PB_Event_FirstCustomValue
    #EVENT_PARAMETER_CHANGED
    #EVENT_BUTTON_PRESSED
    #EVENT_COMBO_PRESSED
    #EVENT_ICON_PRESSED
    #EVENT_TIME_CHANGED
    #EVENT_SELECTION_CHANGED
    #EVENT_COMMAND_CALLED
  EndEnumeration
  
  Enumeration 
    #FONT_TEXT = 1
    #FONT_HEADER
    #FONT_LABEL
    #FONT_TITLE
    #FONT_MENU
    #FONT_SUBMENU
    #FONT_NODE
  EndEnumeration
  
  Enumeration
    #GUI_THEME_LIGHT
    #GUI_THEME_DARK
    #GUI_THEME_CUSTOM
  EndEnumeration
  
  
;   #USE_VECTOR_DRAWING = #False
  CompilerIf Not Defined(USE_VECTOR_DRAWING,#PB_Constant)
    CompilerIf #PB_Compiler_Version<540
      #USE_VECTOR_DRAWING = #False
    CompilerElse
      #USE_VECTOR_DRAWING = #True
    CompilerEndIf
  CompilerEndIf
  
  
  ; ============================================================================
  ;  GLOBALS
  ; ============================================================================
  ; ---[ RGB ]------------------------------------------------------------------
  Global COLOR_MAIN_BG            .i
  Global COLOR_SECONDARY_BG       .i
  Global COLOR_LABEL              .i
  Global COLOR_LABEL_NEG          .i
  Global COLOR_LABEL_DISABLED     .i
  Global COLOR_LABEL_MARKED       .i
  Global COLOR_LABEL_MARKED_DIMMED.i
  Global COLOR_LINE_DIMMED        .i
  Global COLOR_GROUP_FRAME        .i
  Global COLOR_GROUP_LABEL        .i
  Global COLOR_CARET              .i
  Global COLOR_TEXT               .i
  Global COLOR_SELECTED_BG        .i
  Global COLOR_SELECTED_FG        .i
  Global COLOR_NUMBER_BG          .i
  Global COLOR_NUMBER_FG          .i

  Declare Init()
  Declare Term()

EndDeclareModule

;========================================================================================
; Globals Module Implementation
;========================================================================================
Module Globals
  
  Procedure Init()
     ; ---[ Init Once ]----------------------------------------------------
    Protected lval.l = 0
    
    LoadFont( #FONT_TEXT,   "Arial",     8)
    LoadFont( #FONT_HEADER, "Tahoma", 8, #PB_Font_Bold )
    LoadFont( #FONT_LABEL,  "Tahoma",     8)
    LoadFont(#FONT_TITLE,"Tahoma",10,#PB_Font_Bold)
    LoadFont(#FONT_MENU,"Tahoma",9,#PB_Font_Bold)
    LoadFont(#FONT_SUBMENU,"Tahoma",8,#PB_Font_Bold)
    LoadFont(#FONT_NODE,"Tahoma",8)  
    
    COLOR_MAIN_BG             = RGBA(175,175,175,255)
    COLOR_SECONDARY_BG        = RGBA(235,235,235,255)
    COLOR_LABEL               = RGBA(66,66,66,255)
    COLOR_LABEL_NEG           = RGBA(180,180,180,255)
    COLOR_LABEL_DISABLED      = RGBA(150,150,150,100)
    COLOR_LABEL_MARKED        = RGBA(255,175,120,255)
    COLOR_LABEL_MARKED_DIMMED = RGBA(255,175,120,100)
    COLOR_LINE_DIMMED         = RGBA(200,200,200,255)
    COLOR_GROUP_FRAME         = RGBA(100,100,100,200)
    COLOR_GROUP_LABEL         = RGBA(100,100,100,255)
    COLOR_CARET               = RGBA(66,66,66,255)
    COLOR_TEXT                = RGBA(0,0,0,255)
    COLOR_SELECTED_BG         = RGBA(255,100,100,255)
    COLOR_SELECTED_FG         = RGBA(50,100,255,255)
    COLOR_NUMBER_BG           = RGBA(210,210,210,255)
    COLOR_NUMBER_FG           = RGBA(0,0,0,255)
    
  EndProcedure
  
  Procedure Term()
    ; ---[ Term Once ]----------------------------------------------------------
    If IsFont(#FONT_TEXT) : FreeFont( #FONT_TEXT  ) : EndIf
    If IsFont(#FONT_HEADER) : FreeFont( #FONT_HEADER ) : EndIf
    If IsFont(#FONT_LABEL) : FreeFont( #FONT_LABEL   ) : EndIf
    If IsFont(#FONT_TITLE) : FreeFont( #FONT_TITLE  ) : EndIf
    If IsFont(#FONT_MENU) : FreeFont( #FONT_MENU ) : EndIf
    If IsFont(#FONT_SUBMENU) : FreeFont( #FONT_SUBMENU   ) : EndIf
    If IsFont(#FONT_NODE) : FreeFont( #FONT_NODE   ) : EndIf
;   CompilerEndIf
  EndProcedure
  
  
EndModule

  
; IDE Options = PureBasic 5.31 (Windows - x64)
; CursorPosition = 25
; Folding = -
; EnableUnicode
; EnableXP
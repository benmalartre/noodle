XIncludeFile "../core/Globals.pbi"
XIncludeFile "../core/Control.pbi"
XIncludeFile "Divot.pbi"
XIncludeFile "Label.pbi"
XIncludeFile "Check.pbi"
XIncludeFile "Edit.pbi"
XIncludeFile "Number.pbi"
XIncludeFile "Button.pbi"
XIncludeFile "Group.pbi"
XIncludeFile "Icon.pbi"
XIncludeFile "Combo.pbi"
XIncludeFile "Timeline.pbi"


; ==============================================================================
;  CONTROL EDIT MODULE DECLARATION
; ==============================================================================
DeclareModule Controls
  Declare Init()
  Declare Term()
EndDeclareModule

Module Controls
  Procedure Init()
    ControlDivot::Init()
    ControlNumber::Init()
    ControlCheck::Init()
    ControlEdit::Init()
    ControlButton::Init()
    ControlIcon::Init()
    ControlCombo::Init()
  EndProcedure
  
  Procedure Term()
    ControlDivot::Term()
    ControlNumber::Term()
    ControlCheck::Term()
    ControlEdit::Init()
    ControlButton::Term()
    ControlIcon::Term()
    ControlCombo::Term()
  EndProcedure
EndModule

; IDE Options = PureBasic 5.31 (Windows - x64)
; CursorPosition = 40
; Folding = -
; EnableXP
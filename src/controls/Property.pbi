; ============================================================================
;  raafal.gui.controls.property.pb
; ============================================================================
;  2013/06/07 | Ben Malartre
;  - creation
; ============================================================================
XIncludeFile "../core/Control.pbi"
XIncludeFile "../core/Math.pbi"
XIncludeFile "Divot.pbi"
XIncludeFile "Label.pbi"
XIncludeFile "Check.pbi"
XIncludeFile "Edit.pbi"
XIncludeFile "Number.pbi"
XIncludeFile "Button.pbi"
XIncludeFile "Group.pbi"
DeclareModule ControlProperty
  UseModule Math
  #HEAD_HEIGHT = 32

  ; ----------------------------------------------------------------------------
  ;  Object ( ControlProperty_t )
  ; ----------------------------------------------------------------------------
  Structure ControlProperty_t Extends Control::Control_t
    ; ControlProperty::IControlProperty
    pickID.i
    imageID   .i
    label     .s
    append    .i
    row       .i
    down      .i
    overchild .Control::IControl
    focuschild.Control::IControl
    Array *children .Control::Control_t(10)
    Array rowflags .i       (10)
    List *groups.ControlGroup::ControlGroup_t()
    chilcount .i
    current   .i
    closed    .b
          
    head.i
    lock.Control::IControl
    refresh.Control::IControl
    
    ; drawing position
    dx.i
    dy.i
    
    slotID.i
  
  EndStructure
  
  Interface IControlProperty Extends Control::IControl
  EndInterface
  
  Declare New( *object.Object::Object_t, name.s, label.s,x.i=0,y.i=0,width.i=320,height.i=120 )
  Declare Delete(*ctrl.ControlProperty_t)
  Declare Event( *Me.ControlProperty_t, ev_code.i, *ev_data.Control::EventTypeDatas_t = #Null )  
  Declare AppendStart( *Me.ControlProperty_t )
  Declare Append( *Me.ControlProperty_t, ctl.Control::IControl )
  Declare AppendStop( *Me.ControlProperty_t )
  Declare RowStart( *Me.ControlProperty_t )
  Declare RowEnd( *Me.ControlProperty_t )
  Declare AddBoolControl( *Me.ControlProperty_t, name.s,label.s,value.b,*obj.Object::Object_t)
  Declare AddIntegerControl( *Me.ControlProperty_t,name.s,label.s,value.i,*obj.Object::Object_t)
  Declare AddFloatControl( *Me.ControlProperty_t,name.s,label.s,value.f,*obj.Object::Object_t)
  Declare AddVector3Control(*Me.ControlProperty_t,name.s,label.s,*value.v3f32,*obj.Object::Object_t)
  Declare AddQuaternionControl(*Me.ControlProperty_t,name.s,label.s,*value.q4f32,*obj.Object::Object_t)
  Declare AddMatrix4Control(*Me.ControlProperty_t,name.s,label.s,*value.m4f32,*obj.Object::Object_t)
  Declare AddReferenceControl( *Me.ControlProperty_t,name.s,value.s,*obj.Object::Object_t)
  Declare AddColorControl(*Me.ControlProperty_t,name.s,label.s,*value.c4f32,*obj.Object::Object_t)
  Declare AddGroup( *Me.ControlProperty_t,name.s)
  Declare EndGroup( *Me.ControlProperty_t)
  Declare Init( *Me.ControlProperty_t)
  Declare Refresh( *Me.ControlProperty_t)
  Declare EventWithFilter(*Me.ControlProperty_t,filter.i,ev_type.i)
  
  DataSection 
    ControlPropertyVT: 
    Data.i @Event()
    Data.i @Delete()
  EndDataSection
EndDeclareModule

; ============================================================================
;  CONTROL PROPERTY MODULE IMPLEMENTATION 
; ============================================================================
Module ControlProperty
  UseModule Math

  ; ----------------------------------------------------------------------------
  ;  hlpResize
  ; ----------------------------------------------------------------------------
  Procedure.i hlpResize( *Me.ControlProperty_t, *ev_data.Control::EventTypeDatas_t )
    ; If ControlGroup::#Autosize_H:
    ;   Set this Group (client) width to the max width of children width
    ; Else
    ;   Force children width to the (client) width of this Group
    ;
    ; If ControlGroup::#Autosize_V:
    ;   Set this Group (client) height to encompass the last child
    ; Else
    ;   NOP
    
    ; ---[ Sanity Check ]-------------------------------------------------------
    If *Me\chilcount < 1 : ProcedureReturn( void ) : EndIf
    
    ; ---[ Local Variables ]----------------------------------------------------
    Protected dirty   .i = #False
    Protected dirtyPos.i = #False
    Protected i       .i = 0
    Protected j       .i = 0
    Protected iBound  .i = *Me\chilcount - 1
    Protected curV    .i = 0
    Protected curH    .i = #HEAD_HEIGHT
    Protected maxV    .i = 0
    Protected inRow   .i = #False
    Protected son     .Control::IControl
    Protected *son    .Control::Control_t
    Protected lablen.i = Len(*Me\label)
    
    ; 같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같
    ; 같�[ Size Me ]같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같
    ; 같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같
    
    ; ---[ Sanity Check ]-------------------------------------------------------
    If *ev_data
      
      ; ---[ Position ]---------------------------------------------------------
      ; ...[ X ]................................................................
      If ( *ev_data\x <> #PB_Ignore ) And ( *ev_data\x <> *Me\posX )
        dirtyPos = #True
        *Me\posX = *ev_data\x
      EndIf
      ; ...[ Y ]................................................................
      If ( *ev_data\y <> #PB_Ignore ) And ( *ev_data\y <> *Me\posY )
        dirtyPos = #True
        *Me\posY = *ev_data\y
      EndIf
      
      ; ---[ Size ]-------------------------------------------------------------
      ; ...[ Width ]............................................................
      If ( *ev_data\width <> #PB_Ignore ) And ( *ev_data\width <> *Me\sizX )
        dirty = #True
        *Me\sizX = *ev_data\width
      EndIf
      
    EndIf
    
    
    ; 같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같
    ; 같�[ Auto Stacking ]같같같같같같같같같같같같같같같같같같같같같같같같같같같
    ; 같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같
    
    ; ---[ Stack Children ]-----------------------------------------------------
    If *Me\options &  ControlGroup::#Autostack
      ; ...[ Adjust Start Y Depending On Label Presence ].......................
      If lablen : curV = 20 : Else : curV = 14 : EndIf
      ; ...[ Reset Current X Position ].........................................
      curH = 10
      ; ...[ Reset Row Max Height ].............................................
      maxV = 0
      ; ...[ Stack Each Child Under Previous One ]..............................
      For i=0 To iBound
        *son  = *Me\children(i)
        son = *son
  ;       If Son\GetType() = #PB_GadgetType_Group
  ;         Son\Event(#PB_EventType_Resize,@ev_data)
  ;         Continue  
  ;       EndIf
        
        Control::Resize(*son,curH, curV, #PB_Ignore, #PB_Ignore )
        ; ...[ Check Row ]......................................................
        If *Me\rowflags(i)
          curH + *son\sizX + 5
          If maxV < *son\sizY : maxV = *son\sizY: EndIf
        Else
          curH = 10
          If maxV < *son\sizY : maxV = *son\sizY : EndIf
          curV + maxV + 5
          maxV = 0
        EndIf
      Next
      dirty = #True
    EndIf
    
    
    ; 같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같
    ; 같�[ Horizontal Size ]같같같같같같같같같같같같같같같같같같같같같같같같같같
    ; 같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같
  
    ; ---[ Size Gorup ]---------------------------------------------------------
    ;If #True
    If *Me\options & ControlGroup::#Autosize_H
      ; ...[ Reset Values ].....................................................
      maxV = 0 : curV = 0
      ; ...[ Look For Children Max Width ]......................................
      For i=0 To iBound
        *son = *Me\children(i)
        son = *son
        curV = *son\posX + *son\sizX
        If curV > maxV : maxV = curV : EndIf
      Next
      ; ...[ Update Group Width ]...............................................
      If maxV <> *Me\sizX : *Me\sizX = maxV + 10 : dirty = #True : EndIf
      
    ; ---[ Size Children ]------------------------------------------------------
    Else
      ; ...[ Reset Values ].....................................................
      curV = *Me\sizX - 20
      curH = curV
      maxV = 0
      ; ...[ Loop Over Children ]...............................................
      For i=0 To iBound
        If *Me\rowflags(i) And Not inRow
          curH = 0
          For j=i To iBound
            curH + 1
            If Not *Me\rowflags(j) : Break : EndIf
          Next
          curH = ( curV - 5*(curH-1) )/curH
          maxV + 1
          inRow = #True
          Control::Resize(*Me\children(i), #PB_Ignore, #PB_Ignore, curH, #PB_Ignore )
        ElseIf inRow
          Control::Resize(*Me\children(i), 10 + maxV*( curH + 5 ), #PB_Ignore, curH, #PB_Ignore )
          maxV + 1
          If Not *Me\rowflags(i)
            inRow = #False
            curH  = curV
            maxV = 0
          EndIf
        Else
          Control::Resize(*Me\children(i), #PB_Ignore, #PB_Ignore, curH, #PB_Ignore )
        EndIf
      Next
      dirty = #True
    EndIf
  
    
    ; 같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같
    ; 같�[ Vertical Size ]같같같같같같같같같같같같같같같같같같같같같같같같같같같
    ; 같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같
    
    ; ---[ Size Parent ]--------------------------------------------------------
    If *Me\options & ControlGroup::#Autosize_V
      ; ...[ Reset Max Value ]..................................................
      maxV = 0
      ; ...[ Look For Children Max Height ].....................................
      For i=0 To iBound
        *son  = *Me\children(i)
        curV = *son\posY + *son\sizY
        If curV > maxV : maxV = curV : EndIf
      Next
      ; ...[ Update Group Height ]..............................................
      If maxV <> *Me\sizY + 9: *Me\sizY = maxV + 9 : dirty = #True : EndIf
    EndIf
    
    ; 같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같
    ; 같�[ Nested Objects ]같같같같같같같같같같같같같같같같같같같같같같같같같같�
    ; 같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같    
    ; ---[ Resize Nested Groups ]-----------------------------------------------
    For i=0 To iBound
      *son = *Me\children(i)
      If Not *son : Continue : EndIf
      son = *son
      If *son\type = Control::#PB_GadgetType_Group
        Protected ev_data.Control::EventTypeDatas_t
        ev_data\x = *son\posX
        ev_data\y = *son\posY
        ev_data\width = *Me\sizX
        ev_data\height = #PB_Ignore
        Son\Event(Control::#PB_EventType_Resize,@ev_data)
      EndIf
    Next
  
    ; ---[ Check Need Redraw ]--------------------------------------------------
    ResizeGadget( *Me\head,*Me\posX,*Me\posY,*Me\sizX,#HEAD_HEIGHT)
    ResizeGadget( *Me\gadgetID, *Me\posX, *Me\posY+#HEAD_HEIGHT, *Me\sizX, *Me\sizY-#HEAD_HEIGHT )
    ResizeImage ( *Me\imageID, *Me\sizX, *Me\sizY )
  
    ; ---[ Return Redraw Flag ]-------------------------------------------------
    ProcedureReturn( #True )
    
  EndProcedure
  
  ; ----------------------------------------------------------------------------
  ;  hlpNextItem
  ; ----------------------------------------------------------------------------
  Procedure hlpNextItem( *Me.ControlGroup::ControlGroup_t )
    ; ---[ Unfocus Current Item ]-----------------------------------------------
    *Me\focuschild\Event( #PB_EventType_LostFocus, #Null )
    
    ; ---[ Local Variables ]----------------------------------------------------
    Protected iBound.i = *Me\chilcount - 1
    Protected n.i = (*me\current+1)%iBound
    
    Protected ev_data.Control::EventTypeDatas_t 
    *Me\focuschild = *Me\children(n)
    *Me\focuschild\Event(#PB_EventType_Focus,@ev_data)
    ;*Me\focuschild\Event( #PB_EventType_Focus, #Null);*ev_data )
  EndProcedure

  ; ----------------------------------------------------------------------------
  ;  Clear
  ; ----------------------------------------------------------------------------
  Procedure Clear( *Me.ControlGroup::ControlGroup_t )
  ;   If ArraySize(*Me\children())>0
  ;     Protected i
  ;     Protected *ctl.Control::IControl
  ;     For i=0 To *Me\chilcount-1
  ;       Debug "Try to Catch Child : "+Str(i)
  ;       *ctl = *Me\children(i)
  ;       If *ctl<>#Null : *ctl\InstanceDestroy() : EndIf
  ;     Next
  ;     ReDim *Me\children(0)
  ;     *Me\chilcount = 0
  ;   EndIf
  ;   
    
  EndProcedure
  ; ----------------------------------------------------------------------------
  ;  Get Image ID
  ; ----------------------------------------------------------------------------
  Procedure.i GetImageID( *Me.ControlProperty_t)
    ProcedureReturn *Me\imageID
  EndProcedure
  
  ; ----------------------------------------------------------------------------
  ; Select Gadget Under Mouse 
  ; ----------------------------------------------------------------------------
  Procedure Pick(*Me.ControlProperty_t)
    Protected xm = GetGadgetAttribute( *Me\gadgetID, #PB_Canvas_MouseX )
    Protected ym = GetGadgetAttribute( *Me\gadgetID, #PB_Canvas_MouseY )
    
    xm = Math::Min( Math::Max( xm, 0 ), *Me\sizX - 1 )
    ym = Math::Min( Math::Max( ym, 0 ), *Me\sizY - 1 )
    
     ; ---[ First get gadget under mouse ]---------------------------------------
    StartDrawing( ImageOutput(*Me\imageID) )
    *Me\pickID = Point(xm,ym)-1;Red(Point(xm,ym)) - 1
    StopDrawing()
    
    If *Me\pickID >-1 And *Me\pickID<*Me\chilcount
      Protected *overchild.Control::Control_t = *Me\children(*Me\pickID)
      Protected overchild.Control::IControl = *overchild
      If overchild <>*Me\overchild
        *Me\overchild = overchild
        *Me\overchild\Event(#PB_EventType_MouseEnter)
      EndIf
      
      If *overchild\type = Control::#PB_GadgetType_Group
        ControlGroup::Pick(overchild)
      EndIf
    EndIf
  
    ProcedureReturn *Me\pickID
    
  EndProcedure

  ; ----------------------------------------------------------------------------
  ;  Draw Pick Image
  ; ----------------------------------------------------------------------------
  Procedure.i DrawPickImage( *Me.ControlProperty_t)
    ; ---[ Local Variables ]----------------------------------------------------
    Protected i     .i = 0
    Protected iBound.i = *Me\chilcount - 1
  
    Protected  son  .Control::IControl
    Protected *son  .Control::Control_t
  
    ; ---[ Draw ]---------------------------------------------------------------
    StartDrawing( ImageOutput(*Me\imageID) )
    DrawingMode(#PB_2DDrawing_Default)
    
    Box( 0, 0, *Me\sizX, *Me\sizY, RGB($00,$00,$00) )
    
    NewList *ctrls.ControlGroup::IControlGroup()
    
    For i=0 To iBound
  
      *son = *Me\children(i)
      If *son\type = Control::#PB_GadgetType_Group  
        AddElement(*ctrls())
        *ctrls() = *son
        Box( *son\posX, *son\posY, *son\sizX, *son\sizY,i+1)
      Else
        Box( *Me\posX+*son\posX, *Me\posY+*son\posY, *son\sizX, *son\sizY,i+1)
      EndIf
      
    Next
    StopDrawing()
    
  ;   Protected *grp.Control::IControlGroup
  ;   ForEach *ctrls() : *grp = *ctrls()\DrawPickImage() : Next
  
    FreeList(*ctrls())
    
   
  EndProcedure

  ; ----------------------------------------------------------------------------
  ;  Draw
  ; ----------------------------------------------------------------------------
  Declare DrawHead(*Me.ControlProperty_t)
  Procedure.i Draw( *Me.ControlProperty_t)
    Protected label.s = *Me\label
    Protected lalen.i = Len(label)
    Protected maxW .i = *Me\sizX - 21
    Protected curW .i
    
    
    ; ---[ Tag Picking Surface ]------------------------------------------------
    DrawPickImage( *Me)
    ; ---[ Draw Title Bar ]------------------------------------------------------
    DrawHead(*Me)
    
    ; ---[ Drawing Start ]------------------------------------------------------
    StartDrawing( CanvasOutput(*Me\gadgetID) )
    Box( *Me\posX, *Me\posY, *Me\sizX, *Me\sizY, Globals::COLOR_MAIN_BG )
    DrawingMode(#PB_2DDrawing_AlphaBlend)
    
    ; ---[ Sanity Check ]-------------------------------------------------------
    If *Me\chilcount < 1 : StopDrawing() : ProcedureReturn : EndIf
    
    ; ---[ Local Variables ]----------------------------------------------------
    Protected i     .i = 0
    Protected iBound.i = *Me\chilcount - 1
    Protected  son  .Control::IControl
    Protected *son  .Control::Control_t
    
    ; ---[ Redraw Children ]----------------------------------------------------
    Protected ev_data.Control::EventTypeDatas_t
    For i=0 To iBound
       son = *Me\children(i)
      *son = son
      ev_data\xoff = *son\posX
      ev_data\yoff = *son\posY
  
      son\Event( Control::#PB_EventType_Draw, @ev_data )
    Next
  
  ;   ; ---[ Border ]-------------------------------------------------------------
  ;   DrawingMode(#PB_2DDrawing_Outlined)
  ;   RoundBox(*Me\posX,*Me\posY,*Me\sizX,*Me\sizY,5,5,Globals::COLOR_GROUP_FRAME)
  ;   
  ;   ; ---[ Label ]--------------------------------------------------------------
  ;   Protected labelWidth.i = TextWidth(*Me\label)
  ;   DrawingMode(#PB_2DDrawing_Default)
  ;   Box(*Me\posX+20,*Me\posY,labelWidth+10,20,RAA_COLOR_MAIN_BG)
  ;   DrawingMode(#PB_2DDrawing_Transparent)
  ;   DrawText(*Me\posX+25,*Me\posY,*Me\label)
     
    ;DrawImage(ImageID(*Me\imageID),0,0)
    ; ---[ Drawing End ]--------------------------------------------------------
    StopDrawing()
    
    
    
  EndProcedure

  ; ---[ AppendStart ]----------------------------------------------------------
  Procedure AppendStart( *Me.ControlProperty_t )
    
    ; ---[ Check Gadget List Status ]-------------------------------------------
    If *Me\append : ProcedureReturn : EndIf
    
    ; ---[ Update Status ]------------------------------------------------------
    *Me\append = #True
  
  EndProcedure

  ; ---[ Append ]---------------------------------------------------------------
  Procedure.i Append( *Me.ControlProperty_t, *ctl.Control::Control_t )
    ; ---[ Sanity Check ]-------------------------------------------------------
    If Not *ctl
      ProcedureReturn
    EndIf
  
    
    ; ---[ Check Gadget List Status ]-------------------------------------------
    If #False = *Me\append
      ; ...[ FAILED ]...........................................................
     ProcedureReturn #False
    EndIf
    
    ; ---[ Local Variables ]----------------------------------------------------
    Protected Me.Control::IControl     = *Me
  
    ; ---[ Check Array Space ]--------------------------------------------------
    If *Me\chilcount > ArraySize( *Me\children() )
      ReDim *Me\children( *Me\chilcount + 10 )
      ReDim *Me\rowflags( *Me\chilcount + 10 )
    EndIf
    
    ; ---[ Set Me As Control Parent ]-------------------------------------------
    *ctl\parent = Me
  
    ; ---[ Append Control ]-----------------------------------------------------
    *Me\children( *Me\chilcount ) = *ctl
  
    ; ---[ Set Row Flag ]-------------------------------------------------------
    *Me\rowflags( *Me\chilcount ) = *Me\row
  
    ; ---[ One More Control ]---------------------------------------------------
    *Me\chilcount + 1
  
    ; ---[ Return The Added Control ]-------------------------------------------
    ProcedureReturn( ctl )
  
  EndProcedure
  ; ---[ AppendStop ]-----------------------------------------------------------
  Procedure AppendStop( *Me.ControlProperty_t )
    
    ; ---[ Check Gadget List Status ]-------------------------------------------
    If Not *Me\append : ProcedureReturn( void ) : EndIf
    
    ; ---[ Update Status ]------------------------------------------------------
    *Me\append = #False
    
    ; ---[ Update Control And Children ]----------------------------------------
    If #True = hlpResize( *Me, #Null )
      Draw( *Me )
    EndIf
    
  EndProcedure
  ; ---[ RowStart ]-------------------------------------------------------------
  Procedure RowStart( *Me.ControlProperty_t )
    
    ; ---[ Check Row Status ]---------------------------------------------------
    If *Me\row : ProcedureReturn( void ) : EndIf
    
    ; ---[ Update Status ]------------------------------------------------------
    *Me\row = #True
    
  EndProcedure
  ; ---[ RowEnd ]---------------------------------------------------------------
  Procedure RowEnd( *Me.ControlProperty_t )
    
    ; ---[ Check Row Status ]---------------------------------------------------
    If Not *Me\row : ProcedureReturn( void ) : EndIf
    ; ---[ Update Current Child ]-----------------------------------------------
    If *Me\chilcount>0 : *Me\rowflags( *Me\chilcount - 1 ) = #False : EndIf
    
    ; ---[ Update Status ]------------------------------------------------------
    *Me\row = #False
    
  EndProcedure
  ; ---[ Draw Title Bar ]-------------------------------------------------------
  ;------------------------------------------------------------
  Procedure DrawHead(*Me.ControlProperty_t)
  
    StartDrawing( CanvasOutput(*Me\head) )
    
    Box( 0, 0, *Me\sizX, #HEAD_HEIGHT, Globals::COLOR_MAIN_BG )
    ;raaBox(50,10,*Me\sizX,1,Globals::COLOR_GROUP_LABEL)
    Line(0,5,*Me\sizX,1,Globals::COLOR_GROUP_FRAME)
  
  ;   raaDrawingMode( #PB_2DDrawing_Outlined )
  ;   raaClipBoxMask( 12, 0, curW+6, 12 )
  ;   raaRoundBox   ( 3.0, 7.0, *Me\sizX-7, *Me\sizY-10.0, 5.0, 5.0, Globals::COLOR_GROUP_FRAME )
  ;   raaResetClip  ()
  
      DrawingMode( #PB_2DDrawing_Default )
      Box( 12, 0, TextWidth(*Me\label)+6, 12, Globals::COLOR_MAIN_BG )
      DrawingMode( #PB_2DDrawing_Transparent )
      DrawingFont(FontID(Globals::#FONT_HEADER))
      DrawText( 15,  0, *Me\label, Globals::COLOR_GROUP_LABEL )
    
    StopDrawing()
  EndProcedure
  
  ; ---[ Add Bool Control ]-----------------------------------------------
  ;----------------------------------------------------------
  Procedure AddBoolControl( *Me.ControlProperty_t, name.s,label.s,value.b,*obj.Object::Object_t)
    ; ---[ Sanity Check ]-------------------------------------------------------
    If Not *Me : ProcedureReturn : EndIf
    
    Protected Me.IControlProperty = *Me
    
    *Me\dx =0
    Protected width = GadgetWidth(*Me\gadgetID)-10
    Protected Ctl.Control::IControl
    
    ; ---[ Add Parameter ]--------------------------------------------
    If  ListSize(*Me\groups()) And *Me\groups()
      ControlGroup::RowStart(*Me\groups())
      ControlGroup::Append(*Me\groups(),ControlDivot::New(*obj,name+"Divot",ControlDivot::#ANIM_NONE,0,*Me\dx,*Me\dy+2,18,18 ))
      ControlGroup::Append(*Me\groups(),ControlLabel::New(*obj,name+"Label",label,#False,0,*Me\dx+20,*Me\dy,(width-20)*0.25,21 ))
      ControlGroup::Append(*Me\groups(),ControlCheck::New(*obj,name+"Check",name, value))
      ControlGroup::RowEnd(*Me\groups())
    Else
      
      RowStart(*Me)
      Append( *Me,ControlDivot::New(*obj,name+"Divot" ,ControlDivot::#ANIM_NONE,0,*Me\dx,*Me\dy+2,18,18 ))
      Append( *Me,ControlLabel::New(*obj,name+"Label",label,#False,0,*Me\dx+20,*Me\dy,(width-20)*0.25,21 ))
      Append( *Me,ControlCheck::New(*obj, name+"Check",name, value))
    
      RowEnd(*Me)
    EndIf
    
  ;   ; ---[ Connect Signal ]-------------------------------------------
  ;   If *obj
  ;     Protected *class.Class_t = *obj\GetClass()
  ;     *obj\SignalConnect(Ctl\SignalOnChanged(),*Me\slotID)
  ;     *Me\slotID + 1
  ;   EndIf
  ;   
    
    *Me\dy + 22
    ProcedureReturn(#True)
  
  EndProcedure
  
  ; ---[ Add Long To Group ]-----------------------------------------------
  ;----------------------------------------------------------
  Procedure AddIntegerControl( *Me.ControlProperty_t,name.s,label.s,value.i,*obj.Object::Object_t)
    ; ---[ Sanity Check ]-------------------------------------------------------
    If Not*Me : ProcedureReturn : EndIf
    
    Protected Me.ControlProperty::IControlProperty = *Me
    
    *Me\dx = 0
    Protected width = GadgetWidth(*Me\gadgetID)-10
    
    ; ---[ Add Parameter ]--------------------------------------------
    If ListSize(*Me\groups()) And *Me\groups()
     ControlGroup::RowStart( *Me\groups())
      ControlGroup::Append( *Me\groups(), ControlDivot::New(*obj,name+"Divot",ControlDivot::#ANIM_NONE,0,*Me\dx,*Me\dy+2,18,18 ))
      ControlGroup::Append( *Me\groups(), ControlLabel::New(*obj,name+"Label",label,#False,0,*Me\dx+20,*Me\dy,(width-20)*0.25,21 ))
      ControlGroup::Append( *Me\groups(), ControlNumber::New(*obj,name+"Number",value,ControlNumber::#NUMBER_INTEGER,-1000,1000,-10,10,*Me\dx+20+(width-20)*0.25,*Me\dy,(width-20)*0.75,18) )
      ControlGroup::RowEnd( *Me\groups())
    Else
      RowStart(*Me)
      Append(*Me,ControlDivot::New(*obj,name+"Divot",ControlDivot::#ANIM_NONE,0,*Me\dx,*Me\dy+2,18,18 ))
      Append(*Me,ControlLabel::New(*obj,name+"Label",label,#False,0,*Me\dx+20,*Me\dy,(width-20)*0.25,21 ))
      Append(*Me,ControlNumber::New(*obj,name+"Number",value,ControlNumber::#NUMBER_INTEGER,-1000,1000,-10,10,*Me\dx+20+(width-20)*0.25,*Me\dy,(width-20)*0.75,18) )
      RowEnd(*Me)
    EndIf
    
    
  ;   ; ---[ Connect Signal ]-------------------------------------------
  ;   If *obj
  ;     *obj\SignalConnect(Ctl\SignalOnChanged(),0)
  ;   EndIf
    
    ; Offset for Next Control
    ;---------------------------------
    *Me\dy + 22
    
    ProcedureReturn(#True)
  EndProcedure
  
  ; ---[ Add Float To Group ]-----------------------------------------------
  ;----------------------------------------------------------
  Procedure AddFloatControl( *Me.ControlProperty_t,name.s,label.s,value.f,*obj.Object::Object_t)
    ; ---[ Sanity Check ]-------------------------------------------------------
    If Not *Me : ProcedureReturn : EndIf
    
    Protected Me.ControlProperty::IControlProperty = *Me
    
    *Me\dx = 0
    Protected width = GadgetWidth(*Me\gadgetID)-10
    
     ; ---[ Add Parameter ]--------------------------------------------
    If ListSize(*Me\groups()) And *Me\groups()
     ControlGroup::RowStart( *Me\groups())
      ControlGroup::Append( *Me\groups(), ControlDivot::New(*obj,name+"Divot",ControlDivot::#ANIM_NONE,0,*Me\dx,*Me\dy+2,18,18 ))
      ControlGroup::Append( *Me\groups(), ControlLabel::New(*obj,name+"Label",label,#False,0,*Me\dx+20,*Me\dy,(width-20)*0.25,21 ))
      ControlGroup::Append( *Me\groups(), ControlNumber::New(*obj,name+"Number",value,ControlNumber::#NUMBER_SCALAR,-1000,1000,-10,10,*Me\dx+20+(width-20)*0.25,*Me\dy,(width-20)*0.75,18) )
      ControlGroup::RowEnd( *Me\groups())
    Else
      RowStart(*Me)
      Append(*Me,ControlDivot::New(*obj,name+"Divot",ControlDivot::#ANIM_NONE,0,*Me\dx,*Me\dy+2,18,18 ))
      Append(*Me,ControlLabel::New(*obj,name+"Label",label,#False,0,*Me\dx+20,*Me\dy,(width-20)*0.25,21 ))
      Append(*Me,ControlNumber::New(*obj,name+"Number",value,ControlNumber::#NUMBER_SCALAR,-1000,1000,-10,10,*Me\dx+20+(width-20)*0.25,*Me\dy,(width-20)*0.75,18) )
      RowEnd(*Me)
    EndIf
    
    
  ;   ; ---[ Connect Signal ]-------------------------------------------
  ;   If *obj
  ;     *obj\SignalConnect(Ctl\SignalOnChanged(),0)
  ;   EndIf
    
    ; Offset for Next Control
    ;---------------------------------
    *Me\dy + 22
    
    ProcedureReturn(#True)
  EndProcedure
  
  
; ;--------------------------------------------------------------------
; ; ---[ Add Vector2 Control  ]------------------------------------------
; ;--------------------------------------------------------------------
; Procedure AddVector2Control(*Me.ControlProperty_t,name.s,label.s,*value.v2f32,*obj.Object::Object_t)
;   ; Sanity Check
;   ;------------------------------
;   CHECK_PTR1_BOO(*Me)
;   
;   Protected Me.ControlProperty::IControlProperty = *Me
;   Protected Ctl.Control::IControl
;   *Me\dx = 5
;   ; Create Group
;   ;------------------------------
;   Protected options.i = 0;ControlGroup::#Autostack|ControlGroup::#Autosize_V;
;   Protected width = GadgetWidth(*Me\gadgetID)/3
;   Define group.Control::IControlGroup = newControl::IControlGroup( name, name,*Me\gadgetID, *Me\dx, *Me\dy, GadgetWidth(*Me\gadgetID), 40 ,options)
;   
;   ; Add X,Y parameters
;   ;------------------------------
;   ControlGroup::AppendStart(*Me)
;   ControlGroup::RowStart(*Me)
;   
;   ;X
;   ControlGroup::Append(*group, newControl::IControlDivot("XDivot",#RAA_DIVOT_ANIM_NONE,0,*Me\dx,*Me\dy+2,18,18 ))
;   ControlGroup::Append(*group, newControl::IControlLabel("XLabel","X",#False,0,*Me\dx+20,*Me\dy,(width-20)*0.25,21 ))
;   Ctl = ControlGroup::Append(*group, newControl::IControlNumber("XNumber",*value\x,#RAA_CTL_NUMBER_SCALAR,-1000,1000,-10,10,*Me\dx+20+(width-20)*0.25,*Me\dy,(width-20)*0.75,18) )
;   If *obj : *obj\SignalConnect(Ctl\SignalOnChanged(),0) : EndIf
;   
;   ;Y
;   ControlGroup::Append(*group, newControl::IControlDivot("YDivot",#RAA_DIVOT_ANIM_NONE,0,*Me\dx+width,*Me\dy+2,18,18 ))
;   ControlGroup::Append(*group, newControl::IControlLabel("YLabel","Y",#False,0,*Me\dx+width+20,*Me\dy,(width-20)*0.25,21 ))
;   Ctl = ControlGroup::Append(*group, newControl::IControlNumber("YNumber",*value\y,#RAA_CTL_NUMBER_SCALAR,-1000,1000,-10,10,*Me\dx+width+20+(width-20)*0.25,*Me\dy,(width-20)*0.75,18) )
;   If *obj : *obj\SignalConnect(Ctl\SignalOnChanged(),1) : EndIf
;   
;   ControlGroup::RowEnd(*group)
;   ControlGroup::AppendStop(*group)
;   
;   ; Add Group to PPG
;   ;---------------------------------
;   ; ---[ Add Parameter ]--------------------------------------------
;   If ListSize(*Me\groups()) And *Me\groups()
;    *Me\groups()\Append(group)
;   Else
;     Append(*Me,group)
;   EndIf
;   
;   
;   ; Offset for Next Control
;   ;---------------------------------
;   *Me\dy + group\GetHeight()
; 
;   ProcedureReturn(#True)
; EndProcedure

  ;--------------------------------------------------------------------
  ; ---[ Add Vector3 Control  ]------------------------------------------
  ;--------------------------------------------------------------------
  Procedure AddVector3Control(*Me.ControlProperty_t,name.s,label.s,*value.v3f32,*obj.Object::Object_t)
    ; Sanity Check
    ;------------------------------
    If Not *Me : ProcedureReturn : EndIf
    
    Protected Me.ControlProperty::IControlProperty = *Me
    Protected Ctl.Control::IControl
    *Me\dx = 0
    *Me\dy + 10
    ; Create Group
    ;------------------------------
    Protected options.i = 0;ControlGroup::#Autostack|ControlGroup::#Autosize_V;
    Protected width = GadgetWidth(*Me\gadgetID)/3
    Define *group.ControlGroup::ControlGroup_t = ControlGroup::New(*obj, name, name,*Me\gadgetID, *Me\dx, *Me\dy, GadgetWidth(*Me\gadgetID), 40 ,options)
    
    ; Add X,Y,Z parameters
    ;------------------------------
    ControlGroup::AppendStart(*group)
    ControlGroup::RowStart(*group)
  
    Protected w= *Me\sizX/4
    ;X
    ControlGroup::Append(*group,ControlDivot::New(*obj,"XDivot",ControlDivot::#ANIM_NONE,0,*Me\dx,14,18,18 ))
    ControlGroup::Append(*group, ControlLabel::New(*obj,"XLabel","X",#False,0,*Me\dx+20,14,(width-20)*0.25,21 ))
    ControlGroup::Append(*group, ControlNumber::New(*obj,"XNumber",*value\x,ControlNumber::#NUMBER_SCALAR,-1000,1000,-10,10,*Me\dx+40,12,(width-40),18) )
    
    ;Y
    ControlGroup::Append(*group, ControlDivot::New(*obj,"YDivot",ControlDivot::#ANIM_NONE,0,*Me\dx+width,14,18,18 ))
    ControlGroup::Append(*group,ControlLabel::New(*obj,"YLabel","Y",#False,0,*Me\dx+width+20,14,(width-20)*0.25,21 ))
    ControlGroup::Append(*group,ControlNumber::New(*obj,"YNumber",*value\y,ControlNumber::#NUMBER_SCALAR,-1000,1000,-10,10,*Me\dx+width,12,(width-20)*0.75,18) )
    
    ;Z
    ControlGroup::Append(*group, ControlDivot::New(*obj,"ZDivot",ControlDivot::#ANIM_NONE,0,*Me\dx+2*width,14,18,18 ))
    ControlGroup::Append(*group, ControlLabel::New(*obj,"ZLabel","Z",#False,0,*Me\dx+2*width+20,14,(width-20)*0.25,21 ))
    ControlGroup::Append(*group, ControlNumber::New(*obj,"ZNumber",*value\z,ControlNumber::#NUMBER_SCALAR,-1000,1000,-10,10,*Me\dx+2*width,12,(width-20)*0.75,18) )
  
    ControlGroup::RowEnd(*group)
    ControlGroup::AppendStop(*group)
  
    ; Add Group to PPG
    ;---------------------------------
    ; ---[ Add Parameter ]--------------------------------------------
    If ListSize(*Me\groups()) And *Me\groups()
      ControlGroup::Append(*Me\groups(),*group)
  
    Else
      Append(*Me,*group)
    EndIf
    
    
    ; Offset for Next Control
    ;---------------------------------
    *Me\dy + *group\sizY
  
    ProcedureReturn(#True)
  EndProcedure

; ;--------------------------------------------------------------------
; ; ---[ Add Vector4 Control  ]------------------------------------------
; ;--------------------------------------------------------------------
; Procedure AddVector4Control(*Me.ControlProperty_t,name.s,label.s,*value.v4f32,*obj.Object::Object_t)
;   ; Sanity Check
;   ;------------------------------
;   CHECK_PTR1_BOO(*Me)
;   
;   Protected Me.ControlProperty::IControlProperty = *Me
;   Protected Ctl.Control::IControl
;   *Me\dx = 5
;   ; Create Group
;   ;------------------------------
;   Protected options.i = ControlGroup::#Autostack|ControlGroup::#Autosize_V;
;   Protected width = GadgetWidth(*Me\gadgetID)/3
;   Define group.Control::IControlGroup = newControl::IControlGroup( name, name,*Me\gadgetID, *Me\dx, *Me\dy, GadgetWidth(*Me\gadgetID), 40 ,options)
;   
;   ; Add X,Y,Z,W parameters
;   ;------------------------------
;   ControlGroup::AppendStart(*Me)
;   ControlGroup::RowStart(*Me)
;   
;   ;X
;   ControlGroup::Append(*group, newControl::IControlDivot("XDivot",#RAA_DIVOT_ANIM_NONE,0,*Me\dx,*Me\dy+2,18,18 ))
;   ;ControlGroup::Append(*group, newControl::IControlLabel("XLabel","X",#False,0,*Me\dx+20,*Me\dy,(width-20)*0.25,21 ))
;   Ctl = ControlGroup::Append(*group, newControl::IControlNumber("XNumber",*value\x,#RAA_CTL_NUMBER_SCALAR,-1000,1000,-10,10,*Me\dx+20+(width-20)*0.25,*Me\dy,(width-20)*0.75,18) )
;   If *obj : *obj\SignalConnect(Ctl\SignalOnChanged(),0) : EndIf
;   
;   ;Y
;   ControlGroup::Append(*group, newControl::IControlDivot("YDivot",#RAA_DIVOT_ANIM_NONE,0,*Me\dx+width,*Me\dy+2,18,18 ))
;   ;ControlGroup::Append(*group, newControl::IControlLabel("YLabel","Y",#False,0,*Me\dx+width+20,*Me\dy,(width-20)*0.25,21 ))
;   Ctl = ControlGroup::Append(*group, newControl::IControlNumber("YNumber",*value\y,#RAA_CTL_NUMBER_SCALAR,-1000,1000,-10,10,*Me\dx+width+20+(width-20)*0.25,*Me\dy,(width-20)*0.75,18) )
;   If *obj : *obj\SignalConnect(Ctl\SignalOnChanged(),1) : EndIf
;   
;   ;Z
;   ControlGroup::Append(*group, newControl::IControlDivot("ZDivot",#RAA_DIVOT_ANIM_NONE,0,*Me\dx+2*width,*Me\dy+2,18,18 ))
;   ;ControlGroup::Append(*group, newControl::IControlLabel("ZLabel","Z",#False,0,*Me\dx+2*width+20,*Me\dy,(width-20)*0.25,21 ))
;   Ctl = ControlGroup::Append(*group, newControl::IControlNumber("ZNumber",*value\z,#RAA_CTL_NUMBER_SCALAR,-1000,1000,-10,10,*Me\dx+2*width+20+(width-20)*0.25,*Me\dy,(width-20)*0.75,18) )
;   If *obj : *obj\SignalConnect(Ctl\SignalOnChanged(),2) : EndIf
;   
;   ;W
;   ControlGroup::Append(*group, newControl::IControlDivot("WDivot",#RAA_DIVOT_ANIM_NONE,0,*Me\dx+2*width,*Me\dy+2,18,18 ))
;   ;ControlGroup::Append(*group, newControl::IControlLabel("ZLabel","Z",#False,0,*Me\dx+2*width+20,*Me\dy,(width-20)*0.25,21 ))
;   Ctl = ControlGroup::Append(*group, newControl::IControlNumber(">Number",*value\w,#RAA_CTL_NUMBER_SCALAR,-1000,1000,-10,10,*Me\dx+2*width+20+(width-20)*0.25,*Me\dy,(width-20)*0.75,18) )
;   If *obj : *obj\SignalConnect(Ctl\SignalOnChanged(),3) : EndIf
;   
;   ControlGroup::RowEnd(*group)
;   ControlGroup::AppendStop(*group)
;   
;   ; Add Group to PPG
;   ;---------------------------------
;   ; ---[ Add Parameter ]--------------------------------------------
;   If ListSize(*Me\groups()) And *Me\groups()
;    *Me\groups()\Append(group)
;   Else
;     Append(*Me,group)
;   EndIf
;   
;   
;   ; Offset for Next Control
;   ;---------------------------------
;   *Me\dy + group\GetHeight()
; 
;   ProcedureReturn(#True)
; EndProcedure

  ; ---[ Add Quaternion Control  ]------------------------------------------
  ;--------------------------------------------------------------------
  Procedure AddQuaternionControl(*Me.ControlProperty_t,name.s,label.s,*value.q4f32,*obj.Object::Object_t)
    ; Sanity Check
    ;------------------------------
    If Not *Me : ProcedureReturn : EndIf
    
    Protected Me.ControlProperty::IControlProperty = *Me
    Protected Ctl.Control::IControl
    
    ; Create Group
    ;------------------------------
    Protected options.i = ControlGroup::#Autostack|ControlGroup::#Autosize_V
    Protected width = GadgetWidth(*Me\gadgetID)/4
    Define *group.ControlGroup::ControlGroup_t = ControlGroup::New( *obj,name, name,*Me\gadgetID, *Me\dx, *Me\dy, width*4, 50 ,options)
    
    ; Add X,Y,Z parameters
    ;------------------------------
    ControlGroup::AppendStart(*group)
    ControlGroup::RowStart(*group)
  
    Protected w= *Me\sizX/4
    ;X
    ControlGroup::Append(*group,ControlDivot::New(*obj,"XDivot",ControlDivot::#ANIM_NONE,0,*Me\dx,14,18,18 ))
    ControlGroup::Append(*group, ControlLabel::New(*obj,"XLabel","X",#False,0,*Me\dx+20,14,(width-20)*0.25,21 ))
    ControlGroup::Append(*group, ControlNumber::New(*obj,"XNumber",*value\x,ControlNumber::#NUMBER_SCALAR,-1000,1000,-10,10,*Me\dx+40,12,(width-40),18) )
    
    ;Y
    ControlGroup::Append(*group, ControlDivot::New(*obj,"YDivot",ControlDivot::#ANIM_NONE,0,*Me\dx+width,14,18,18 ))
    ControlGroup::Append(*group,ControlLabel::New(*obj,"YLabel","Y",#False,0,*Me\dx+width+20,14,(width-20)*0.25,21 ))
    ControlGroup::Append(*group,ControlNumber::New(*obj,"YNumber",*value\y,ControlNumber::#NUMBER_SCALAR,-1000,1000,-10,10,*Me\dx+width,12,(width-20)*0.75,18) )
    
    ;Z
    ControlGroup::Append(*group, ControlDivot::New(*obj,"ZDivot",ControlDivot::#ANIM_NONE,0,*Me\dx+2*width,14,18,18 ))
    ControlGroup::Append(*group, ControlLabel::New(*obj,"ZLabel","Z",#False,0,*Me\dx+2*width+20,14,(width-20)*0.25,21 ))
    ControlGroup::Append(*group, ControlNumber::New(*obj,"ZNumber",*value\z,ControlNumber::#NUMBER_SCALAR,-1000,1000,-10,10,*Me\dx+2*width,12,(width-20)*0.75,18) )
    
    ;Angle
    ControlGroup::Append(*group, ControlDivot::New(*obj,"AngleDivot",ControlDivot::#ANIM_NONE,0,*Me\dx+2*width,14,18,18 ))
    ControlGroup::Append(*group, ControlLabel::New(*obj,"AngleLabel","Angle",#False,0,*Me\dx+2*width+20,14,(width-20)*0.25,21 ))
    ControlGroup::Append(*group, ControlNumber::New(*obj,"AngleNumber",*value\z,ControlNumber::#NUMBER_SCALAR,-1000,1000,-10,10,*Me\dx+3*width,12,(width-20)*0.75,18) )
  
    ControlGroup::RowEnd(*group)
    ControlGroup::AppendStop(*group)
  
    ; Add Group to PPG
    ;---------------------------------
    ; ---[ Add Parameter ]--------------------------------------------
    If ListSize(*Me\groups()) And *Me\groups()
      ControlGroup::Append(*Me\groups(),*group)
  
    Else
      Append(*Me,*group)
    EndIf
    
    ; Offset for Next Control
    ;---------------------------------
    *Me\dy + *group\sizY
    
    ProcedureReturn(#True)
  
  EndProcedure

  ; ---[ Add Matrix4 Control  ]------------------------------------------
  ;--------------------------------------------------------------------
  Procedure AddMatrix4Control(*Me.ControlProperty_t,name.s,label.s,*value.m4f32,*obj.Object::Object_t)
  
  EndProcedure

  ; ---[ Add Reference Control  ]------------------------------------------
  ;--------------------------------------------------------------------
  Procedure AddReferenceControl( *Me.ControlProperty_t,name.s,value.s,*obj.Object::Object_t)
    ; ---[ Sanity Check ]-------------------------------------------------------
    If Not *Me : ProcedureReturn : EndIf
    
    Protected Me.ControlProperty::IControlProperty = *Me
    Protected Ctl.Control::IControl
    
    Protected width = GadgetWidth(*Me\gadgetID)-10
    
    Protected options = ControlGroup::#Autostack|ControlGroup::#Autosize_V
    Define *group.ControlGroup::ControlGroup_t = ControlGroup::New(*obj, name, name,*Me\gadgetID, *Me\dx, *Me\dy, width, 50 ,options)
    
    Append(*Me,*group)
   
    
    ; ---[ Add Parameter ]--------------------------------------------
    ControlGroup::AppendStart(*group)
    ControlGroup::RowStart(*group)
    ;Append(*Me, newControl::IControlLabel(name+"Label",label,#False,0,*Me\dx,*Me\dy,(width-20)*0.25,21 ))
    ControlGroup::Append( *group,ControlEdit::New(*obj,name+"_Edit",value,5,*Me\dx,*Me\dy+2,(width-110),18) )
    ;*obj\SignalConnect(Ctl\SignalOnChanged(),0)
  ;   Ctl = ControlGroup::Append(*group, newControl::IControlButton(name+"Pick_Btn","Pick",#False,0,(width-60),*Me\dy,50,21))
  ;   *obj\SignalConnect(Ctl\SignalOnChanged(),1)
  ;   Ctl = ControlGroup::Append(*group, newControl::IControlButton(name+"Explore_Btn","...",#False,0,(width-30),*Me\dy,50,21))
  ;   *obj\SignalConnect(Ctl\SignalOnChanged(),2)
    
    ControlGroup::RowEnd(*group)
    ControlGroup::AppendStop(*group)
    
    ; Offset for Next Control
    ;---------------------------------
    *Me\dy +*group\sizY
    
    ProcedureReturn(#True)
  EndProcedure

  ; ---[ Add Matrix4 Control  ]------------------------------------------
  ;--------------------------------------------------------------------
  Procedure AddColorControl(*Me.ControlProperty_t,name.s,label.s,*value.c4f32,*obj.Object::Object_t)
  
    ; ---[ Sanity Check ]-------------------------------------------------------
    If Not *Me : ProcedureReturn : EndIf
    
    Protected Me.ControlProperty::IControlProperty = *Me
    Protected Ctl.Control::IControl
    
    Protected width = GadgetWidth(*Me\gadgetID)-10
    
    Protected options = ControlGroup::#Autostack|ControlGroup::#Autosize_V
    Define *group.ControlGroup::ControlGroup_t = ControlGroup::New(*obj, name, name,*Me\gadgetID, *Me\dx, *Me\dy, width, 50 ,options)
    
    Append(*Me,*group)
   
    
    ; ---[ Add Parameter ]--------------------------------------------
    ControlGroup::AppendStart(*group)
    ControlGroup::RowStart(*group)
    ;Append(*Me, newControl::IControlLabel(name+"Label",label,#False,0,*Me\dx,*Me\dy,(width-20)*0.25,21 ))
    ControlGroup::Append(*group, ControlEdit::New(*obj,name+"_Edit","",5,*Me\dx,*Me\dy+2,(width-110),18) )
  ;   *obj\SignalConnect(Ctl\SignalOnChanged(),0)
  ;   Ctl = ControlGroup::Append(*group, newControl::IControlButton(name+"Pick_Btn","Pick",#False,0,(width-60),*Me\dy,50,21))
  ;   *obj\SignalConnect(Ctl\SignalOnChanged(),1)
  ;   Ctl = ControlGroup::Append(*group, newControl::IControlButton(name+"Explore_Btn","...",#False,0,(width-30),*Me\dy,50,21))
  ;   *obj\SignalConnect(Ctl\SignalOnChanged(),2)
    
    ControlGroup::RowEnd(*group)
    ControlGroup::AppendStop(*group)
    
    ; Offset for Next Control
    ;---------------------------------
    *Me\dy + *group\sizY
    
    ProcedureReturn(#True)
  EndProcedure

  
  ; ---[ Add Group Control  ]------------------------------------------
  ;--------------------------------------------------------------------
  Procedure AddGroup( *Me.ControlProperty_t,name.s)
    ; ---[ Sanity Check ]-------------------------------------------------------
    If Not *Me : ProcedureReturn : EndIf
    
    Protected Me.ControlProperty::IControlProperty = *Me
    Protected Ctl.Control::IControl
    
    Protected width = GadgetWidth(*Me\gadgetID)-10
    
    Protected options = ControlGroup::#Autostack|ControlGroup::#Autosize_V
    Define *group.ControlGroup::ControlGroup_t = ControlGroup::New(*obj, name, name,*Me\gadgetID, *Me\dx, *Me\dy, width, 50 ,options)
    
    AddElement(*Me\groups())
    *Me\groups() = *group
    Append(*Me,*group)
   
    ; ---[ Add Parameter ]--------------------------------------------
    ControlGroup::AppendStart(*group)
  
    ; Offset for Next Control
    ;---------------------------------
    *Me\dy + 20
    
    ProcedureReturn(*group)
  EndProcedure

  Procedure EndGroup( *Me.ControlProperty_t)
    ; ---[ Sanity Check ]-------------------------------------------------------
    If Not *Me : ProcedureReturn : EndIf
    
    ; Retrieve currently open group
    Protected *group.ControlGroup::ControlGroup_t = *Me\groups()
    If Not *group : ProcedureReturn : EndIf
    
    ; Offset for Next Control
    ;---------------------------------
    *Me\dy + *group\sizY-20
   
    ; ---[ Add Parameter ]--------------------------------------------
    ControlGroup::AppendStop(*group)
    DeleteElement(*Me\groups())
   
    ProcedureReturn(#Null)
  EndProcedure
  
  
  ; ---[ On Init ]-----------------------------------------------
  ;---------------------------------------
  Procedure Init( *Me.ControlProperty_t)
    
    ; ---[ Sanity Check ]-------------------------------------------------------
    If Not *Me : ProcedureReturn : EndIf
  
    ; ---[ Add Parameter ]--------------------------------------------
    
    ; ---[ Draw Pick Image ]------------------------------------------
    hlpResize(*Me,#Null)
    DrawPickImage(*Me)
    Draw(*Me)
    
    ProcedureReturn(#True)
  EndProcedure

  ; ---[ Refresh ]-----------------------------------------------
  ;---------------------------------------
  Procedure Refresh( *Me.ControlProperty_t)
    
    ; ---[ Sanity Check ]--------------
    If Not *Me : ProcedureReturn : EndIf
    
    
    ProcedureReturn(#True)
  EndProcedure
  
  ; ---[ Send Event To Filtered Child ]-----------------------------------------------
  ;---------------------------------------
  Procedure EventWithFilter(*Me.ControlProperty_t,filter.i,ev_type.i)
    Protected *son.Control::IControl
    Protected i
    For i=0 To *Me\chilcount-1
      *son = *Me\children(i)
      *son\Event(ev_type,#Null)
    Next i
    
  ;   ForEach *Me\groups()
  ;   
  ;     If filter = *Me\groups()\GetGadgetID() : *Me\groups()\Event( ev_type ) : EndIf
  ;   Next
  EndProcedure


  ; ---[ Free ]-----------------------------------------------------------------
  ;----------------------------------------
  Procedure Delete( *Me.ControlProperty_t )
    
    ; ---[ Sanity Check ]-------------------------------------------------------
    If Not *Me : ProcedureReturn : EndIf
    
    ; ---[ Deallocate Memory ]--------------------------------------------------
    FreeMemory( *Me )
    
  EndProcedure


  ; ============================================================================
  ;  OVERRIDE ( Control::IControl )
  ; ============================================================================
  ; ---[ OnEvent ]--------------------------------------------------------------
  Procedure.i Event( *Me.ControlProperty_t, ev_code.i, *ev_data.Control::EventTypeDatas_t = #Null )  
    Protected *c.Control::IControl = *Me\children(*Me\current)
  
    ; ---[ Local Variables ]----------------------------------------------------
    Protected  ev_data.Control::EventTypeDatas_t
    Protected *son.Control::Control_t
    Protected  son.Control::IControl
    Protected idx,xm,ym
    Protected *overchild.Control::Control_t
    
    idx = Pick(*Me)
    
  
    ; ---[ Dispatch Event ]-----------------------------------------------------
    Select ev_code
        
      ; ------------------------------------------------------------------------
      ;  Resize
      ; ------------------------------------------------------------------------
    Case Control::#PB_EventType_Resize
      
        ; ...[ Update & Check Dirty ]...........................................
        If #True = hlpResize( *Me, *ev_data.Control::EventTypeDatas_t )
          ; ...[ Redraw Control ]...............................................
          Draw( *Me )
        EndIf
        
  ;       ; ...[ Processed ]......................................................
  ;       ProcedureReturn( #True )
        
      ; ------------------------------------------------------------------------
      ;  DrawChild
      ; ------------------------------------------------------------------------
    Case Control::#PB_EventType_DrawChild
        *son.Control::Control_t = *ev_data
        son.Control::IControl    = *son
        ev_data\xoff    = *son\posX
        ev_data\yoff    = *son\posY
        StartDrawing( CanvasOutput(*Me\gadgetID) )
        Box( *son\posX, *son\posY, *son\sizX, *son\sizY, Globals::COLOR_MAIN_BG )
        son\Event( Control::#PB_EventType_Draw, @ev_data )
        StopDrawing()
  
      ; ------------------------------------------------------------------------
      ;  Focus
      ; ------------------------------------------------------------------------
      Case #PB_EventType_Focus
        
      ; ------------------------------------------------------------------------
      ;  ChildFocused
      ; ------------------------------------------------------------------------
      Case Control::#PB_EventType_ChildFocused
        *Me\focuschild = *ev_data
        
      ; ------------------------------------------------------------------------
      ;  ChildDeFocused
      ; ------------------------------------------------------------------------
      Case Control::#PB_EventType_ChildDeFocused
        *Me\focuschild = #Null
        
      ; ------------------------------------------------------------------------
      ;  ChildCursor
      ; ------------------------------------------------------------------------
      Case Control::#PB_EventType_ChildCursor
        SetGadgetAttribute( *Me\gadgetID, #PB_Canvas_Cursor, *ev_data )
        
      ; ------------------------------------------------------------------------
      ;  LostFocus
      ; ------------------------------------------------------------------------
      Case #PB_EventType_LostFocus
        If *Me\focuschild
          *Me\focuschild\Event( #PB_EventType_LostFocus, #Null )
          *Me\focuschild = #Null
        EndIf
        
      ; ------------------------------------------------------------------------
      ;  MouseMove
      ; ------------------------------------------------------------------------
      Case #PB_EventType_MouseMove
  
        xm = GetGadgetAttribute( *Me\gadgetID, #PB_Canvas_MouseX )
        ym = GetGadgetAttribute( *Me\gadgetID, #PB_Canvas_MouseY )
        
        xm = Min( Max( xm, 0 ), *Me\sizX - 1 )
        ym = Min( Max( ym, 0 ), *Me\sizY - 1 )
        
        *overchild.Control::Control_t = *Me\overchild
        If *overchild And *overchild\type = Control::#PB_GadgetType_Group
          Protected ev_datas.Control::EventTypeDatas_t
          ev_datas\x = *overchild\posX
          ev_datas\y = *overchild\posY
          *Me\overchild\Event(#PB_EventType_MouseMove)
        Else
          If *Me\pickID <= 0 And ( *Me\overchild <> #Null ) And  Not *Me\down
            *Me\overchild\Event(#PB_EventType_MouseLeave)
            *Me\overchild = #Null
            SetGadgetAttribute( *Me\gadgetID, #PB_Canvas_Cursor, #PB_Cursor_Default )
          ElseIf *Me\pickID > 0 And *Me\pickID < *Me\chilcount
            Protected ctl.Control::IControl = *Me\children( *Me\pickID )
            If ( ctl <> *Me\overchild ) And  Not *Me\down
              If *Me\overchild <> #Null
                *Me\overchild\Event(#PB_EventType_MouseLeave)
                SetGadgetAttribute( *Me\gadgetID, #PB_Canvas_Cursor, #PB_Cursor_Default )
              EndIf
    
              ctl\Event(#PB_EventType_MouseEnter)
              If Not *Me\down
                *Me\overchild = ctl
              EndIf
            ElseIf *Me\overchild
              *overchild.Control::Control_t = *Me\overchild
              ev_data\x    = xm - *overchild\posX
              ev_data\y    = ym - *overchild\posY
              *Me\overchild\Event(#PB_EventType_MouseMove,@ev_data)
            EndIf
          ElseIf *Me\overchild
            *overchild.Control::Control_t = *Me\overchild
            ev_data\x    = xm - *overchild\posX
            ev_data\y    = ym - *overchild\posY
            *Me\overchild\Event(#PB_EventType_MouseMove,@ev_data)
          EndIf
        EndIf
        
      ; ------------------------------------------------------------------------
      ;  LeftButtonDown
      ; ------------------------------------------------------------------------
    Case #PB_EventType_LeftButtonDown
      *Me\down = #True
      
        If *Me\overchild
          If *Me\focuschild And ( *Me\overchild <> *Me\focuschild )
            *Me\focuschild\Event( #PB_EventType_LostFocus, #Null )
          EndIf
          *overchild.Control::Control_t = *Me\overchild
          ev_data\x = GetGadgetAttribute( *Me\gadgetID, #PB_Canvas_MouseX ) - *overchild\posX
          ev_data\y = GetGadgetAttribute( *Me\gadgetID, #PB_Canvas_MouseY ) - *overchild\posY
          *Me\overchild\Event(#PB_EventType_LeftButtonDown,@ev_data)
  
        ElseIf *Me\focuschild
          *Me\focuschild\Event( #PB_EventType_LostFocus, #Null )
        EndIf
        
      ; ------------------------------------------------------------------------
      ;  LeftButtonUp
      ; ------------------------------------------------------------------------
      Case #PB_EventType_LeftButtonUp
        *overchild.Control::Control_t = *Me\overchild
      If *overchild
          ev_data\x = GetGadgetAttribute( *Me\gadgetID, #PB_Canvas_MouseX ) - *overchild\posX
          ev_data\y = GetGadgetAttribute( *Me\gadgetID, #PB_Canvas_MouseY ) - *overchild\posY
          *Me\overchild\Event(#PB_EventType_LeftButtonUp,@ev_data)
        Else
          Debug ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Left Button Up NO OVERCHILD!!!"
        EndIf
      
        *Me\down = #False
        
      ; ------------------------------------------------------------------------
      ;  LeftDoubleClick
      ; ------------------------------------------------------------------------
      Case #PB_EventType_LeftDoubleClick
        *overchild.Control::Control_t = *Me\overchild
        If *Me\overchild
          ev_data\x = GetGadgetAttribute( *Me\gadgetID, #PB_Canvas_MouseX ) - *overchild\posX
          ev_data\y = GetGadgetAttribute( *Me\gadgetID, #PB_Canvas_MouseY ) - *overchild\posY
          *Me\overchild\Event(#PB_EventType_LeftDoubleClick,@ev_data)
          *Me\focuschild = *Me\overchild
        EndIf
        
      ; ------------------------------------------------------------------------
      ;  RightButtonDown
      ; ------------------------------------------------------------------------
      Case #PB_EventType_RightButtonDown
        *Me\down = #True
        *overchild.Control::Control_t = *Me\overchild
        If *Me\overchild
          If *Me\focuschild And ( *Me\overchild <> *Me\focuschild )
            *Me\focuschild\Event( #PB_EventType_LostFocus, #Null )
          EndIf
          ev_data\x = GetGadgetAttribute( *Me\gadgetID, #PB_Canvas_MouseX ) - *overchild\posX
          ev_data\y = GetGadgetAttribute( *Me\gadgetID, #PB_Canvas_MouseY ) - *overchild\posY
          *Me\overchild\Event(#PB_EventType_RightButtonDown,@ev_data)
        ElseIf *Me\focuschild
          *Me\focuschild\Event( #PB_EventType_LostFocus, #Null )
        EndIf
        
      ; ------------------------------------------------------------------------
      ;  RightButtonUp
      ; ------------------------------------------------------------------------
      Case #PB_EventType_RightButtonUp
        
        If *Me\overchild
          *overchild.Control::Control_t = *Me\overchild
          ev_data\x = GetGadgetAttribute( *Me\gadgetID, #PB_Canvas_MouseX ) - *overchild\posX
          ev_data\y = GetGadgetAttribute( *Me\gadgetID, #PB_Canvas_MouseY ) - *overchild\posY
          *Me\overchild\Event(#PB_EventType_RightButtonUp,@ev_data)
        EndIf
        *Me\down = #False
      
      ; ------------------------------------------------------------------------
      ;  RightButtonUp
      ; ------------------------------------------------------------------------
      Case #PB_EventType_RightButtonUp
        If *Me\overchild
          *overchild.Control::Control_t = *Me\overchild
          ev_data\x = GetGadgetAttribute( *Me\gadgetID, #PB_Canvas_MouseX ) - *overchild\posX
          ev_data\y = GetGadgetAttribute( *Me\gadgetID, #PB_Canvas_MouseY ) - *overchild\posY
          *Me\overchild\Event(#PB_EventType_RightButtonUp,@ev_data)
        EndIf
        
      ; ------------------------------------------------------------------------
      ;  Input
      ; ------------------------------------------------------------------------
    Case #PB_EventType_Input
        ; ---[ Do We Have A Focused Child ? ]-----------------------------------
        If *Me\focuschild
          ; ...[ Retrieve Character ]...........................................
          ev_data\input = Chr(GetGadgetAttribute(*Me\gadgetID,#PB_Canvas_Input))
          ; ...[ Send Character To Focused Child ]..............................
          *Me\focuschild\Event(#PB_EventType_Input,@ev_data)
        EndIf
        
      ; ------------------------------------------------------------------------
      ;  KeyDown
      ; ------------------------------------------------------------------------
      Case #PB_EventType_KeyDown
        ; ---[ Do We Have A Focused Child ? ]-----------------------------------
        If *Me\focuschild
          ; ...[ Retrieve Key ].................................................
          ev_data\key   = GetGadgetAttribute(*Me\gadgetID,#PB_Canvas_Key      )
          ev_data\modif = GetGadgetAttribute(*Me\gadgetID,#PB_Canvas_Modifiers)
          
          ; ...[ Send Key To Focused Child ]....................................
          *Me\focuschild\Event(#PB_EventType_KeyDown,@ev_data)
        EndIf
        
      ; ------------------------------------------------------------------------
      ;  SHORTCUT_COPY
      ; ------------------------------------------------------------------------
      Case Globals::#SHORTCUT_COPY
        ; ---[ Do We Have A Focused Child ? ]-----------------------------------
        If *Me\focuschild
          ; ...[ Send Key To Focused Child ]....................................
          *Me\focuschild\Event(Globals::#SHORTCUT_COPY,#Null)
        EndIf
        
      ; ------------------------------------------------------------------------
      ;  SHORTCUT_CUT
      ; ------------------------------------------------------------------------
      Case Globals::#SHORTCUT_CUT
        ; ---[ Do We Have A Focused Child ? ]-----------------------------------
        If *Me\focuschild
          ; ...[ Send Key To Focused Child ]....................................
          *Me\focuschild\Event(Globals::#SHORTCUT_CUT,#Null)
        EndIf
        
      ; ------------------------------------------------------------------------
      ;  SHORTCUT_PASTE
      ; ------------------------------------------------------------------------
      Case Globals::#SHORTCUT_PASTE
        ; ---[ Do We Have A Focused Child ? ]-----------------------------------
        If *Me\focuschild
          ; ...[ Send Key To Focused Child ]....................................
          *Me\focuschild\Event(Globals::#SHORTCUT_PASTE,#Null)
        EndIf
        
      ; ------------------------------------------------------------------------
      ;  SHORTCUT_UNDO
      ; ------------------------------------------------------------------------
      Case Globals::#SHORTCUT_UNDO
        ; ---[ Do We Have A Focused Child ? ]-----------------------------------
        If *Me\focuschild
          ; ...[ Send Key To Focused Child ]....................................
          *Me\focuschild\Event(Globals::#SHORTCUT_UNDO,#Null)
        EndIf
        
  ;       ; ------------------------------------------------------------------------
  ;       ;  SHORTCUT_NEXT
  ;       ; ------------------------------------------------------------------------
  ;       Case Globals::#SHORTCUT_NEXT
  ;         ; ---[ Do We Have A Focused Child ? ]-------------------------------------
  ;         If *Me\focuschild
  ;           ; ---[ Go To Next Item ]------------------------------------------------
  ;           OControlGroup_hlpNextItem( *Me ) 
  ;         EndIf
  ;         
  ;       ;------------------------------------------------------------------------
  ;       ; SHORTCUT_PREVIOUS
  ;       ;------------------------------------------------------------------------
  ;       Case Globals::#SHORTCUT_PREVIOUS
  ;           Debug "Previous Item called"
  ;           ; ---[ Do We Have A Focused Child ? ]-----------------------------------
  ;           If *Me\focuschild
  ;             ; go to previous child
  ;             Debug "previous child per favor..."
  ;           EndIf
                 
               
        
      ;Case #PB_EventType_KeyUp
      ;Case #PB_EventType_MiddleButtonDown
      ;Case #PB_EventType_MiddleButtonUp
      ;Case #PB_EventType_MouseWheel
      ;Case #PB_EventType_PopupMenu
        ;Debug ">> PopupMenu"
      ;Case #PB_EventType_PopupWindow
        ;Debug ">> PopupWindow"
        
    EndSelect
  
    ; ---[ Process Default ]----------------------------------------------------
    ProcedureReturn( #False )
    
  EndProcedure


  ; ============================================================================
  ;  CONSTRUCTORS
  ; ============================================================================
  Procedure.i New( *object.Object::Object_t, name.s, label.s,x.i=0,y.i=0,width.i=320,height.i=120 )
    
    ; ---[ Allocate Object Memory ]---------------------------------------------
    Protected *Me.ControlProperty_t = AllocateMemory( SizeOf(ControlProperty_t) )
    
    *Me\VT = ?ControlPropertyVT
    *Me\classname = "CONTROLPROPERTY"
    *Me\object = *object
    
    ; ---[ Init Members ]-------------------------------------------------------
    *Me\type       = #PB_GadgetType_Container
    *Me\name       = name
    *Me\gadgetID   = CanvasGadget(#PB_Any,0,#HEAD_HEIGHT,width,height-#HEAD_HEIGHT,#PB_Canvas_Keyboard)
    *Me\imageID    = CreateImage(#PB_Any,width,height)
    *Me\pickID     = CreateImage(#PB_Any,width,height)
    *Me\head       = CanvasGadget(#PB_Any,0,0,width,#HEAD_HEIGHT,#PB_Canvas_Keyboard)
    SetGadgetColor(*Me\gadgetID,#PB_Gadget_BackColor,Globals::COLOR_MAIN_BG )
  
    *Me\posX       = x
    *Me\posY       = y
    *Me\sizX       = width
    *Me\sizY       = height
    *Me\label      = label
    *Me\visible    = #True
    *Me\enable     = #True
  
    ; ---[ Init Structure ]-----------------------------------------------------
    InitializeStructure( *Me, ControlProperty_t ) ; List
   
    ; ---[ Return Initialized Object ]------------------------------------------
    ProcedureReturn( *Me )
    
  EndProcedure

EndModule


; ============================================================================
;  EOF
; ============================================================================

      
      
    
; IDE Options = PureBasic 5.31 (Windows - x64)
; CursorPosition = 79
; FirstLine = 38
; Folding = P0986-
; EnableXP